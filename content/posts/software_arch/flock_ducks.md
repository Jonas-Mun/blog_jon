---
title: "Flock of Ducks - Architecture"
description:
toc: true
authors: ["Jonas"]
tags: ["godot", "software-engineering", "software-architecture"]
categories: ["software-architecture"]
series: []
date: 2020-09-10T10:50:03-05:00
featuredVideo:
featuredImage:
draft: false
---

# Introduction
I will be detailing the architecture of **Flock of Ducks**, a game I made in a team for GWJ#24.
As well as the manner in which each component was implemented.

# Movement
Movement was directional. Having 8 directions for the playeer to move towards:

* N
* S
* E
* W
* NE
* SE
* NW
* SW

## Duck
Initially the Duck would snap to its direction, without any steering.

Afterwards a steering behaviour was given to add some weight to the Duck while moving.
This was achieved by interpolating the *forward vector* of the Duck and the *desired direction*.

The Duck would be moving along its forward vector, and as the forward vector would be interpolated with the direction vector,
it would steer the duck towards the desired direction.

{{< figure src="flock_ducks/Movement_0_test.png" alt="Hello">}}

{{< figure src="flock_ducks/Movement_1.png" >}}

{{< figure src="flock_ducks/Movement_2.png" >}}
## Ducklings
The ducklings had a similar movement, the only differece was that they would have to follow a goal instead of
being steered.

Simply calculating the direction of towards the goal and have the ducklings move toward it achieved the effect of a horde
of ducklings moving together.

{{< figure src="flock_ducks/Duckling_3.png" >}}

{{< figure src="flock_ducks/Duckling_4.png" >}}

### Follow
The follow behaviour had the following design:

1. Given a forward vector, calculate the position directly behind it (opposite direction of forward vector)
2. Make that positiong the new goal
3. Move towards the goal.

Where a forward vector would be given by the *lead* that the duckling would be following. 

{{< figure src="flock_ducks/Follow_5.png" >}}

The position that is to be followed is calculated as follows:

```
position_behind: Point3 = (-forward * r) + lead.position
```
Where *r* is the distance away from the *lead*.

{{< figure src="flock_ducks/Follow_6.png" >}}

## Vector Movement
Both the Duckling and the Duck have the same values:

* Forward Vector
* Desired Vector

I can abstract this and create a Vector Movement class that is in charge of containing these vectors.

{{< figure src="flock_ducks/Vector_Movement.png" >}}


## Flock Manager - Commands
Commands would tell whether the ducklings would stop, or follow based on a given input.

Having a **Flock Manager** would receive the *command* and tell all ducklings to either stop or keep moving.

{{< figure src="flock_ducks/Flock_Command.png" >}}

During this time, the Duck would always be in control of the player, but that would change.

# Player Controller
As the player would be able to control either the Duck or Ducklings, there had to be a way to switch between controlling 
the Duck or Ducklings. This was achieved using the *Player Controller*.

It was a FSM with two states:
* DuckMovement
* DucklingMovement

Depending on the state, it would signal either the Duck or *Flock Manager* to move towards the desired direction.

This means taking awaying the input responsibility from the Duck and placing it in the **Player Controller**.
Where the Player Controller is solely responsible for the input given by the player.

{{< figure src="flock_ducks/Player_Controller.png" >}}

# Flock movement
The reason for the Player Controller was because we wanted the player to control either the Duck or the Flock of Ducklings.

Controlling the flock would be simple. As each duckling has a goal to go towards, the player can be in control of the 
a goal that all ducklings would follow.

{{< figure src="flock_ducks/Flock_7.png" >}}

{{< figure src="flock_ducks/Flock_8.png" >}}

There is one issue with this. If the ducklings collide with an object, the goal would keep moving, and not stay near the flock.
Causing a delay in moving if the player wants to go backwards.

{{< figure src="flock_ducks/Flock_collide_9.png" >}}

The solution would be to tie the goal based on the average center of the flock of ducklings by a certain distance.
Giving the equation: 
```
goal = avg_center + direction * r.
```

Where *r* is the distance away from the average center.

{{< figure src="flock_ducks/Flock_average_center_10.png" >}}

The Flock manager would be responsible for managing the goal and calculating the average center after every frame.

{{< figure src="flock_ducks/Flock_Goal_Flow.png" >}}

# Switches
I wanted to have the switches be easy to be placed in level. Avoiding the need to code in order to make switches work.

The basic behaviour of a switch is that it *affects* an object, when activated.
* switch -> affect

{{< figure src="flock_ducks/Switch_Effect.png" >}}

Leading to the creation of the **Effect Object**

## Effect Objects
The Effect Object is an object that is affected by a switch. It is given the switches that are to affect it, and based on
whether all switches are on or not it plays the appropriate effect. 

{{< figure src="flock_ducks/Switch_effect.png" >}}

It has two types:
* Single (switches have to be on only once)
* Constant (switches have to be on constantly)

The effect object is responsible for assiging 
the switches effect object. This is to avoid having unsyncrhonized switches to their effect object.

{{< figure src="flock_ducks/Effect_Assign.png" >}}

### Binary Effect
Every Effect object has two effects:
* Open
* Close

#### First Approach
The initial approach was to have an Effect Object have their *Open when off* and *Close when on* effect hardcoded. Meaning if you wanted
the reverse behaviour, you'd have to create the same Effect Object, but with the effects reversed.

This was not ideal for developing levels, as I wanted the developer to have the freedom to play around with
ideas with ease. So I came up with another solution.

#### Second Approach - Map Node effect
The last approach would be to have nodes that contain en effect. Depending on the settings given to the Effect Object,
a certain *effect node* would activate when the effect object is *on*. If the effect object was off then another *effect node*
would activate.

Which effect node plays depends on the settings given when creating the Effect Object.
This allows the developer to reverse the effect when desired.

{{< figure src="flock_ducks/Map_Node_Effect.png" >}}

# Items
In the game you would be able to pick up items to unlock doors. 
I was focused on having things be modular, and thought it best to have an **Item Manager** that would be responsible
for storing the item based on who picked the item up.

As you could control two entities (Duck, Ducklings), the Item Manager would have to point to the right
entity when it is being controlled.

This entity contains data of the position of the Duck or Ducklings and the current item
it is holding. Meaning, the entity in the Item Manager has to be synchronized with the Duck or Ducklings.

The synchronization occurs every frame to update the position of the item in *Item Manager*.

{{< figure src="flock_ducks/Item_Manager.png" >}}

When the player presses 'E' near an item:
1. The Item sends a signal to the Item Manager, if the item (itself) can be picked up by the entity.
2. Item Manager checks the current entity being controlled
3. If yes, item's collision is de-activated and placed on the position of the tnity
4. otherwise, the item remains where it is.

## Issues
This design is rather convoluted and overcomplicated.
In fact, each item would have to connect to the Item Manager when setting up the level, to notify when it is being picked up or not. This adds an unnecessary step, and led
to broken levels.

The reason for this design was that I wanted to have the item behaviour be a component of the Duck and Ducklings.
Perhaps I was thinking too much about how the Player Controller is set up, that I decided to implement a similar design.

## Alternative
Instead of there being an *Item Manager*, the Duck and FlockManager could have a component that is responsible for storing an item.
When the Player presses 'E', the Duck or Flock Manager check their current Item and decide to pick it up or not.

This avoids the signal entanglement caused by the previous design, as well as avoiding the syncrhonization of entities in the Item Manager.

# Locks
Each Lock would be given an item that can unlock it, when nearby.
The item would be assigned while editing the level.

As the item was tied to the Item Manager, the lock would have to interact with the Item Manager and check whether the entity contains
the item to unlock it. 

This design is tied to the Item Manager and its overcomplication. It would have been better to adopt the alternative and have the Lock
interact directly with the entity in question. Avoiding the unecessary coordination between the Item Manager and the lock.

{{< figure src="flock_ducks/Lock_Mechanic.png" >}}

### Alternative
{{< figure src="flock_ducks/Lock_Mechanic_Alt.png" >}}

# Pause UI
The Pause UI would be activated, while the in-game processes would de-activate.
So if the Player Controller could activate the Pause UI, then there would be no way to go back to the in-game process, as the Player Controller would aslso be de-activated.
For this reason a **Game Manager** was created to be able to toggle between Pause or In-game state.

{{< figure src="flock_ducks/Game_Manager.png" >}}

## Level transition
When a level was completed, the Game Manager would remove the current level, and place in the next. This meant having one scene where levels would be
interchanged.

For Godot, I am not sure if this is the best approach. Perhaps using the Autoload for the Game Manager would have been better, but I will have to experiment with that.

# Remarks
I followed a bottom-up and module approach when building this game. 
The bottom-up approach allowed me to implement the necessary mechanics, and encapsulate behaviours to isolate bugs when they arose.

It helped tremendously in making changes where needed, or adding new features.

## Dislikes
I did not like the approach I took when implementing the *Item manager*. It caused un-necessary synchronization between various components, making it harder to
get right. 

This could have been avoided by implementing the alternative approach I mentioned before.

# Conclusion
Using the lessons I have learned over the past game jams, I was able to create a code infrastructure that would aid me and my team in developing a game.
The approach I have taken has shown its use and is a powerful tool to follow. I still have not encoutnered a scenario where a different approach would be
taken. 

I must remind myself that this architecture is a means to an end. To help create and develop a game and avoid code entanglement as I experienced in my first
game jam. Making it easier to find bugs and add or implement new features with ease.

