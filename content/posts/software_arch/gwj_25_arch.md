---
title: "3D Architecture in C-Lash"
description:
toc: true
authors: ["Jonas"]
tags: ["video-game","game-jam","godot", "software-architecture", "software-engineering"]
series: []
date: 2020-09-23T12:07:59-05:00
featuredVideo:
featuredImage:
draft: false
---

The theme for Godot Wild Jam #25 was **Change yourself, change the world**.
I initially planned to have it be similar to an *A-RPG*, the likes
of Diablo. Where the more enemies you kill, the stronger you become which allows you
to interact with the world, kill enemies quicker,  and change the world accordingly.

It was a decent idea, but I did not want to create an RPG. Instead I decided
to introduce *costumes* into the game. These costumes change your appearance as well
as your stats. Providing a different means to attack.
Having some RPG elements while avoiding the levelling system.

# Basic Mechanic
The initial idea was to have items that can be picked up throughout the game.
Depending on the item, it can either be:
* Consumable
* Costume

Here the player would have **Stats** that would affect the attack speed and health.

If it was a costume then their stats would change based on the costume's stats
that is acquired from picking up the *costume item*.

# Player + Stats
As the player was going to recover its health, there had to be a way to store its
*max* health while storing their *current* health.

This could ve implemented in various ways.

## Single Script/Class
The max values and current values are stored in the class and are updated accordingly.

This also means that the modified stats will be managed as well.

### Thoughts
Having all types of stats in a single script would involve managing
various variables and coordinating between: max health, current health and modified health.
The modified stats would affect the max stats. Personally I feel I would end up tangling
myself in a web of connections between stats.
It also contains many responsibilities.

## Multiple Components + Stats Interface
Have seperate components for:
* Current Stats - Stats that would be set for the player in the beginning
* Modifier Stats - Stats that contain modifiers to the Base Stats. (Obtained from Costume)
* Base Stats - Stats that are to be used during the game. *Runtime-stats*

To get *max* stats would be to add **Base + Mod** stats.

### Stats Interface
Having a **Stats Interface** would abstract the implementation of the multiple component
statistics and provide an interface that acquires the max stats.
In fact, any changes to the player's stats would occur via the **Stats interface**.
Where the stats interface acts as an API to the Player's stats.

{{< figure src="gwj_25_arch/Stats_Interface.png" >}}

Ideally the function in *SI* for health would be:
* get_max_health
* update_health
* set_modifier_health

This has the Player contain the following structure:

{{< figure src="gwj_25_arch/Player_Stats.png" >}}

# Remarks at this stage.
Describing the architecture as I am doing right now helps create a mental image of how
the game is going to be organized.
When coding I was just writing code with this kind of architecture in mind. This made
it hard for me to contain the structure of the code and be confident that it
would work.

It also helps that I am not focusing too much on the creation of an infrastructure
but the implementation of a behaviour.
This distinction allows me to know when to stop and avoid over-engineering the architecture.
Avoiding the trap of coding for the sake of coding and not for implementing a behaviour/mechanic for a game.

# Items
The idea of items would be to change costumes or acquire a consumable that would
affect the player's current stats.
Being near an iem would allow the player to pick it up, affecting the player's stats
and/or looks.

When near the item, the player can pick it up, so the question becomes:
**"How is the effect of the item applied?"**

There are various ways to do it.

## Item Display + Data
Since the item contains data, stats and/or costume, then the in-game item would just
be a representation of the item. Containing a model and an *item data*.

* Model
* Item Data

Where the model is what is seens in game.

This might be a better approach than the one I ended up creating.
## Item Database
The item would contain:
* Model
* ID

The ID would be used to retrieve the data about the item in the Item Database.
Once the item is retrieved from the item DB, the player would then apply the
necessary changes to either their *current stats* or swap their *costume*.

{{< figure src="gwj_25_arch/Item_Display.png" >}}

### Remarks
The issue with this approach is that the Item Database must be updated for every new item.
It will have to be synchronized with the appropriate item IDs in both in-game and in
the itemDB.

Making it fragile and easy to break. 

## About the Item Display + Data approach
This approach avoids the syncronization between the item's ID and the Item DB.
Everything is contained in the *item display*. Bypassing an extra step.

This pattern is similar to one I discovered for another game: Doctor & Plague.

Usuallu if you want to represent data in a game, you would habe an *object* that would contain the data as a component, while being attached to a model/mesh.

So the Item Display:
* Model/Mesh
* Item Data

# Remarks
Coming up with an implementation requires some exploration with designs. Trying to
figure out the pros and cons and other alternatives.

This is something I discovered after I finished implementing the architecture.
When setting out to code the architecture, I was thinking of creating the best
or most optimal architecture for the game. This led me to think try and get
the best result first without exploring other options.

# Interacting with Items
When the player is near an item, they can pick it up.

Initially I had it so that the item would ping a *Item Manager* that a player
was near. The Item Manager would keep track of the player, and then pass the item
when the player presses *E*.

## Issues
The issue with this approach is that the Item Manager would habe to coordinate the
item that the player would have. Not only that, but if I wanted to have more entities
that can pick up items, the Item Manager would have to coordinate betweent these entities
about which item is near which entity. 

This is a design choice I made in one of my previous games. It introduced a lot
of syncrhonization between the entity and the item in the Item Manager.

## Item Listener
I wanted to avoid having to have an Item Manager coordinate between various entities, so I decided to have an **Item Listener** be a part of the player.
This Item Listener acts as an Item Manager, but instead of coordinate between all entities
that pick up items, it is only in charge of containing the item that is near the player.

When the player is near, the item will tell the player's **Item Listener** that the player
is near an item. When the player presses *E*, the player checks if the Item Listener 
contains an item that is near. If so, it applies the appropriate item.

{{< figure src="gwj_25_arch/Pick_up_item.png" >}}

Applying the item requires to have the item's ID in order to retrieve it from the
Item Database. The Item listener must store the item's ID when it is near an item.

As the item can be either:
* Consumable
* Costume

then the player would apply the appropriate functions.

This would end up having the Player with the following components:

{{< figure src="gwj_25_arch/Player_Item_Listener.png" >}}

# Costume/Model
I planned to have combat in the game, where a collision box would be attached to the model and move based on the animation.

For example: with fists there would be two collision shapes each in the position of the
model's fists. As the fists move so does the collision shapes. When the collision shapes
interact with a body it will deal damage.

This would change based on the **Costume** If the costume would have a *sword* instead of fists, then there would be a collision shape for the sword.

This makes the combat system be animation-based with collision detection. Each model
would have animations that would correspond to the following:

* Idle
* Movement
* Prepare
* Attack
* Intermediary

Each Costume would have these 5 animations.

The idea is that when the player decides to attack, it has a *prepare* animation that
readies the model to attack. After attacking it will go to the intermediary animation
to await another attack. Otherwise it would go back to being idle.

When the player decides to attack, it would play the *prepare -> attack* sequence.
After the attack if would ne: *attack -> intermediary*.

Here is where the abstraction comes in.
The player will only command the costume to attack. It is the job of the costume to transition from: *prepare->attack* or *intermediary->attack*.
The player has no idea how the *Attack* animations are implemented, it only knows
that when it decides to attack, the costume will attack.

## Costume Interface
When having the **Costume** under player, I thought it best to have a **Costume Interface**
be responsible for swapping the *Costume*. The player would not be responsible for
swapping the costume, that would be on the Costume Interface.

{{< figure src="gwj_25_arch/Costume_Interface.png" >}}

# Attacking

I planned to have two attack animations and an intermediary animation:
* Attack0 -> Intermediary -> Attack1

How the player attacks would change between costumes, but the same logic would apply.
As long as the costumes would have this attack system, a *Base System* can be used for all
costumes.

The issues is that the collision shape that would deal damage will have to be deactivated when the
*Costume* is not attacking. As differente costumes will have different number of collision shapes
, this must be abstracted so that the costume only calls a *Deactivate* function and the
weapon/damager would be deactivated.

This weapon is called the **Damager**, and the interface is the **DamagerArea** which controls
the collision.

The idea is to have each costume contain their damager weapon, but the costume would
only be able to interact with it via the *DamagerArea*.

# Remarks
THe system I am designing on paper is the result of implementing it in Godot and changing it 
to make it work. Through trial and error I was able to create a system that would allow me
to have various costumes without the need to code them everytime, yet the process took w 
while to get it right.

Perhaps this could have been avoid, saving time, if I started designing on paper and
experimenting with ideas as I hace beend oing in some parts of this document.

Designing an architecture seems to involve experimentation and discovery. Thinking of various
ways to achive a task, disucssing the pros and cons and whether there are any alternatives.

Although certain patterns can be tools to help implement the design.

It is by understanding their uses and what they are good for, what they implement, that will help
to know when it can be used.

# Player Control
THe current player control is due by accident as I was following a tutorial to implement a
player controller in 3D.

The Player control would control a player node, which would have a *spatial* node that would
represent the mesh/costume. Being responsible for all the input that controls the player.

* Player - Movement/Translation
* Costume Interface - Rotation

The Costume Interface would be rotated while the player would be translated.
This is beneficial as the Costume's rotation would be controlled by the Costume Interface.
Meaning, when swapping between costumes, the costume model would still be rotated to the current 
rotation, and not have to be set everytime a new costume is swapped.

## Building without a tutorial
If I were to build the PLayer Controller from scratch, without a tutorial, I would initially have
the Player contain code for the *input*. This would be ok but seperating the input from the
player entity allows me to contain all input errors in the *player controller*.

Not only that, but I would be able to create an AI Controller that can control the player.
What would be needed is to have the *player* be an interface that provides behaviours.
The *Player/AI Controller* would then make the **Player** entity act according to the input.

{{< figure src="gwj_25_arch/Player_Controller_Costume.png" >}}

# AI (Scrapped)
The AI would have the same system structure as the player with costumes providing the behaviours.
What would be needed is to create an AI Controller that would be able to interact and move
accordingly.

{{< figure src="gwj_25_arch/Enemy_Controller.png" >}}

# Hover Effect + Object Select
 wanted to have a way to signal the player that they are hovering on an object that can be
interactable. 

When hovering, the *entity* that is being hovered would have its material highlighted using
a shader, and unhighlighted when not being hovered over.

The player would only be able to attack if the object that is being hovered is a **Targettable ENtity**. I could of had the Player controller be responsible for managing the hover, but that
would give it more tasks that just the input.

Instead I decided to create a component called: **Entity Acquisition** that is responsible for
managiing the hover behaviour.
The Player Controller would check if there is an entity being hovered, and if so, apply the
appropriate behaviour. The Player Controller could have been given this responsibility,
but as the hovering behaviour had to manage whether it is hovering on an entity, I though it would become too bloated and make the player controller cubmersome when debugging.

{{< figure src="gwj_25_arch/Player_Controller.png" >}}

# Changing the Scene
I wanted to have the look of the game change depending on the current costume the player is using.
This means that upon picking up a costume item, the game would change its look, and enemies.

I could have the Player Controller make the change, but decided not to.

## Level Manager
I decided to have a level manager that is responsible for the current state of the game.
When a costume is picked up, the Player Controller would tell the level manager to change state
based on the costume.

{{< figure src="gwj_25_arch/Level_Manager.png" >}}

# Damager
As I wanted to have weapons deal damage based on collision. I had them be under a *damager*
type. The damager would deal damage to a *Targettable entity* once collided.

The amount of damage dealt would be based on the stats it contains, fists for example.

# Audio Manager
Change music based on the state of the level and scene.

{{< figure src="gwj_25_arch/Audio_Manager.png" >}}

# Conclusion
THis has been an explanation and a look into the software architecture of the game I created
for the GWJ#25. I learned a lot about the creation of an architecture, as well as my reasons
for doing so. The process involved a lot fo trial and error, due to not planning first, but I
eventually got the system I wanted for my 3D game.

## Experimentation and Discovery
When implementing this architecture in godot, I did not take the time to design it first
and then implement it. I though about it in my head and went on implementing it.

This prevented me from exploring alternative approaches to solve the problem. An example is the
*Item ID + Item DB* implementation. When designing on paper I cam across a more flexible approach,
one that came up when exploring ideas.

## Architecture to support the game
When I joined the jame, I wanted to create an architecture not the game. This mentality
made me focus more on coding code and not code for the gameplay. Leading to over-engineering some
aspects of the game just so I can create a cool and flexible architecture for my game.

Any opportunity to code a system, I would take it even if it would not be so beneficial to the game.

The goal in the end was to create a game, not the architecture. A game can have no architecture and
still be good. But how easy it is to add new features or fix bugs is another question.
Here is where the architecture comes into play. It is only a means to and end, not the end itself.
