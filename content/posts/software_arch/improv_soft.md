---
title: "Improving my Code Architecture"
description:
toc: true
authors: ["Jonas"]
tags: ["software-engineering","unity", "software-architecture"]
categories: ["software-architecture"]
series: []
date: 2020-01-07T15:54:41-06:00
featuredVideo:
featuredImage:
draft: false
---

In my retrospection on my game after the game jam, I talked about how the code I had was a mess. References everywhere which felt I had no cohesion. To add a simple function would mean to battle my code and hope it did not break anything else.

This made the experience a lot more difficult than it should have been. For this reason I aim to try and refactor my code to create an architecture that will increase my productivity in the future.

The following images shows how my code referenced many things:


{{< figure src="improv_soft/1_doc.png" class="right" alt="doc-image">}} 

**Red arrow** shows the main function it was supposed to do.
**Black arrows** show that it *also* ended up doing.

Notice how both components had control of the following:
* GUI
* Data
* Logic
* Input

Might not be a big issue, but it makes it a maze when wanting to change a specific property of a GUI and having to look for it in both components.

This kind of structure is what I will be refactoring.

## Principle to Follow
One thing I noticed when creating code was that I tend to start thinking of the architecture before knowing what it is I would be organizing.
This method would lead me to create scenarios that would not have existed or ignore scenarios that end up existing. It felt like creating a roof with no base, if that makes sense.
I followed a **Top-Down** approach.

This time I will try a **Bottom-Up** approach. Figuring out what assets I am going to use, and then think how I can organize these assets in a managable manner.

## What needs to be Organized

### Scenario/Terrain
The game would display a terrain, and some buildings. This is the scenario/map of the game.
I would need to have the following displayed:
* Terrain
* Buildings

A *MapSetup* would be in charge of placing the terrain and the buildings.
In fact, MapSetup would be in charge of the current state of the *Map*.

What led me to come to this conclusion?

The map will have to contain all the live buildings as well as the live terrain.
It could have been given to the Game Manager, but that would be giving the responsibilities to two components for one functionality, something I want to avoid.

As said before, the Game manager is only in charge of coordinating and communicating between other *Managers*.
Containing everything about the scenario under *Map* makes it easier to manage.

### Activation of Components
I want to have the map be constructed when neccesary.
This is so I can transition from the main menu to the in game.

The **Game Manager** would communicate with the **Map**. Telling it to create the scenario.

### Diagram so far

{{< figure src="improv_soft/2_refact.png" class="right" >}}


## Abstract Design
The design above can be abstracted to convery the general idea of how my architecture is going to become.
***Note:*** This is only to convey the general idea of how the architecture will be, not an exact representation.

The software architecture will consist of a *main manager* and many *sub managers*. Making sure each sub manager is in charge of their **single** functional duty, to avoid having many references being scattered in many places.
Managers in green can only communicate through the Game Manager.
{{< figure src="improv_soft/3_abs_des.png" class="right" >}}

Using the exmaple of the **Map**, Map can be a sub manager; providing the single functional duty of maintaining the scenario/map.

The buttons in blue have access to all the managers. This is to allow some flexibility in the architecture. It makes it easier to change data or the overall state of the game without having to funnel data through managers. They have *direct* access to what they need.

Having managers communicating via Game Manager allows me to contain their functional duty in their space. While the buttons being a direct access to many managers in order to change states or logic in the game.

The Game Manager, although meant to provide communication between sub managers, also maintains the state of the overall game. Where changing the state propogates the change to all sub managers.
It does break the principle of having a single functional duty, but the game manager has access to all sub managers and so can propogate state changes to all managers when necessary. Creating a seperate manager that maintains the state may be overkill. Perhaps when there are various states for different managers, then maybe a sub manager is necessary for states. But one can argue that the sub manager itself can store their own sub stae.

## Final Design

{{< figure src="improv_soft/4_prac_design.png" class="right" >}}

Here is the finished code structure.

Notice the **Orange** blocks. These are *elements* of the sub manager. They can be considered as a *sub sub manager* (Managing certain aspects of the gui). All to contain the code and avoid components referencing many areas of the whole architecture.

The **Purple** shapes are **Prefabs**. They can be the player, NPCs, or anything that interacts with the game. These have direct access to certain managers such as the *Logic Manager* .

## Closing Remarks
The architecture I have ended up with has made it easier to add and test the basic mechanics of the game. Allowing me to implement a *Test States* button, making it easier to debug certain areas of the game.

Not only that, but it gives me confidence in adding new functionalities without worrying if I am breaking something. Although inevitably I will, but the code is *contained* in a mannder that makes it easier to fix it. (Optimistically)

I would like to point out that the Manager and Sub Manager concept can be viewed as a **Manager** concept without the Sub Manager. What I mean is to treat all kinds of managers as main managers and not seperate them to *Manager* and *Sub Manager*.

{{< figure src="improv_soft/5_prac_pers.png" class="right" >}}

Where the *Game Manager* is in charge of maintaing the state throughout the game. No longer in charge of being a channel for all managers. This would mean that all other managers can interact with each other.

This might be a good idea, but the main reason for having all the Sub Managers communicating through the Game Manager is to avoid references poiting in all *directions* in the architecture. Although I am not sure of the scalability of this architecture.

Would the concept of all managers communicating with each other be a lot more scalable, or having a single Game Manager being the center of communication be more preferable? 

Time may tell me the answer, but for now the architecture I have engineered fits the needs of my game.

One issue could be that one change to any of the function signatures can break the whole communication. 

## Conclusion
The architecture was created by following the **Bottom-up** approach. Implementing this architecture without knowing the *things* that are to be organized may just bring a lot more problems in the future. Rather this architecture is a guide to mold the code in a manner that helps the programmer be more productive.

In the end this architecture is a means to an end: To remove the mess of references I had, and be able to change the state of the game.

I believe this architecture does just that.
