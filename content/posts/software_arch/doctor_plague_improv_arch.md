---
title: "Doctor & Plague - Improving Code Architecture"
description:
toc: true
authors: ["Jonas"]
tags: ["software-architecture", "software-engineering","unity"]
categories: ["software-architecture"]
series: []
date: 2020-05-24T19:14:34-05:00
featuredVideo:
featuredImage:
draft: false
---

# What will be done
I will be going over the code architecture that was used to create the *gameplay* mechanic of **Doctor & Plague**.
First, going over what it does, and its limitations. Then I will create an improved architecture.

# Prototype Architecture
The prototype had the following architecture.


{{< figure src="doctor_plague_improv_arch/Old/Prototype_Architecture_DP.png" alt="sdHe">}}



Not much is going on as it was solely to see how that game would be played. As well as there was no set of rules
for the card game just yet.

# Final Architecture

{{< figure src="doctor_plague_improv_arch/Old/Game_Architecture_DP.png" >}}

Notice how the architecture is very similar to the prototype. I built the gameplay architecture on top of the prototype which forced me to hard code certain aspects of the game.

## Issues:

### Fixed Actor Piles
In the *Game Arena*, there is a list of piiles for each actor. Meaning if a new actor were to appear in game, I would have to manually add the pile onto the *Game Arena* to support three players.
This was not flexible in the long run.

### No Finite Decks for Actors
At the momemnt, Actors would get their cards from the **Card Factory** directly. This made it so that each Actor would have infite cards to play until the game ends.

It works, but it is not how the game was intended to be played. Each Actor should have a finite set of cards to pick from. This finite set is their deck of cards.

# Improving the Architecture
To improve the architecture, I had to start from scratch. Thinking about each component of the gameplay mechanic.
What did each component do, and how did it do it.

## Actors
To refactor the Actors I thought of the following questions:

### What does each character contain? - Data
* Deck
* Cards in Hand
* Center Point
* Piles
* ID

Here I decided to have actors contain their own list of piles. This new addition makes it easy to add new actors onto the game. No longer will I have to manually add a list of piles in arena, rather the actor provides all the data necessary to play a game.

### What can each actor do? - Behaviour
* Play Card
* Close Pile
* End Turn
* Many more...

Each actor would have a certain actions it can do, while manipulating the data in the actor.

{{< figure src="doctor_plague_improv_arch/Actor_Behaviour.png" alt="HEWLL" >}}

### Actor Architecture

{{< figure src="doctor_plague_improv_arch/Actor_Diagram.png" >}}

**Notice** how the actor can add cards onto piles. This means that the actor can play any cards it wants without a set of rules.

## Game Logic
In the old architecutre the game arena was responsible for some aspects of the game logic, making it responsible for another aspect of the game.
My solution was to have all aspects of rules and logic in one section.

To do so, I organize the rules into the following categories.

### Atomic Rules
Most basic rules

1. Pile Empty
2. Card is Red
3. Pile is Closed
4. Pile is Open
5. Special Card.
6. Red and Blue Alternate
7. Difference == 2
8. Cards Negate
9. Equal Value Cards
10. Equal 10

### Composite Rules
Made up of Atomic Rules

* First Card Red: 1 -> 2
* Special Card  : 4 -> 5 -> 7
* Red/Blue      : 4 -> 6 -> 7

### Card Effect Rules
These had an effect during the game.

* Negation: !1 -> 8
* Counter :  3 -> 5 -> 9
* Counter :  3 -> 11

As the **Atomic Rules** would be used in various areas of the logic, I decided to have them be *static* so they can be called wherever they are required to avoid having the logic components containing a reference.

{{< figure src="doctor_plague_improv_arch/Game_Logic.png" >}}

## Game Arena
Both Actors and Game Logic have been created. Now it is a matter of integrating them together.

The *Game Arena* is in charge of checking whether the card that the *Actor* plays is valid, as well as playing effects after each round/turn.
It does so by utilizing the *Game Logic*.

{{< figure src="doctor_plague_improv_arch/Game_Arena.png" >}}

The idea is that the *Actor* wants to play a card, but asks the *Gamre Arena* if the card is valid.
The *Game Arena* checks with its rules to see if it's valid. If so, the *Actor* plays the card in their respective pile.
Else, it has to choose another card.

This sequential logic can be generalize as so:

{{< figure src="doctor_plague_improv_arch/General_Gameplay.png" >}}


## Improved Architecture.

Combining them together gives the following gameplay architecture

{{< figure src="doctor_plague_improv_arch/Current_Architecture.png" >}}

# Benefits of New Architecture

## Flexibility on Actors
Since the *Game Arena* contains a dictionary of current actors, it is easy to add more players in the game.
No need to manually code in another pile as would have been the case in the old architecture.

## Addition of Rules
By isolating the rules, new rules can be added when necessary. One can come up with crazy game types for the card game, and still maintain the core set of rules. It is simply a matter of creating another rules component like, *Party Rules*, that are to be used when a *Party Game* is being played.

## Actor Interface
To control an actor, an actor interface is implemented that allows an *AI* or a *Player* to take control of a specific actor. This is beneficial in multiplayer games when a player drops out.
Instead of ending the game, the interface switched to an *AI* interface. Allowing the game to continue.

## Bugs
Since each functionality is encapsulated in its own component,  bugs can be tracked to their origin with relative ease and be dealth with appropriately.

# Conclusion
The new architecture allows for a lot of flexibility. It allows for various actors in games, without adding code into *Game Arena*, while adding decks onto actors. The flexibility also allows one to play around with rules.

It succeeds in the initial goals that were set, while providing a lot of possibilities for future additions.
Yet, I must remember that this architecture was a means to an end. The end was to have a flexbile architecture that allowed to add/create/change various aspects of the game without fear of breaking the whole game.

I believe this architecture succeeds in that goal.
