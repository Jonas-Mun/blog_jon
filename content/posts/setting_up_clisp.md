---
title: Setting up Common Lisp in vim
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-26T21:01:58Z
lastmod: 2022-11-26T21:01:58Z
featuredVideo:
featuredImage:
draft: false
---

In this article I will attempt to install Common Lisp in vim.

I need to have three things:
* Common Lisp implementation
* Quicklisp Library
* SLIME - Form of interacting with Lisp

# Common Lisp Implementation

I have decided to use **SBCL** (Steel Bank Common Lisp)
```
sudo pacman -S sbcl
```

# Installing Quicklisp

What is *quicklisp*?
It is a package manager for Common Lisp.
Luckily for me, Arch comes with *quicklisp*.

```
sudo pacman -S quicklisp
```

We now have to install *quicklisp*.
We do this by using *sbcl*:

```
sbcl --load '/usr/share/quicklisp/quicklisp.lisp' --eval '(quicklisp-quickstart:install)' --quit
```

If you installed using the package manager, you would get:
```
Quicklisp has already been installed. #P"/path/to/user/quicklisp/setup.lisp."
```
so we firs run *sbcl*
```
$ sbcl --load /path/to/user/quicklisp/setup.lisp
```

Something along those lines. I think I skipped some steps.
This should add lines to the file: */path/to/user/.sbclrc*.

# SLIME
I found the following plugin for vim:
```
https://github.com/jpalardy/vim-slime
```

Simply add it to your plugins.
I use *VimPlug*, so I add the following:
```
Plug 'jpalardy/vim-slime'
```

Now the way it works is that you have a target running.
This target could be *tmux* as I have chosen it to be.

I added:
```
let g:slime_target = "tmux"
```
into my *.vimrc*, so that it uses *tmux* as a target.
I have to specify the *pane-number* so that it can send text to the tmux pane.

## Other option

There is another option: https://github.com/kovisoft/slimv
specifically for *SLIME*, but I see the usefulness of *vim-slime* as I can use it for *ocaml* or even *python*.
Anything that has a REPL. Even Haskell!

# CLISP - (Recommended)

There is a more simpler LISP implementation called **CLISP**.
Simply type:
```
sudo pacman -S clisp
```
and it will be installed.

No need to setup Quicklisp or SLIME!
Although the *vim-slime* works perfectly with it.
