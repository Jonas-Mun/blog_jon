---
title: i3 Setup
description:
toc: true
authors: ["Jonas"]
tags: ["linux","i3"]
categories: []
series: []
date: 2021-08-07T10:03:31-05:00
lastmod: 2021-08-07T10:03:31-05:00
featuredVideo:
featuredImage:
draft: false
---

The i3 WM is installed in Manjaro.

# Installing
Installing i3 via
```
sudo pacman -S i3
```

# Setup
Once installed, switch to i3 before logging in.

A prompt will ask you to set up the *i3 config*.
This will create the default config for starting i3.

It is located at:
**~/.config/i3/**

# Brightness
When installing i3, I experienced the brightness
buttons not working. Initially I tried to use *xbacklight*but got an error.

The remedy was to install **acpilight** via
```
sudo pacman -S acpilight
```

## Add to Video group
Then I added the user (me) to the group **video**
If you write:
```
groups
```

in terminal, you will see the groups the current user is in.

To add the user to the video group, type:
```
usermod -a -G video <user>
```

## Add Udev rules
The *udev rules* are located in the following directory:
**/etc/udev/rules.d/**
The file to make/edit is **bacjlight.rules**

Add the following rules:
```
ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness"
ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"
```

### Kernel Specific
In the **Kernel Specific**, you will have to add the one based on your computer. This can be different if using AMD.
As I am using *intel*, having **Intel Backlight** will work.

## Brightness Controls in i3 Config
In the **i3/config**, add the following binds:
```
bindsym XF86MonBrightnessUp exec xbacklight -inc 10
bindsym XF86MonBrightnessDown exec xbacklight -dec 10
```

# i3 Bar
i3 provides a default bar. This can be configured in the *config* file:

```
bar {
    status_command i3status
}
```
i3Bar uses i3Status to gain information.
To configure i3status, the config will have to be created:
```
mkdir -p ~/.config/i3status/
touch ~/.config/i3status/config
```

## Adding to i3Status
i3 bar uses the i3 status. My setup followed this blog:

<a href="https://medium.com/hacker-toolbelt/my-i3status-customization-3e8ad6f0153a">i3 status</a>

### Volume
In the **i3status/config**, add:

```
order += volume master
volume master {
    format = "♪: %volume"
    format_muted = "♪: muted (%volume)"
    device = "pulse"
}
```

### Brightness

For brightness, add the following in the **i3status/config** file:

```
order += read_file BRIGHTNESS
read_file BRIGHTNESS {
    format = "BRI %content"
    path = "/sys/class/backlight/intel_backlight/brightness"
    max_characters = 5
}
```

# Gaps
If installing via arch, you may have already install **i3-gaps**.
To add gaps, add the following to your **i3/config**
```
gaps inner 10
gaps outer 10
```

# Transparency in i3
TO have some transparency in i3 for:
* alacritty
* browsers

The reason for installing picom was to add transparency to alacritty.


install picom:

```
sudo pacman -S picom
```

To have picom start when booting i3, add the following line to **i3/config**
```
exec --no-startup-id picom --experimental-backends &
```

If you want to have fade, add **-f**:
```
exec --no-startup-id picom -f --experimental-backends &
```

There was a reason why I added *--experimental-backends*, but I don't remember why.

## Reducing Artifacts

Sometimes when swapping windows, certain artifacts may show.
To reduce these artifacts, change the following settings to (in ~/.config/picom.conf):

```
fade-in-step = 0.83
fade-out-step = 0.63
```


## Config
The config file is located in:
**~/.config/picom/picom.conf**

I added the following to my config:
```
opacity-rule = [
    "90:class_g = 'Alacritty'",
    "100:class_g = 'firefox'"
];

#menu
dropdown_menu = {shadow = false; }
popup_menu = {shadow = false; }
utility = {shadow = false; }
```

This provides the transparency I need.

# Setting Background in i3
To st i3, I downloaded nitrogen:

```
sudo pacman -S nitrogen
```

Simply call *nitrogen* in the directory where the
wallpaper is located and choose the desired wallpaper.

## Saving background
Add the following code to **i3/config** to safe the background chosen:

```
exec --no-startup-id nitrogen --restore
```

# Bind Keys to apps
I have set up some bindkeys to open some applications:
* File System - Mod+Shift+f
* Browser     - Mod+Shift+b

This is located in the config file of i3 as:
```
bindsym $mod+Shift+b exec firefox
bindsym $mod+Shift+f exec thunar
```

#Conclusion

This setup took me days to get it right. I was using XFCE Manjaro before, but felt I was not being as productive as
I could be. So I decided to look at alternatives and stumbled upon i3. I set out to switch to i3.

Its purpose was to provide a comfortable environment where I could
be productive when programming. So far, I am liking it.

Would I do it again? Maybe not. It takes so much of your time to just understand how the tool works, and to get
it to work to your liking. I may add some things here and there if I feel like it.

This articles showed how you can use various tools to provide the functionality
that XFCE already provides out of the box. The driving force that kept me going as, as mentioned before, to create
a comfortable and productive environment for me to use my computer.
