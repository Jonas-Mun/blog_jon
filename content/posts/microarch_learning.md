---
title: An Overview of Micro-Architecture
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-08T14:22:53Z
lastmod: 2022-11-08T14:22:53Z
featuredVideo:
featuredImage:
draft: false
---

In this article, I will attempt to describe the microarchitecture of processors; specifically the MIPS architecture, because I am most familiar with it.
This is a way for me to gauge my understanding and see where I have gaps in my knowledge.
Some of my writings may be wrong, so do not take this as truth, but a means of exploring a topic.

# Performance of a CPU

To measure performance, we must make sure we know exactly what we are measuring for.
There are two forms of measuring performance:
* Execution Time/run time
* Throughput of instructions

The *Execution time* deals with how long it takes for a program to finish execution.
Whereas, *throughput of instructions* measures how many instructions get executed per a given unit of time.

In this article we will focus on the *execution time* of a program as a measure of performance.
The equation used is:

$$CPU execution time = Instructions of program * average Clock cycle per instructions * clock cycle period (1)$$

To use *clock rate*;

$CPU execution time = \frac{Instructions of program * average Clock cycle per instructions}{clock rate} (2)$

where the *average clock cycle per instructions* is called the **CPI**.

Using this equation allows us to separate the factors that impact the CPU execution time.

# The Execution Cycle 

Imagine we are given an *instruction set* **IS** that contains all the instructions our CPU can execute.

The CPU would *fetch* an instruction from memory (this can be an instruction memory), *decode* the instruction in order to set the appropriate state, and *execute* the instruction.

Assuming all instructions take the same amount of time to execute, the time it takes for the CPU to execute an instruction is know as the **clock cycle period**.
The *clock cycle period* is abstracted to the unit **clock cycle**. That is, for every *clock cycle* an instruction gets executed.

The following equation states that all instructions take the same amount of time to execute:

$time(Ins_1) = time(Ins_2) = ... = time(Ins_n)$

where $n$ is the total number of instructions in the instruction set **IS**.

## Introduction of a new Instruction

Say we introduce a new instruction that takes longer than the rest of the insturctions: $Ins_{n+1}$ such that:

$time(Ins_{n+1} > time(Ins_n)$
$...$
$time(Ins_{n+1} > time(Ins_1)$

The **cycle period** will now be $time(Ins_{n+1})$, as it has to accommodate the worst case time in order to complete a *clock cycle*.
Thus the *cycle period* becomes:
$Max(time(Ins_1), ..., time(Ins_{n+1})) (3)$

### Example

Take for example the following instruction set, mapped to the time it takes to execute the instruction:

```
add	    - 400ps
sub	    - 400ps
mul	    - 400ps
jmp	    - 300ps
bne	    - 800ps
addi    - 400ps
subi    - 400ps
```

(The time given is arbitrary.)

The **cycle period** ends up being: $time(bne)$, as it is the worst case of the instructions.

Notice how this slows down all other operations, even though they take less time, as each instruction now takes *800ps* to execute.

# Pipeline Architecture

By using formula (1) we can improve the *CPU execution time* by reducing the *clock cycle period*.
This can be done by taking advantage of *instruction level parallelism* amongst the instructions (**ILP**).

In the *fetch-decode-execute* cycle it is as if the instruction is going through stages.
First going through the *fetch* stage, then the *decode* stage and finally to the *execute* stage.

Instead of only having an instruction going through all the stages per clock cycle, we can have multiple instructions in different stages at once!
Every time a clock cycle occurs, the most recent instruction exits the *execute* phase and the instruction in *decode* phase takes its place.
Same applies to the instruction in the *fetch* phase. Every instruction moves up in a **pipeline**.

The ability to split the instructions into phases is taking advantage of the *instruction-level parallelism* between instructions.
This is possible because we are able to separate the resources of the microarchitecture into phases/sections.

These phases/stages are called **Pipeline Stages**. 
For example, in the *fetch-decode-execute* cycle, we have the following stages:

* Fetch Stage
* Decode Stage
* Execute Stage

Where after every *clock cycle*, an instruction manages to move forward in the pipeline for the next *clock cycle* to begin.

## MIPS Example

In a MIPS architecture, the micro-architecture has five stages:

```
Fetch
Decode
Execute
Memory
Write-Back
```

Notice that there are two new stages in the microarchitecture.
This execution cycle involves *writing/reading* from memory and *writing* the values back to the registers for the next instruction.

## Reducing the Clock Cycle Period.

By splitting the micro-architecture into stages, we are able to decrease the *clock cycle period* to be based on the *longest* pipeline stage.
The reason for basing it on the longest pipeline stage is to ensure that for after every clock cycle, an instruction is executed.

The clock cycle now becomes:

$Max(time(pipelineStage_1), ... , time(pipelineStage_n))$

where $n$ is the total number of pipeline stages.
(The stages that a pipeline architecture would have is dependent on the design of the micro-architecture and the instruction set. More on this later.)

## Example

The following information tells the pipeline stage and the time it takes:


```
Fetch	-	150ps
Decode	-	150ps
Execute	-	200ps
```

The **clock-cycle period** now becomes: *200ps*.
This means that every *clock cycle* takes *200ps* to execute.
Not only did it decrease the *clock cycle period*, but it also increased the **throughput of instructions** by reducing the *clock cycle period*.
As before, for every *800ps* an instruction was executed, but now after every *200ps* an instruction gets executed (assuming the pipeline is full).

This is the *main* benefit of using a pipeline architecture.
This benefit is only achieved if the pipeline is full of instructions, otherwise some clock cycles will be lost in order to fill up the pipeline.
For example, in the beginning of program execution, the pipeline will initially lose instructions. It is only after the pipeline is full that the *pipeline architecture* begins execute an instruction every clock cycle.


## Pipeline Hazards

The ideal pipeline would complete an instruction after every clock cycle.
Unfortunately, ideals are left to ideals, as we have to face certain issues arising when using the pipeline architecture.
These issues are termed **hazards**.
There are *three* main hazards in a pipeline:

* Structural Hazard
* Data Hazard
* Control Hazard

### Structural Hazards

Structural hazards occur when multiple stages in the pipeline architecture are attempting to use the same hardware component.

An example is that of memory in a *Von Neumann Architecture*.
In a *Von Neumann architecture*, instructions and data are stored in the same memory component.

As an example, we are using the 5-stage pipeline of the MIPS micro-architecture.
In the *fetch* stage, the processor is accessing memory to fetch the next instruction.
In the *memory* stage, the processor is either storing or loading data into/from memory.
These two stages are occurring simultaneously, thus causing a *structural hazard*.
(This issue is known as the *Von Neumann Bottleneck*.)

The MIPS architecture solves this by having the instruction be stored in a separate memory component from that of the data.

### Data Hazards

Data-hazards occur due to **data-dependencies** between instructions.

Say we have the following program:

```asm
Ins_1	r2, r1, r1
Ins_2	r3, r1, r2
Ins_3	r9, r0, r0
Ins_4	r1, r0, r0
Ins_5	r4, r5, r6
```

At a point of executing the program above, we get to the following state:

```
FetchStage(Ins_3), DecStage(Ins_2), ExecStage(Ins_1), MemStage(nop), WriteBackStage(nop)
```

(*nop* stands for *no operation*)

What gets decoded in the *DecStage* are old values. Meaning the *DecStage* is not acquiring the executed value from $ExecStage(Ins_1)$ as the value from *ExecStage* has not reached the *write-back* stage.

#### Forwarding or Stalling

There are two ways to solve this hazard.

1. Stall the pipeline until the value in $Ins_1$ has been written back to the registers.
2. Forward the value to the previous stage so that the $Ins_2$ instruction gets access to the updated value.

The first approach causes clock cycles to be lost, as it has to stall the pipeline until the $Ins_1$ instruction has written its value back.

The second approach avoids the loss of clock cycles, and is able to have the pipeline continue as if nothing happened.


### Load-Use Data Hazard
There is another form of data-hazard called: *load-use data hazard*.
This hazard arises in the MIPS architecture when *loading* data from memory.

Take the following program:

```asm
lw	r1, 0($r2)
add	r2, r1, r1
```

This would produce the following Pipeline:

```
FetchStage(ins_{x+2}), DecStage(ins_{x+1}), ExecStage(ins_x), MemStage(add), WriteBackStage(lw)
```

Notice how the *lw* has not been able to pass the value from memory into the registers, and the *add* instruction has passed the *execution* stage.
No amount of *forwarding* will solve this data-dependency.
What needs to be done is stall the pipeline in order to have the register *r1* contain the data from memory.

```
FetchStage(ins_{x}), DecStage(add), ExecStage(lw), MemStage(nop), WriteBackStage(nop)
FetchStage(ins_{x}), DecStage(add), ExecStage(nop), MemStage(lw), WriteBackStage(nop)
FetchStage(ins_{x}), DecStage(add), ExecStage(nop), MemStage(nop), WriteBackStage(lw)
FetchStage(ins_{x+1}), DecStage(ins_{x}), ExecStage(add), MemStage(nop), WriteBackStage(lw)
```

The pipeline has stalled in the *DecStage*. 
(This is not representative of the MIPS architecture. This is only an example of how the pipeline would be stalled. Depending on the microarchitecture, the write back could pass date to the execute stage instaed of passing it to the Decode state.)

#### Compiler Solution
Another solution would be to have the compiler detect *load-use data hazards* and re-arrange the instruction to add instructions in between *lw* and *add* to avoid stalling the pipeline.
This would allow the *add* instruction to reach the *decode* stage just in time to access the updated value from memory.

For example, take the following program:
```asm
lw	r1, 0($r2)
add	r2, r1, r1
mul r3 r6 r7
sub r9 r7 r5
```

THe compiler can re-arrange the instructions as:
```asm
lw	r1, 0($r2)
mul r3 r6 r7
sub r9 r7 r5
add	r2, r1, r1
```

### Control Hazards

Control hazards occur when the processor has to make a decision during execution.
This decision usually involves whether to branch to an instruction or not.

One such occasion is when branching to another instruction:
* Jump
* Branch on Equals

Because the pipeline architecture assumes the next instruction is going to be executed, if there is a branch/jump then the pipeline must be *flushed*.
Flushing the pipeline will cause computation to lose clock-cycles because the pipeline is no longer full.
Execution will begin with the next instruction.

### Instruction Set Design and the Pipeline

The instruction set of the MIPS architecture was specifically designed to be used in a pipeline architecture.
For this reason, the instruction set was designed in such a way as to maintain symmetry amongst instructions, while reducing the amount of instruction categories.

Such *symmetry* reduces the need to implement more circuitry to manage the different instruction formats.

The following characteristics of an instruction set can affect the total stages of a pipeline architecture:
* Variety of bit width amongst instructions
* Symmetry between instructions
* Number of categories
* Memory Alignment (Fetching of data in only one instruction)

### Memory Alignment

A good article about memory alignment is this: https://developer.ibm.com/articles/pa-dalign/

The basic idea is that the processor accesses a number of bytes *per* memory access. 
The number of bytes acquired per memory access is termed the **Memory Granularity**.

A processor with a *1-byte* memory granularity would take *four* memory accesses to fetch 4 bytes of data.
A processor with a *4-byte* memory granularity would take only *one* memory access to fetch 4 bytes of data.

#### Accessing Memory

Say we have the following memory block.

{{< figure src="microarch_learning/1_memory_block.png" alt="Memory_Block">}}

and we access on memory address 0.

{{< figure src="microarch_learning/2_memory_access_aligned.png" alt="Memory_Block">}}

Notice how this access is *memory-aligned*, so we will fetch the desired 4-byte data.

Now what if we decided to access data at memory-address 1.

BLOCK OF MEMORY WANTED:
{{< figure src="microarch_learning/3_memory_access_unaligned.png" alt="Memory_Block">}}

Because the processor accesses memory based on the *memory granularity*, the first block of 4-byte data is accessed, and then the second block data is accessed.

First Block:
{{< figure src="microarch_learning/2_memory_access_aligned.png" alt="Memory_Block">}}

Second Block:
{{< figure src="microarch_learning/4_memory_access_unaligned_second.png" alt="Memory_Block">}}


To then specify the data starting from memory-address 1, Block 1 will be shifted up by one byte, whereas Block 2 will be shifted down by 3.

{{< figure src="microarch_learning/5_memory_block_one.png" alt="Memory_Block">}}

{{< figure src="microarch_learning/6_memory_block_two.png" alt="Memory_Block">}}

These two blocks are the merged to form the desired data block.

{{< figure src="microarch_learning/7_memory_block_merged.png" alt="Memory_Block">}}

Such a procedure was supported by some processors, but it took up space in the architecture.

The MIPS architecture decided to not support *un-aligned memory addresses* and forced *memory-aligned addresses*.

#### Performance Penalty

*How does Memory Alignment affect performance?*
Using the memory from the example above, we are accessing from *memory address* 1.

We would have to do *two* memory accesses, and then do the merging of the blocks to acquire the data.
Where as if the data is *memory-aligned* we just have to do *one* memory access to get the desired data.

Because memory access costs a lot of clock cycles (this is discussed in *cache and memory* article), having a *memory-aligned* architecture affects the CPU execution time.

### Improving Performance

Going back to the CPU execution Time equation:

$CPU execution time = Instructions per program * CPI * clock cycle period$

The pipeline architecture is able to decrease the execution time by reducing the *clock cycle period*.
Another approach for reducing the CPU execution time is to focus on the average clock cycles per instructions (CPI).
This parameter can also be stated as: **Instructions Per Clock Cycle** (IPC).

To reduce CPI (and hence, increase IPC) we use an approach called **Multiple Issue**.

# Multiple Issues

Multiple issues provides the ability to *finish* executing multiple instructions in one clock cycle.
Whereas pipeline allowed to work on multiple instructions at once, but on different stages, Multiple issue allows the processor to work on *multiple* instructions per stage.
Allowing for the processor to finish execution of multiple instructions per clock cycle.

There are two main components to *Multiple issue*:

* Issue Slots
* Issue Packets

An *issue slot* is a specific slot/location that an instruction would occupy based on the properties of the instruction.
For example, the instruction *lw* would be given a memory slot because it accesses memory.
Whereas the instruction *add* would be given a register slot because it utilizes registers.
(This is an oversimplification, but I hope it gets the idea accross.)

The **Issue packets** is what the processor executes per clock cycle.
It is where the instructions are stored and sent.
Within these *issue packets* there are *issue slots* where appropriate instructions would be inserted.

For example, take a 16-bit instruction format with a 32-bit CPU micro-architecture:
If the CPU was Multiple Issue, the instructions would be packed into the 32-bits:

```
Issue Packet
0000 0000 0000 0000 | 1111 1111 1111 1111
```

The micro-architecture would have to implement the circuitry to allow for multiple instructions to be executed.

To demonstrate the use of *issue slots*, say we have the following program:

```asm
lw	
add
add
sw
sub
add
```

This program would be stored in the following *issue packets*:

```
ALU/Branch      Memory
add           | lw	        - Issue Packet
add           | nop	        - Issue Packet
add           | sw	        - Issue Packet
sub           | nop	        - Issue Packet
add           | nop	        - Issue Packet
```

This all works well, but issues arise such as **Data-Dependencies**.

### MIPS Example (From Book)
Say we have the following program:

**TODO**: Go over the hazards when using **MUltiple ISSUE**
	* Loop Unrolling

```asm
Loop:   lw      $t0, 0($s1)         # t0=array element
        addu    $t0, $t0, $s2            # add scalar in $s2
        sw      $t0, 0($s1)         #store result
        addi    $s1, $s1, -4        # decrement pointer
        bne     $s1, $zero, Loop    # branch s1 != 0
```

If we were to naively place them into *issue packets* we would get the following:

```
ALU/Branch      Memory
addu  $t0, $t0, $s2      | lw $t0, 0($s1)   CC-1
addi  $s1, $s1, -4       | sw $t0, 0($s1)   CC-2
bne $s1, $zero, Loop     | nop	            CC-3
```

The first Issue packet would be executing on a value that has not yet been loaded by *lw*!
To fix this issue, the instructions must be re-arranged in order to avoid *data-dependency* hazards.

```
ALU/Branch      Memory
 nop                     | lw $t0, 0($s1)   CC-1
addi  $s1, $s1, -4       |  nop             CC-2
addu  $t0, $t0, $s2      |  nop             CC-3
bne $s1, $zero, Loop     | sw $t0, 0($s1)	CC-4
```
These dependencies means that the *CPI* is not exactly *0.5*, but *0.8*.
Not the desired outcome we would have wanted when data-dependencies occur.

### Loop Unrolling

One way to improve performance is to have the compiler unroll some of the instructions in a loop.
By doing so, there are more instructions to take advantage of *Instruction Level Parallelism* (ILP).
Allowing for interleaving of instructions to reduce the stalling due to data-dependencies.

#### Register Re-naming

By unrolling the loop, the compiler would introduce new instructions containing the same *register name*.
The thing with these new registers is that they contain data that is not dependent from the previous instructions!
That is, they are *independent*. They are only dependent based on the given name, and not on the data.
The compiler will have to *rename* these registers to convey their independence.

## Main Responsibilities

There are main responsibilities when using *multiple issues*:

1.	Package Instructions into *issue slots*.
2.	Deal with *data* and control *hazards*.

There are two ways of doing so:

1.	Static (Compile time)
2.	Dynamic (During execution time)

Although Multiple-Issue decreases the CPI (Increases Instructions Per Cycle IPC), there can some stalling issues due to multiple instructions being executed at once.

### Static Multiple issue

Stalling of execution occurs mostly due to **data-dependencies**.

THe following steps are involved in the *static* approach:
* Re-order instructions
* Loop Unrolling
* Register Renaming


To reduce stalling, the compiler can *re-order instructions* to avoid data-dependency stalls.

*Loop unrolling* creates more instructions that can be executed in parallel.
Allowing for the compiler to re-order the instructions to minimize stalling.

*Register renaming* is done to eliminate "dependencies that are not true data dependencies".
That is, the dependence between instructions was only based on the *name* of the register and not on the data.


### Dynamic Multiple issue

When this approach is used by a processor, the processor is called a: **Superscalar processor**.

The most basic implementation is:
* Issue instructions in order
* Processor decides how many instructions are executed per clock cycle.

This approach can be extended to deal with **pipeline hazards**:

#### Dynamic Pipeline Scheduling

The basic idea is to choose the next instruction to execute *while* avoiding hazards.
To do so, the processor would *re-order*. 
This term is called: **Out-of-Order Execution**.

A basic implementation contains the following:

* Fetch/Decode Unit
* Reservation Stations
* Functional Units
* Commit Unit

The Fetch/Decode Unit would fetch the instructions and decode them.
It would then send the result to a *reservation station*.

A *reservation station* is responsible for holding all the operands and operations.
Once it has all the data (i.e., operands/values and operations) it releases what it has to the *functional unit*.
The *functional unit* is a unit given a specific function to do.
For example, a *functional unit* can deal with:

* Integers
* Floating Points
* Other stuff

Once the value has been calculated, the *functional unit* then sends its result to the *commit unit*.

The *commit unit* is responsible for deciding when to release the values to the *register files* or can send the value to a *reservation stations*.

This method can be used to deal with *cache misses*, as it avoids having to stall the processor while it waits for memory to be added to the cache.

**TODO**: Go over Dynamic Pipeline Scheduling again.

# Conclusion

The CPU execution time is dependent on the following formula:

$CPU execution time = instructions * CPI * clock cycle period$

Using a *pipeline architecture* reduces the *clock cycle period*, whereas *multiple issues* decreases the *CPI*.

So far we have assumed, using the MIPS architecture, that memory access required only one clock cycle.
This assumption will be looked into when dealing with *caches* and the *memory hierarchy*.
