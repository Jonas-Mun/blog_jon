---
title: Experience_linux_setup
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-10-29T23:58:28+01:00
lastmod: 2022-10-29T23:58:28+01:00
featuredVideo:
featuredImage:
draft: false
---

Throughout having installing and setting up environments or workflows, I have noticed a pattern that tends to be followed.

1. First Attempt at installing and setting it up. The end result is something that does not exactly work or has broken dependencies.
2. Second attempt I manage to successfully set-up the system.

The first attempt gives me the chance to familiarize myself with the system and how it works.
Event though the result may be something broken, I at least have an idea of how it works or the things involved.

Once I give up or realize the system does not work correctly, I give myself a day to recover.
Setting up a system requires a lot of patience as sometimes I had no idea why something was not working.
I would at times become clueless as to the thing that went wrong.

Once the break was over (a day past) I would attempt the installation/setup again.
With 'wisdom' gained from my previous experience, I would tend to have a working set-up or something that works a lot better.


This experience taught me and has given me the confidence of being able to set-up almost any environment in Linux.
The next question is whether I am willing to give myself the time and allow myself to muster the patience required to go through this pattern again.
