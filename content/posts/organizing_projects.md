---
title: Organizing Projects
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-19T11:39:03Z
lastmod: 2022-11-19T11:39:03Z
featuredVideo:
featuredImage:
draft: true
---

After going over ThePrimeagen's tmux script, I have decided to re-organize how I setup projects.
This should make it easier to look at my projects via scripts.

# Current Organization

At the moment I have the basic structure:

```
* Dev
    - Programming
        -lang1
            -proj1
            -proj2
            -meta-proj
                -proj1
                -proj2
            -proj3
        -lang2
            -proj1
            -proj2
        -lang3
    - GameProjects
        -game1
        -game2
    - Websites
```

The issue is that I tend to nest projects into a other projects.
The example is *meta-proj*.

The solution would be to have a folder that would encompass the whole project.
The goal is to minimize the number of sub-projects as in *meta-proj*.

But what about projects with more than one language?
I could create a folder called *MultiLangProjects* under Programming for this case.

```
* Dev
    - Programming
        -MultiLangProjects
        -lang1
            -proj1
            -proj2
            -meta-proj
                -proj1
                -proj2
            -proj3
        -lang2
            -proj1
            -proj2
        -lang3
    - GameProjects
        -game1
        -game2
    - Websites
```

Maybe not. I'll keep my initial setup.

I will leave this command here becauuse it joins two outputs into a single pipe:


```
(find ~/Dev/Programming/ -mindepth 1 -maxdepth 2 -type d; find ~/Dev/Game_Projects/ -mindepth 1 -maxdepth 1 ; ) | fzf
```
This allows me to display all projects for languages, as well as all projects for games with different depths.


