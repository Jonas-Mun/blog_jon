---
title: Workflow Improvement - Accessing Projects
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-19T15:58:25Z
lastmod: 2022-11-19T15:58:25Z
featuredVideo:
featuredImage:
draft: false
---

I have been watching ThePrimeagen a lot to better understand how he is able to switch between projects with ease. This aspect of development has always slowed me down, so I finally decided to investigate how ThePrimeagen does it.

# The Tmux-Sessionizer

It all starts with *tmux* and his script *tmux-sessionizer*.
In a past article I went line by line through this script to see how he did it.
By understanding the script I have been able to adjust to my needs and succeeded.

There are two major additions I made:

## Multiple input to a pipe

ThePrimeagen has a directory structure different from mine.
Because of his directory structure, he would only need to travel a depth of one when fuzzy finding a project's directory.
For me I sometimes had to go to 1 depth or 2 depth depending on the directory.

The following code is what was originally in his script:

```bash
    selected=$(find ~/work/builds ~/projects ~/ ~/work ~/personal ~/personal/yt -mindepth 1 -maxdepth 1 -type d | fzf)
```

I changed this to:

```bash
    selected=$( (find ~/Dev/Programming/ -mindepth 2 -maxdepth 2 -type d; find ~/Dev/Game_Projects/ ~/Dev/Website/ -mindepth 1 -maxdepth 1; find ~/Dev/Programming/bash/ -mindepth 0 -maxdepth 0 ; )| fzf)
```

The functionality of interest is:

```bash
(find ~/Dev/Programming/ -mindepth 2 -maxdepth 2 -type d; find ~/Dev/GameProjects/ ~/Dev/Website/ -mindepth 1 -maxdepth 1; find ~/Dev/Programming/bash/ -mindepth 0 -maxdepth 0 ; ) | fzf
```

This line allows me to pass the output of all the commands in the parenthesis *(...)* and pass it to the pipe for *fzf* to use.

## Opening in Command Line or in Tmux

The other addition was having the ability to jump to a project when not in tmux.
Before, the script would only jump to a project if I was in a tmux session, due to the following line:

```bash
tmux switch-client -t $selected_name
```

The command *switch-client* was only possible inside a tmux session.
To remedy this, I used the environment variable *$TMUX* to check whether I was currently in a tmux session or not.

```bash
# We are either in tmux or in command line environment.
if [[ -z $TMUX ]]; then
    tmux attach -t $selected_name
else
    tmux switch-client -t $selected_name
fi
```

If *$TMUX* has nothing, then that means I am not in a tmux session and only attach it.
Otherwise, I use the previous functionality as I am in a tmux session.


# Conclusion

I am loving this setup! I am beginning to feel the POWER!!!! The smoothness of development!
