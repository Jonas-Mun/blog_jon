---
title: Learning Bash Through Scripts
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-18T21:29:40Z
lastmod: 2022-11-18T21:29:40Z
featuredVideo:
featuredImage:
draft: false
---

This article will go over a script made by ThePrimeagen to better understand *bash* and use the script he created.
The script in question is the following:

```bash
#!/usr/bin/env bash

if [[ $# -eq 1 ]]; then
    selected=$1
else
    selected=$(find ~/work/builds ~/projects ~/ ~/work ~/personal ~/personal/yt -mindepth 1 -maxdepth 1 -type d | fzf)
fi

if [[ -z $selected ]]; then
    exit 0
fi

selected_name=$(basename "$selected" | tr . _)
tmux_running=$(pgrep tmux)

if [[ -z $TMUX ]] && [[ -z $tmux_running ]]; then
    tmux new-session -s $selected_name -c $selected
    exit 0
fi

if ! tmux has-session -t=$selected_name 2> /dev/null; then
    tmux new-session -ds $selected_name -c $selected
fi

tmux switch-client -t $selected_name
```

It can be found here: https://github.com/ThePrimeagen/.dotfiles/blob/master/bin/.local/scripts/tmux-sessionizer

# Beginning the analysis

We start with the following block:

```bash
if [[ $# -eq 1 ]]; then
    selected=$1
else
```

We have three questions:
1. What does *$* do?
2. What does *#* mean?
3. What does *-eq* do?
4. What does *$1* mean?

Fomr the link: https://askubuntu.com/questions/939620/what-does-mean-in-bash
*$* is used to reference a variable, and *#* is used to acquire the total arguments given.

That answers questions 1 and 2.
As for question 3: It is the comparison for equality
https://kapeli.com/cheat_sheets/Bash_Test_Operators.docset/Contents/Resources/Documents/index

For question 4: It acquires the 1st argument passed to the script.

--
We now move to the *else* part:

```bash
    selected=$(find ~/work/builds ~/projects ~/ ~/work ~/personal ~/personal/yt -mindepth 1 -maxdepth 1 -type d | fzf)
fi
```

We will disect this line into the following questions:
1. What does *find* do?
2. What does *-type d* do?
3. How does *fzf* work?
4. How does the *$* work with the values?

## find

To the docs!

```
$ man find
```

From the docs:
```
find - search for files in a directory hierarchy
```

Alright, so we look for files in a directory. So what do the other options means:
* -mindepth 1 -> Don't apply actions at depth less than 1.
* -maxdepth 1 -> Descend at most *1* level

Ok, so now we check *type -d*:

*d* stand for directory, so I assume it only focuses on directories.

So we got the *find* command done, now we move to the third question.

```
man fzf -> A command-line fuzzy finder
```

When we run *fzf* on its own, it prints out ALL the directories, but we are able to search through the lists; hence the name *fuzzy finder*.

So the *find* command narrows down the paths to search and passes it to *fzf* which then allows us to select the directory we want to use.

Now we go to the fourth question.
To test this we will create a scrip that will ask for input from the user.

### Ask input from user

We use *read* to ask for input from users:
```bash
read var1
```

Input script(ins.sh)
```bash
read var1

echo $var1
```

Output Scripts (out.sh)
```
var = $(sh ins.sh)
echo $var
```

```
$ ./out.sh
```

Doing this, we are expect to enter input due to our input script.
And when we input it, we get an output!

So then the following line:
```bash
    selected=$(find ~/work/builds ~/projects ~/ ~/work ~/personal ~/personal/yt -mindepth 1 -maxdepth 1 -type d | fzf)
```

Passes the filtered directories to *fzf*, where it awaits use input to select a directory using fuzzy finder, and stores it in *selected* variable.

We now move to the next block

## Block 2

```bash
if [[ -z $selected ]]; then
    exit 0
fi
```

We have two questions:
1. What does *-z* mean?
2. Why exit with *0*?

To answer (1) we go to the docs!
```
$ man bash
/-z (in vim)
```

So it expects a string and returns:
```
True if the length of string is zero
```

For question (2), we remain in the docs:
```
/exit (in vim)
```

Under the **EXIT STATUS** we realize that *exit* gives the status after executing the script.
```
"a command which exists with a zero exit status has succeeded."
```
Interesting. Although it did not receive the desired input, it still acts as if it succeded.
Perhaps this should be exit 1?

Anyway, this block of code ensures that we continue with a *selected* path.

## Block 3

```bash
selected_name=$(basename "$selected" | tr ._)
tmux_running=$(pgrep tmux)
```

We have three questions to start with:
1. What does *basename* do?
2. What does *tr* do?
3. What does *._* signify for *tr*?

### basename

From the docs themselves:
```
$ man basename
Print NAME with any leading directories removed.
```

So what means if *selected* was */home/mine/dude/text.js*:
```
$ basename "/home/mine/dude/text.js"
-> test.js
$ basename "/home/mine/dude/"
-> dude
```

Ok, so we extract the last one.

Now what about question (2)?

### tr

```
$ man tr
-> translate or delete characters.
```

After playing around with *tr . _*: it essentially checks ALL characters for *.*, and replaces it wtih *_*.

```bash
$ tr . _
asd.fds ->asd_fds
hello dude -> hello dude
```

With this knowledge, then the first line of the block takes the last name of the path, and replaces all *dots* with *_*.

This answers all our initial questions.

Now what about:

```bash
tmux_running=$(pgrep tmux)
```

We have one question:
1. What does *pgrep* do?

### pgrep
To the docs!

```bash
$ man pgrep
```
From the docs, it *lists* the PIDS of running proccesses from the given program.
SO in this case, it checks the available processes of tmux.

## Block 4

```bash
if [[ -z $TMUX ]] && [[ -z $tmux_running ]]; then
    tmux new-session -s $selected_name -c $selected
    exit 0
fi
```

We have four questions:
1. What is *$TMUX*?
2. What does *tmux new-session* do?
3. WHat does *-s* do?
4. What does *-c* do?

For question 1, I imagine an environment variable is defined as *$TMUX*.
In fact, if we do the following:
```bash
$ tmux
$ (tmux) echo $TMUX
```

We get information of the tmux session.
So if there are no tmux running, then we run the following:
```bash
    tmux new-session -s $selected_name -c $selected
```

*new-session*: creates new session with session-name.

From the docs we gather:
* -s => session name
* -c => start directory

So the session name is the last name in the path, and the starting directory is the one obtained in the beginning.
This ONLY applies if there are no tmux sessions running.

## Block 5

```bash
if ! tmux has-session -t=$selected_name 2> /dev/null; then
    tmux new-session -ds $selected_name -c $selected
fi

tmux switch-client -t $selected_name
```

The first line has a lot:
1. What does *tmux has-session* work?
2. Why have *2> /dev/null*?

### tmux has-session

As always, to the docs!

```
$ man tmux
-> Report an error and exit with 1 if the specified session does not exist.
If it does exist, exit with 0.
```
The *-5* is the *target session* specified by the given name.
In this case it is *selected_name*.

Now we go to the following: *2> /dev/null*
From the following Stackoverflow link: https://unix.stackexchange.com/questions/581240/what-is-the-use-of-script-2-dev-null-in-the-following-script

```
2> /dev/null meanst that, redirect the error output from this command to /dev/null.
```

So if no session is found, then it skips it due to the initial *!*.
Otherwise, if a session has been found, run:

```bash
    tmux new-session -ds $selected_name -c $selected
```
We now have a new question:
1. What does *-d* do?

I do not know. From reading, It mentioned something about *default size*.
Oh, after doing some testing I discovered that *-d* creates a tmux session, but does not attach instantly:

```
$ (tmux) tmux new-session -s "test" => sessions should not be nested...
$ (tmux) tmux new-session -ds "test"
$ (tmux) tmux ls => test
```

So it creates a *tmux sessions* but does not run it from the current tmux session.

The last line:
```bash
tmux switch-client -t $selected_name
```

is run. From what I gather, it switches to the tmux session with the given name in *selected_name*.
So if no tmux session with the name has been created, it is created first in order to run this command.

Let us now customize this for our personal use.
Thank you, ThePrimeagen
