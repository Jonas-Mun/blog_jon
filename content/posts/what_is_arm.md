---
title: What is Arm?
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-06T14:55:58Z
lastmod: 2022-11-06T14:55:58Z
featuredVideo:
featuredImage:
draft: true
---

Arm is a company that designs the architecture of chips.
These chips can be CPUs and GPUs or other specific chips.
It is these designs that are licensed to companies who can use the chap design in their own chip.

Arm gives guarantees that the behaviour of their designs will be met.
By having licensed Arm's design, a company can integrate the arm chip into their overall chip based on their requirements.

There are three types of IP:

* Hard IP Core - The circuit design is physical
* Firm IP Core - Provides more flexibility to be connect it to other modules and FPGAs
* Soft IP Core - Customized to map the desired chip. Can be integrated with a lot of modules.
