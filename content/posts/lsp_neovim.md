---
title: LSP and Autocomplete in Neovim
description:
toc: true
authors: ["Jonas"]
tags: ["vim","setup"]
categories: []
series: []
date: 2021-07-31T22:07:38-05:00
lastmod: 2021-07-31T22:07:38-05:00
featuredVideo:
featuredImage:
draft: false
---

My basic setup to have LSP installed in my Neovim.

# LSP-Config
The first step is to install the <a href="https://github.com/neovim/nvim-lspconfig">nvim-lspconfig</a> plugin.

I installed it using Pathogen, cloning lsp-config under *.config/nvim/bundle/*

Following the <a href="https://neovim.io/doc/user/lsp.html">Neovim Doc</a> made things simpler. Especially the **Quickstart** section.

## Language Server
Once you have the **nvim-lspconfig** plugin installed, you will have to get the LSP for your desired language. Follow the **Quickstart** section in the **Neovim Doc**.

This can vary depending on the programming language.

## Setting up Client
To set up the client, you will have notify in your init.vim which language you want to configure. This is a file config that comes with **nvim-lspconfig**. <a href="https://github.com/neovim/nvim-lspconfig/tree/master/lua/lspconfig">Language Config</a>

Once you know the name of the file config, a command must be included in your *init.vim* files to set it up.

```
lua require('lspconfig').<language-config>.setup{}
```

The *language-config* is the name of the config file for the specific language.

Example:
```
lua require('lspconfig').gdscript.setup{}
lua require('lspconfig').rust_analyzer.setup{}
```

## LSP Colors

To have errors and warnings be of a different color, make sure you define your colorscheme before setting up the LSP server.

As well as linking the following groups into the syntax:

vim
```
highlight! link LspDiagnosticsDefaultError = <group>
highlight! link LspDiagnosticsFloatingError = <group>
highlight! link LspDiagnosticsSignError = <group> 

highlight! link LspDiagnosticsDefaultWarning <group>
highlight! link LspDiagnosticsFloatingWarning <group>,
highlight! link LspDiagnosticsSignWarning <group>

highlight! link LspDiagnosticsDefaultHint <group>
highlight! link LspDiagnosticsFloatingHint <group>
highlight! link LspDiagnosticsSignHint <group>

highlight! link LspDiagnosticsDefaultInformation <group>
highlight! link LspDiagnosticsFloatingInformation <group>
highlight! link LspDiagnosticsSignInformation <group>

highlight! link LspDiagnosticsUnderlineError <group>
highlight! link LspDiagnosticsUnderlineHint <group>
highlight! link LspDiagnosticsUnderlineInfo <group>
highlight! link LspDiagnosticsUnderlineWarning <group>

```
# Autocomplete
Once the LSP for the desired language is set up, you can now begin to implement the autocomplete plugin in neovim.

For this I installed <a href="https://github.com/hrsh7th/nvim-compe">nvim-compe</a>.

Once it is installed, a config file will have to be created. I have the following directory setup:
* .config/nvim/lua/plugins/

It is under the *plugins* folder I created where the config file will be. The config file is: **compe-config.lua**

.config/nvim/lua/plugins/compe-config.lua
```
vim.o.completeopt = "menuone,noselect"

require'compe'.setup {
  enabled = true;
  autocomplete = true;
  debug = false;
  min_length = 1;
  preselect = 'enable';
  throttle_time = 80;
  source_timeout = 200;
  resolve_timeout = 800;
  incomplete_delay = 400;
  max_abbr_width = 100;
  max_kind_width = 100;
  max_menu_width = 100;
  documentation = {
    border = { '', '' ,'', ' ', '', '', '', ' ' }, -- the border option is the same as `|help nvim_open_win|`
    winhighlight = "NormalFloat:CompeDocumentation,FloatBorder:CompeDocumentationBorder",
    max_width = 120,
    min_width = 60,
    max_height = math.floor(vim.o.lines * 0.3),
    min_height = 1,
  };

  source = {
    path = true;
    buffer = true;
    calc = true;
    nvim_lsp = true;
    nvim_lua = true;
    vsnip = true;
    spell = true;
    tags = true;
    ultisnips = false;
    luasnip = true;
  };
}

local t = function(str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

local check_back_space = function()
    local col = vim.fn.col('.') - 1
    if col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
        return true
    else
        return false
    end
end

-- Use (s-)tab to:
--- move to prev/next item in completion menuone
--- jump to prev/next snippet's placeholder
_G.tab_complete = function()
  if vim.fn.pumvisible() == 1 then
    return t "<C-n>"
  elseif vim.fn.call("vsnip#available", {1}) == 1 then
    return t "<Plug>(vsnip-expand-or-jump)"
  elseif check_back_space() then
    return t "<Tab>"
  else
    return vim.fn['compe#complete']()
  end
end
_G.s_tab_complete = function()
  if vim.fn.pumvisible() == 1 then
    return t "<C-p>"
  elseif vim.fn.call("vsnip#jumpable", {-1}) == 1 then
    return t "<Plug>(vsnip-jump-prev)"
  else
    -- If <S-Tab> is not working in your terminal, change it to <C-h>
    return t "<S-Tab>"
  end
end

vim.api.nvim_set_keymap("i", "<Tab>", "v:lua.tab_complete()", {expr = true})
vim.api.nvim_set_keymap("s", "<Tab>", "v:lua.tab_complete()", {expr = true})
vim.api.nvim_set_keymap("i", "<S-Tab>", "v:lua.s_tab_complete()", {expr = true})
vim.api.nvim_set_keymap("s", "<S-Tab>", "v:lua.s_tab_complete()", {expr = true})
```

With that, the autocomplete should work upon restarting neovim.


# References
All this information I managed to obtained by viewing the following videos in Youtube:
* <a href="https://www.youtube.com/watch?v=NXysez2vS4Q&t=993s">ChrisAtMachone</a>
* <a href="https://www.youtube.com/watch?v=tOjVHXaUrzo&t=188s">The Primeagan</a>

# GDScript
If you want to have gdscript working, you will have to download: **netcat** to setup the client. <a href="https://github.com/neovim/nvim-lspconfig/issues/131">Github Info</a>

Make sure you have Godot opened in the project you want to work on.

Special thanks to them.
