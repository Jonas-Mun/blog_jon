---
title: Exploring Neovim's API Through Harpoon
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-16T20:36:35Z
lastmod: 2022-11-16T20:36:35Z
featuredVideo:
featuredImage:
draft: false
---

Welcome to part two of exploring Neovim's API through Harpoon.
In this article we will be exploring the following files:

1. init.lua
2. mark.lua

# init.lua

The first vim functions we encouter are:
```lua
local config_path = vim.fn.stdpath("config")
local data_path = vim.fn.stdpath("data")
```

If we recall, *stdpath* returns the complete path based on the given type.
The type in this case is given as a string.

Next we are introduced to a lua string function that places a given variable inside a string:

```lua
local user_config = string.format("%s/harpoon.json", config_path)
local cache_config = string.format("%s/harpoon.json", data_path)
```

From this we gather that the data stored in *harpoon.json* is placed in their respective folders based on config and data.

If you have used harpoon before, go to the *data path* and you will find a file called *harpoon.json* that has stored the marks.

Ok, now we move to the creation of an *autogroup*:

```lua
local the_primeagen_harpoon = vim.api.nvim_create_augroup(
    "THE_PRIMEAGEN_HARPOON",
    { clear = true }
)
```

This brings two questions:
1. What is an autogroup?
2. How is it created with neovim's API?

To answer the first question, let us delve into the docs!
```
:h augroup
```

From the docs it is a group of *autocommands* that gets executed under the group name.
So what is an *autocommand*??
To the docs!

```
:h autocommand
```
From the docs it is a command that gets automatically executed based on certain events.
These events can be:
* File Read
* File Write
* Buffer Open
* etc...

Ok, this answers question 1. Now how about question 2?
To the docs!

```
:h nvim_create_augroup()
```
So it either gets or creates and *augroup*. What does *clear* mean??
From the man himself: TJDevriees!

LINK:


So no we know that there is an *autogroup* called **THE_PRIMEAGEN_HARPOON**.

## Creating an autocommand
This group exists, but it has no commands.
No worries, the following function fixes this:

```lua
vim.api.nvim_create_autocmd({ "BufLeave, VimLeave" }, {
    callback = function()
        require("harpoon.mark").store_offset()
    end,
    group = the_primeagen_harpoon,
})
```

What do-to the docs!

```
:h nvim_create_autocmd()
```

Alright, so the command is going to execute when the following events occur:
* BufLeave   - execute before leaving to another buffer/leaving or closing current window
* VimLeave   - Before exiting vim.

THe action to do is stored in *callback*.
In this case it will *store_offset* using functionality in *marks.lua*. We will explor this file afterwards.
It is added to the group: **THE_PRIMEAGEN_HARPOON**.


Alright, with the setup mostly done, we advance to the next information!
Oh, wait, ThePrimeagen left us some comments.
```lua

--[[
{
    projects = {
        ["/path/to/director"] = {
            term = {
                cmds = {
                }
                ... is there anything that could be options?
            },
            mark = {
                marks = {
                }
                ... is there anything that could be options?
            }
        }
    },
    ... high level settings
}
--]]
```

This structure is similar to the *harpoon.json* data.

## HarponConfig

This baby here is interesting:
```lua
HarpoonConfig = HarpoonConfig or {}
```
My guess is that this is a global variable that can change from somewhere in the script.
If it has not been already filled, then it starts as an empty table.
We must keep an eye on this baby here. I have a feeling it will be crucial (or not).

## merge_table_impl(t1, t2)

Let us step into this function, line by line:
```lua
    for k, v in pairs(t2) do
```
So *t2* is expected to be a table, so we traverse through it.
```lua
        if type(v) == "table" then
```
If table *t2* stores a table as a value, then...

```lua
            if type(t1[k]) == "table" then
```
If the *key* used in *t2* also contains a table in *t1*, then...

```lua

                merge_table_impl(t1[k], v)
```
merge these two table together.
else...
```lua
                t1[k] = v
```
map the key *k* in *t2* to the value *v* in table *t1*.
...
From what I gather, the table in *t2* would replace whatever value key *k* pointed to in table *t1*.
Replacing it with the table *v* in *t2*.


For the last else, we get:

```lua
            t1[k] = v
```
Applies if the value *v* is not a table. So map *k* to *v* in table *t1*.
Samething, whatever value *k* pointed to in *t1* is replaced by *v* from *t2*.

### What does it do?
It merges *t2* into *t1*, where if the key *k* is a table in *t1*, it merges it with the value *v* if it also is a table.
Otherwise, it just replaces the value in *t1[k]* with the value *v* from *t2*.

I guess it assumes that *t1* does not store the same keys as *t2*, unless it is a table as well.

## mark_config_key(global_settings)

We begin with the first line:
```lua
    global_settings = global_settings or M.get_global_settings()
```

So, if *global_settings* is *nil* then get the on from *M.get_global_settings()*.

Interesting choice. Let us delve into *M.get_global_settings()*

### M.get_global_settings()

Well well well, look what we have here:
```lua
function M.get_global_settings()
    log.trace("get_global_settings()")
    return HarpoonConfig.global_settings
end
```

*HarpoonConfig*... We meet again.

So if there are no global settings defined in *HarpoonConfig*, then we have nothing.

back to the function, we now get the following branch:

```lua
    if global_settings.mark_branch then
        return utils.branch_key()
    else
        return utils.project_key()
    end
```

So *mark_branch* is a field we must take into consideration.

Regardless, there are two options:

* utils.branch_key()    -> Either global settings and mark_branch
* utils.project_key()   -> No global settings or mark_branch

What did these bad boys do?

Well, this sure is interesting...

from branch_key()
```lua
    if branch then
        return vim.loop.cwd() .. "-" .. branch
    else
        return M.project_key()
    end
```

If we recall, the function *branch_key* takes the current branch, if any.
If no *branch* is found then it returns the current working directory/*project_key*.

Now if we go back to *mark_config_key*, then if the global settings has a *mark branch*, we return the directory alongside the branch name, otherwise we return just the current working directory.

### CONCERN
From here I assume that global settings can be created without a tie to a *branch* despite a git branch being used.
This could be the case when first running Harpoon.

We will keep a close eye on this.

The concern is that they both depend on functionalities that return similar values.

## merge_tables(...)

I assume we have a lot of tables that we want to merge together.
We get the following:

```lua
    local out = {}
    for i = 1, select("#", ...) do
        merge_table_impl(out, select(i, ...))
    end
```

We now have two questions:
1. What does *select* do?
2. Why is there and "#" in the first *select*?

To the docs!

### select

To the DOCS! https://www.tutorialspoint.com/select-function-in-lua-programming

There are two ways of using it:

```lua
select(1,'a','b','c') => a b c
select(2,'a','b','c') => b c
```

So the function *select* is due to the variable arguments

```lua
select("#",'a','b','c') => 3
```
In the above case, *#* prints out the total number of arguments.
Aha! We got it, lads and lassies!

Now we go back to the original code:

```lua
    local out = {}
    for i = 1, select("#", ...) do
        merge_table_impl(out, select(i, ...))
    end
```

So the for loop iterates as many times as there are arguments.
Now what is confusing to me is the following:

```lua
        merge_table_impl(out, select(i, ...))
```

Based on our example, using *i* selects all arguments starting from the *ith* position.
Since *merge_table(t1,t2)* takes two arguments, I assume the first value is used and the rest is ignored.
Let us test this out.

```lua
local function me_two(x, y)
    print(x)
    print(y)
end

me_two(1,2,34,5)
```

We get the following:
```
1
2
```
I did get a warning:
```
This function expects a maximum of 2 argument(s) but insteas it is receiving...
```

Ok, so this confirms my theory.

### What does it do?

It receives a variable amount of *tables* and merges them together, using *merge_table_impl*.

## ensure_correct_config(config)

So from the name, this checks that *config* is correct.

(I miss types... It would make it easier to know what da hell each function expects to use and the fileds it has.)

We gather that *config* has the following:
* projects
* global_settings

From the *global_settings* we get a thing called a *mark_key*.
The *mark_key* is used in projects.

Question:
* Is the *mark_key* an index to a *mark*?

If there is no *mark_key* in projects, then a default empty one is created:

```lua
        projects[mark_key] = {
            mark = { marks = {} },
            term = {
                cmds = {},
            },
        }
```

This means => Mark is its own **Data Type**.
We will keep this in mind when we decide to refactor things.

From this, a basic table is given for the *mark_key*.

As we advance, we take this table and test it.

```lua
    local proj = projects[mark_key]
    if proj.mark == nil then
        log.debug("ensure_correct_config(): No marks found for", mark_key)
        proj.mark = { marks = {} }
    end

    if proj.term == nil then
        log.debug(
            "ensure_correct_config(): No terminal commands found for",
            mark_key
        )
        proj.term = { cmds = {} }
    end
```

These two blocks of code check if the appropriate fields are in the project data from the *mark_key*.

Afterwards we go deep into *marks*!!!

```lua
    local marks = proj.mark.marks
```

We then move to the next block of code:

```lua
    for idx, mark in pairs(marks) do
        if type(mark) == "string" then
            mark = { filename = mark }
            marks[idx] = mark
        end

        marks[idx].filename = utils.normalize_path(mark.filename)
    end
```

THe first *if* would revalue *mark* onto a table with field *filename*, and replaces it in the *marks* table.

With this new-found *filename*, it is then ... Oh my. Does it use the recently changed *mark*??
What is, it no longer uses *mark* as a *string* but as a table.

(**LOOK INTO**: This is a nit-pick but maybe we could refactor it)

```lua
        marks[idx].filename = utils.normalize_path(mark.filename)
```

Ok, so the filename is normalized. Remember: *marks[idx]* is the *mark* value.
(**LOOK INTO**: Is this correct? If so, why use different names?)

Ok, so here we can test how it works:
(from utils.normalize_path)

```
print(Path:new("/path/to/file/my_file.lua"):make_relative("/path/to/file/"))
=> my_file.lua
```
Aha! So it cutes the directory and focus solely on the filename.
That's what *normalize* means.

Thus the following:
```lua
        marks[idx].filename = utils.normalize_path(mark.filename)
```
usually stores only the filename.
It then finishes by returning the *config*.

### Concerns

Does the *config* get update for all uses afterwards, or is it copied thus creating two different versions of config.

### What does it do?

It either creates a starting *config* or ensures the values in *config* are correct.

In this, we realize that this *config* may be its own data type, alongside *mark*, *marks*, and *term*.

## expand_dir(config)

This is another function that deals with *config*.
It focuses on *projects* stored in the *config*.

(**LOOK INTO**: How is a *config* created/started?)

```lua
    local projects = config.projects or {}
    for k in pairs(projects) do
        local expanded_path = Path.new(k):expand()
        projects[expanded_path] = projects[k]
        if expanded_path ~= k then
            projects[k] = nil
        end
    end
```

So in this *for* loop we solely focus on *keys* and not on values.
We get one question:

1. What does Path.new(k) do?
2. What does Path.new(k):expand() do?

For question (1), I assume it takes some string representing a path and creates some *object* out of it.
This is because:
```lua
print(Path.new("my/path/tesd.txt")) => "my/path/tesd.txt"
```

Now using the example does nothing with the following:
```lua
print(Path.new("my/path/tesd.txt"):expand()) => "my/path/tesd.txt"
```

but if we use: "~/my/path/tesd.txt":
```lua
print(Path.new("~/my/path/tesd.txt"):expand()) => "/home/me/my/path/tesd.txt"
```

So it *expands* the given path. I imagine *Path* provides this functionality somehow.

We now go on to the following line:

```lua
        projects[expanded_path] = projects[k]
```

So the *full-path* is used as a key for the *relative-path* project.

(**LOOK INTO**: Look how all the data is stored in this *config*)

Now lets investigate the following if:

```lua
        if expanded_path ~= k then
            projects[k] = nil
        end
```

So if the expanded path is not the same as the *key*, then set the key to empty.
Why is this the case?
If they are equal, then we keep it. Keeping it is that same as the previous line of code.

(**LOOK INTO**: Why must the relative path be deleted?)

This returns the *config*.

### Concerns

* We need to look into how *config* is built and the possible operations to manipulate them.

### What does it do?
It expands all the given paths from the *config*, and removes them from the *config* database.


## M.refresh_projects_b4update()

This function starts with log, but we will focus on the second block of code:

```lua
    -- save current runtime version of our project config for merging back in later
    local cwd = mark_config_key()
    local current_p_config = {
        projects = {
            [cwd] = ensure_correct_config(HarpoonConfig).projects[cwd],
        },
    }
```

Ok, so what was *mark_config_key()*?
It would return the *current working directory* or the directory with *branch*.
Just as *cwd* but it may have *branch* name on it.
It then creates the *project* config.

Aha! One of the first uses of *HarpoonConfig*. It IS a *config*.
So it stores the value of the project in *cwd* in the current config.

As the comment has suggested:

```lua
    -- erase all projects from global config, will be loaded back from disk
    HarpoonConfig.projects = nil
```

I imagine all projects are 

(**IDEA**: We can seperate the files into: *runtime* and *data type*. *runtime* would store the *HarpoonConfig* and do all the setup/scripts needed. Whereas *data type* would contain all functionality available.)

Here we arrive at a concern: It uses a *global variable*. Not ideal as it creates side-effects and can be hard to debug.

Let us jump to the next block of code:

```lua
    -- this reads a stale version of our project but up-to-date versions
    -- of all other projects
    local ok2, c_config = pcall(read_config, cache_config)

    if not ok2 then
        log.debug(
            "refresh_projects_b4update(): No cache config present at",
            cache_config
        )
        c_config = { projects = {} }
    end
```
Alright, so we have one question:
1. What does *pcall* do?

To the docs!

So:
```
pcall takes a function, and runs with the given arguments.
```

ALright, so it runs *read_config* with the *cache_config* directory
So what does it return?

* Status code - true if success, false if error.
* result

With this in mind we see that *c_config* will be given a *default* value.

Now lets see the next two functionalities:
```lua
    -- don't override non-project config in HarpoonConfig later
    c_config = { projects = c_config.projects }

    -- erase our own project, will be merged in from current_p_config later
    c_config.projects[cwd] = nil
```

So we would pass projects onto a new value.
No we erase the project in *c_config* of the *cwd*.

This sounds convoluated...I'll have to look into it.
(**LOOK INTO**: Look how this works.)

With this, we move to the next block of code:

```lua
    local complete_config = merge_tables(
        HarpoonConfig,
        expand_dir(c_config),
        expand_dir(current_p_config)
    )
```

Now we merge all the configs, with *expanded* filenames.
All that is stored in *complete_config*.

Then we move to the last blocks:

```lua
    -- There was this issue where the vim.loop.cwd() didn't have marks or term, but had
    -- an object for vim.loop.cwd()
    ensure_correct_config(complete_config)

    HarpoonConfig = complete_config
```

So it makes sure it is correct, and it updates *HarpoonConfig*.

(**ISSUE**: global variable used.)

From what I am gathering, *HarpoonConfig* is the main data structure used.

### What does it do?

It focuses on the *current working directory* projects.
Reads the cache information from the config cache file, and stores them.
Removes the cached project data, then merges all data from:
* HarpoonConfig
* cache data projects
* current working directory

and stores it all in *HarpoonConfig*.
I'll have to play around with it more to better grasp what it does.
Hopefully once I have outlined all the data types used and their operations.

We no go to the function *read_config* that was used...

## read_config(local_config)

This has one main line:

```lua
    return vim.fn.json_decode(Path:new(local_config):read())
```

We now have two questions:
1. What does *vim.fn.json_decode* do?
2. What does *read* do?

For question (1), we go to the docs!
```
:h json_decode
```

So it takes the expression from JSON object into:
* Dictionaries
OR
* String
Depending on the expression.

SO for question (2):
Does it return a string of the file??

Let us test it with the following code:
```lua
print(Path:new("my_test.txt"):read())
```

It sure does!!
It outputted the contents of my file.

Now lets use the following file with content:
```
{
    "marks": [1,2,3,4]
}
```

```lua
print(vim.inspect(vim.fn.json_decode(Path:new("my_test.txt"):read())))
```

We get:
```
{
   marks: {1,2,3,4} 
}
```

### What does it do?

It reads a file with JSON data and converts it into a data-structure in Lua.

## M.save()

This function first updates *HarpoonConfig* under the current working directory,
and writes the data in the *cache_config*:

```lua
    -- first refresh from disk everything but our project
    M.refresh_projects_b4update()

    log.trace("save(): Saving cache config to", cache_config)
    Path:new(cache_config):write(vim.fn.json_encode(HarpoonConfig), "w")
```

### What does it do?

It just writes the data in *HarpoonConfig* into the *cache_config* file in a JSON format.

## M.setup()

The initial observation is the following pattern:

```lua
    local ok, u_config = pcall(read_config, user_config)

    if not ok then
        log.debug("setup(): No user config present at", user_config)
        u_config = {}
    end

    local ok2, c_config = pcall(read_config, cache_config)

    if not ok2 then
        log.debug("setup(): No cache config present at", cache_config)
        c_config = {}
    end
```
Here we are reading data and storing it.
But we check if the data was read well.

(**LOOK INTO**: See how this can be refactored)

Next we move on to the main section:

```lua
    local complete_config = merge_tables({
        projects = {},
        global_settings = {
            ["save_on_toggle"] = false,
            ["save_on_change"] = true,
            ["enter_on_sendcmd"] = false,
            ["tmux_autoclose_windows"] = false,
            ["excluded_filetypes"] = { "harpoon" },
            ["mark_branch"] = false,
        },
    }, expand_dir(c_config), expand_dir(u_config), expand_dir(config))
```

THis bad boy merges the tables together with the data read from the files.

Then we reach the final stage:

```lua
    -- There was this issue where the vim.loop.cwd() didn't have marks or term, but had
    -- an object for vim.loop.cwd()
    ensure_correct_config(complete_config)

    HarpoonConfig = complete_config
    log.debug("setup(): Complete config", HarpoonConfig)
    log.trace("setup(): log_key", Dev.get_log_key())
```

Where the runtime *HarpoonConfig* is updated.

### Concerns

It uses a global variable to update.

### What does it do?

It merges config, cached/stored data and into the runtime data: *HarpoonConfig*.
It what begins the running of the plugin.

## Runtime Functions

The following functions are based on the runtime data structure: *HarpoonConfig*:
* M.get_term_config()
* M.get_mark_config()
* M.get_menu_config()
* M.print_config()
* M.get_global_settings()

## HarpoonConfig

So what is this *runtime HarpoonConfig*?

```lua
print(vim.inspect(HarpoonConfig))
---
{
    global_settings ={
        enter_on_sendcmd = false,
        excluded_filetypes = { "harpoon" },
        mark_branch = false,
        save_on_change = true,
        save_on_toggle = false,
        tmux_autoclose_windows = false
    },
    projects = {
        [<cwd_0>] = {
            mark = {
                marks = { {
                    col = 0,
                    filename = "<filename_0>",
                    row = 15
                },  {
                    col = 0,
                    filename = "<filename_1>",
                    row = 3
                } }
            },
            term = {
                cmds = {}
            }
        },
        ....
        [<cwd_n>] = {
            mark = {
                marks = { {
                    col = 0,
                    filename = "<filename_0>",
                    row = 2
                },  {
                    col = 0,
                    filename = "<filename_1>",
                    row = 4
                } }
            },
            term = {
                cmds = {}
            }
        }
    }
}
```

## Conclusion on init.lua

There is a lot of dependence on global variables being changed in functions.
This is especially the case with *HarpoonConfig*.

My guess is we can seperate the functions that have almost no side-effects, and then have a section that deals with the runtime data known as *HarpoonConfig*.


With this knowledge in hand, we are ready to delve into marks.lua!

Before we go on to *mark.lua* we will give an overview of the core data type used in *init.lua*, *utils.lua* and *dev.lua*.

