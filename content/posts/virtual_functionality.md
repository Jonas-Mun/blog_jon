---
title: Virtual Functionality C++
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-26T16:26:37Z
lastmod: 2022-11-26T16:26:37Z
featuredVideo:
featuredImage:
draft: false
---

This article will delve into *virtual functions* which allow *Polymorphism* to become possible.

Saw we have the following class:

```cpp
class Animal {
    protected:
        std::string m_name;

        Animal(std::string name) : m_name(name) {

        }

        // to prevent slicing (covered later)
        Animal(const Animal&) = default;
        Animal& operator=(const Animal&) = default;
};


class Cat : public Animal {
    Cat(std::string name) : Animal(name) {
    }

    std::string speak() const { return "Meow"; }
};
```

and call them as follows:
```cpp
int main()
{
    const Cat cat{"Fred"};
    std::cout << "cat is named " << cat.getName() << "and says, " << cat.speak() << '\n';

    const Animal* anim(&cat);
    std::cout << "Panimal is named: " << anim->getName() << "and says, " << anim->speak() << '\n';
}
```

By running the program above we would get:
```
cat is named Fredand says, Meow
Panimal is named: Fredand says, ???
```

The reason is because *anim* is a pointer of *Animal*. It can only see the portion of *Animal*, which is why it calls the *Animal* speak.
Whereas in *cat*, it has access to the cat's *speak* function.

The question becomes:

*How can we have the base pointer call the derived classes's function?*

## Virtual Functions and Polymorphism!!

A vrtual function, when resolved, calls the most-dertived function.

We have the following code:

```cpp
#include <iostream>
#include <string>

class Base {
    public:
        virtual std::string getName() const { return "Base"; }
};

class Derived : public Base {
    virtual std::string getName() const { return "Derived"; }
};

int main() {
    Derived derived = Derived();
    Base& rbase(derived);
    std::cout << "RBase is a " << rbase.getName() << '\n';
}
```

The output would be:
```
RBase is a Derived
```

The reason is because having called a function *virtual*, we tell the program to go look for any *derived versions* of the function.
If there is, then we get the resoled value.

Note how we ensure that *rbase* is a *reference* to a *derived* class.

IMPORTANT NOTE:
The derived class function must have a matching signature and return type. Otherwise the function would not be mapped to the base virtual function.

## Override 

Why use the term *override*.
Take the folowing example, that shows how important having matching signatures and types is:

```
#include <iostream>
#include <string_view>

class A
{
public:
	virtual std::string_view getName1(int x) { return "A"; }
	virtual std::string_view getName2(int x) { return "A"; }
};

class B : public A
{
public:
	virtual std::string_view getName1(short int x) { return "B"; } // note: parameter is a short int
	virtual std::string_view getName2(int x) const { return "B"; } // note: function is const
};

int main()
{
	B b{};
	A& rBase{ b };
	std::cout << rBase.getName1(1) << '\n';
	std::cout << rBase.getName2(2) << '\n';

	return 0;
}
```

When running this we get:
```
A
A
```

This minute detail can cause severe consequences for complex programs.
To ensure we are actually overriding the virtual function, use the term *override*.

```
class B : public A
{
public:
	virtual std::string_view getName1(short int x) override { return "B"; } // note: parameter is a short int
	virtual std::string_view getName2(int x) const override { return "B"; } // note: function is const
};
```

This now produces a *compiler error*.

The use of *override* already implies virtual so there is no need to include *virtual* in the derived classes.

```cpp
class B : public A
{
public:
	std::string_view getName1(short int x) override { return "B"; } // note: parameter is a short int
	std::string_view getName2(int x) const override { return "B"; } // note: function is const
};
```

NOTE: Always use *override* for derived virtual functions.

# Final

The *final* specifier tells thec compiler that the class or virtual function can no longer be *inherited* or *overriden* by classes.

Take the following example:

```cpp
class A
{
public:
	virtual std::string getName() { return "A"; }
};

class B final : public A
{
public:
	// note use of final specifier on following line -- that makes this function no longer overridable
	std::string getName() override final { return "B"; } // okay, overrides A::getName()
};

class C : public B
{
public:
	std::string getName() override { return "C"; } // compile error: overrides B::getName(), which is final
};
```

If you don't plan to have your class to be inherited, make it *final*!

## Destructors

Always use a *virtual destructor* if you are using inheritance.

## Early-Binding/Late-Binding 

When a function is called directly:
```
add(x,0);
```
This is called *early-binding*. The compiler can replace this high-level code with the address to the function *add*.

*Late-binding* occurs when the function is resolved during *run-time*.
In C++, *dynamic binding* is used where the function is known *at* runtime.

NOTE:
There are two environemnts where *types* live in:
* Compile-Time
* Dynamic-Time


# Virtual Table

How are virtual functions implemented?
With the use of:
* Virtual pointer
* Virtual Table


This description is important:

```
When a class object is created, *__vptr is set to point to the virtual table for that class. For example, when an object of type Base is created, *__vptr is set to point to the virtual table for Base. When objects of type D1 or D2 are constructed, *__vptr is set to point to the virtual table for D1 or D2 respectively.
```

## Virtual Base Class

A virtual base class mostly arises if one encounters the *diamon problem* of inheritance.
When this happens, two copis of the common class are created.
SOmetimes you only want one copy of this class, so you use the following *virtual*:

```cpp
class PoweredDevice { ... };
class Scanner : virtual public PoweredDevice { ... };
```

# Object Slicing

Object slicing occurs when we copy a derived class to a base class:

```cpp
class Base {
    ....
};

class Derived : public Base {
    ...
};


Derived d1;
Base b {d1};
```

As the derived class contains the base class as well, only the *Base portion* of data will be *copied* to *b*.
This means that *b* will have its *virtual table pointer* point to the *Base table*.

Object slicing occurs when one is *copying* a derived class to a base class or a class above it.

This situation can occur in functions and vectors:

```cpp
void printName(const Base base) {   // object-slicing
    ...
    base.f1(); // calls f1 from Base class
}

void printName(const Base& base) {  // no object-slicing
    base.f1();  // calls f1 from the runtime class
}

vector<Base> vs;    // causes object slicing
vector<Base&> vs;   // No object-slicing
```

## Frankenobject

THe Frankenobject occurs when you are assigning a derived class to a base class as so:

```cpp
Derived d1;
Derived d2;
Base& b{d2};

b = d1;
```

Because *=* is not virtual by default, it will only copy the Base portion of *d2*.
Meaning, object *b* will have contents from both *d1* **AND** *d2*!!

# Dynamic-casting

We can use *dynamic_cast<type>(t)* to case the given *pointer* to the desired type.

```cpp
Derived d1;
Base* b{&d1};

Derived* dd{ dynamic_cast<Derived*>(b)};
```

If the casting failes, *dynamic_cast* returns a *nullptr*, so AWLAYS checks the returned value.
Otherwise, you will get undefined behaviour if you try to call a value from *dd*.

## Dynamic-casting vs Static-casting

Only use Dynamic-casting if you are *down-casting*.
If you are *up-casting* use *static-casting*.

In general, though, avoid using *dynamic-casting* and favor *virtual functions*.
Dynamic-casting is expensive!!
