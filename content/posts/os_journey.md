---
title: Quick Journey Through OS
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-30T19:33:32Z
lastmod: 2022-11-30T19:33:32Z
featuredVideo:
featuredImage:
draft: false
---

# OS Concepts
The concepts below will be quickly visited in one day!

* [x] Basic System Organization
* [x] user mode and Kernel Mode
* [x] System Calls
* [x] Processes
    - [x] State
    - [x] Control Block
    - [x] Memory
* [x] Context Switching/Process Swap
* [x] Interrupts
* [x] Threads
    - [x] Memory Organization
    - [x] Concurrency/Deadlocks
* [ ] Memory Management

# User Mode and Kernel Mode

User mode is where the user is able to call functions and do various things.
One thing it can not do is have access to the *hardware* of the computer.
This section is reserved for privileged modes such as the *kernel mode*.

The *Kernel Mode* has access to all components of the hardware, in order to prevent the *user* from tampering or changing sensitive data/components of the computer.

So how does the *user* gain access to parts of the hardware?
The *kernel* provides an interface that allows the *user* to utilize certain functionalities of the hardware.
This interface is known as **System Calls**.

## System Calls

**System Calls** are functions/routines that can be called by the *user* in order for the *kernel* to do the appropriate function.
From here, *user mode* gets passed to the *kernel mode*, which involves a *context switch*.

These system calls can be grouped into 5 categories:
* Process Control
    - Controlling of processes. (load, halt, abort, create, wait for time, memory allocation, process atribute manipulation)
* File Manipulation 
    - Managing files (e.g., create/delete, open/close, read/write, file attributes)
* Device Management
    - Manipulating devices (e.g., read/write, request/release, attach/detach, device attributes)
* Information maintenance
    - Information about system (e.g., date and time, system data)
* Communications
    - Communication between different processes or devices (e.g., create/delete communication, send/receive messages, transfer status information)

How can we go into *kernel mode*?

Kernel mode can be entered via *exceptions* or *interrupts*.
The bit is set from 1 to 0 to signify kernel mode.

# Process

A process is a program in execution.

When a process is executing, it can change from one *state* to another.
The *state* is defined by the current activity of the process.

What are these states?
* NEW -> process is being created.
* RUNNING -> Process is currently being executed.
* WAITING/BLOCKED -> Waiting for some event (e.g., I/O event, reception of a signal) to occur.
* READY -> Waiting to be assigned to a processor.
* TERMINATED -> Process finished execution.

## Process Control Block

Each process is represented in the OS by a *Process Control Block*.
A process control block contains the following information:
* Process state - state of the process at the particular moment.
* Process number (PID) - unique number/id of the process.
* Program counter - Address of next instruction that has to be executed.
* Registers - registers being used by the process.
* Scheduling Information -  priority of process.
* Memory Limit - memory being used by the process.
* Accounting Information - accounts all the resources being used by the process.
* Page Table Register - Location of Page Table.
* List of open files/I/O information
... (so on)

## Scheduling

When a process is created, it is placed in the **Job Queue**, where all processes reside.
There is a queue called the **Ready Queue** where all *ready* processes reside, where they await the chance to use the CPU.

Because the CPU can only have *one process* running at a given time, the CPU has to schedule which process is going to be given the CPU next.
This means it has to *swap* the current process with the next process.

# Process Management

This is about Processes vs Threads.

A process is a *current instance of a program*.
The OS has to allocate memory when executing the binary file of the program.

A thread is a *unit of execution* within a process.
A single process can have *many* threads.

## Threads

THreads share the same virtual address space of the *process*.
BUT, they have *seperate* **Memory Stack**, as it is used to store local variables.
It has limited size, so it is best to store *large values* in the *heap*.

Because the threads share the same virtual address space, they can share memory such as global variables.

AND they are faster to create than a process.

### What they contain

They contain:
* A thread ID
* Program Counter
* Register set
* Stack

It shares:
* code section
* data section
* others...

### User Threads
The kernel has no knowledge of *user threads*.
All thread management is done by the running process via a *thread library*.
* Scheduling
* Communication

A system call invokes the *kernel* which blocks all the other threads.
A thread can only be run in a *single CPU/core*.

### Kernel Threads

Kernel threads are managed by the system scheduler.
Switching between threads can be slower compared to user mode.

Despite these difference, there **must** be a *relationship* between the user threads and kernel threads.

### Multi-Threading Models

* many-to-one
    + Many user threads to one kernel thread.
    + Thread management done in user space NOT kernel level. Efficient (manage threads in user space)
    - Entire process blocks if a thread does a system call that gets blocked.
    - Unable to have multiple threads run in parallel on multi-processors. Due to there being only one kernel thread running in one processor.
* One-to-one
    - One user thread mapped to one kernel thread.
    + Allows other threads to run, despite a single thread making a blocking system call.
    + Can run in parallel on multi-processors.
    - Creating user thread requires a kernel thread being created. (Can become costly!)
    - Due to the cost of creating kernel threads, there is a limit of kernel threads that can be created.
* Many-to-Many 
    - Many user threads mapped to many kernel threads.
    + Multiplexes many user threads to a smaller/equal number of kernel threads.
    + Kernel threads can run in parallel in multi-processors.
    + Many user threads can be created, which are mapped to the kernel threads; allowing for them to run in parallel.
    + 

## Process

In order for the process to be swapped, its information must be stored in order to resume execution.
To organize sections of a *process* a **Process Image** is used that contains:
* Process Control Block
* Code
* heap
* Thread Stacks

All these processes are stored in a *process table*.

## Context Switching

To provide virtual parallelism of process execution, the processes need to be swapped in order to use the CPU.
Ensuring a resume of the process, the *Process Control Block* is used to get the process back to where it was at when it was swapped out.

The idea is to *save state* of the current process and *restore state* of the next scheduled process so that it can use the CPU resource
This is known as *context switching*.

When a *context switch* occurs, it is *pure* overhead. No useful work is being done. Only the swapping of processes.

# Interrupts

When an interrupt occurs, the following happens:
1. The current state of the runnig process is saved.
2. The current process is swapped for an *interrupt handler*.
    - A specific program that is run to deal with the interrupt.

That is, a *context switch* occurs.

There are different types of interrupts. One of these interrupts are called *exceptions*:

* Faults -> fix before next instruction
* Traps -> Fix after instruction
* Abort -> ABORT MISSION!

An interrupt has priority levels.
The higher priority, the faster it gains access to the CPU to handle the interrupt.

# Thread Syncrhonization/Concurrency

When executing a basic instruction such as:
```
x = x + 1;
```
What actually happens is that the instruction is split into sub-insturctions.

```asm
mov r1 x
add x 1
```

It could be possible that during execution, the process gets swapped for anothe process that is accessing *x*.
But because we have not finished the previous value, we are updating an old value.
Thus by the end of our initial instruction, we would have *lost* an operation.
This is known as a **Race Condition**.

In these cases, we want the execution to be **ATOMIC**.
That is, no other process can be swapped until the current process is done doing the **ATOMIC** execution.

This sections of code are called **Critical Sections**.

To ensure only one process enters this *critical section*, we ensure **Mutual Exclusion** of this section.
That is, only one process at a time can enter this *critical section*.

## Ensuring Mutual Exclusion

* Mutual Exclusion - No two or more processes may run in the critical section
* Progress - A process waiting to enter a critical section must wait for a *finite amout of time*, if the critical section is not used by another process.
* No external influence - A process outside the cirtical section must not influence which process enters next
* Bounded Waiting - Limit on how many times a process can enter a criticla section, while processes are waiting.


There is hardware instructions to ensure *atomic execution*.
Software solutions are not scalable.
