---
title: Vim Learn
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-22T22:28:44Z
lastmod: 2022-11-22T22:28:44Z
featuredVideo:
featuredImage:
draft: false
---

In this article I will have a list to demonstrate various sections of vim that I plan to go over:

1. [ ] vim movement (motions)
    - Horizontal Movement
    - Vertical Movement
    - File Movement
2. [ ] actions
    - motions
    - operators
    - custom motions
3. [ ] regex
4. [x] Find/replace
5. [ ] Global
6. [ ] Visual Mode
7. [ ] vimscript
    - [ ] syntax highlighting

# Find/replace

In the man pages:
```
:h find-replace
```
We are given the whole description of how it is used:

```
:s[ubstitue]
```

The basic syntax is as follows:
```
:[range]s/from/to/[flag]
```

Both *from* and *to* are patterns.
That means they can be described as *regexes*.

*range* can either be line numbers:
```
:1,5s
:.,$s
:?Chapter,/Chapter/s=grey=gray=g
```

# motions

```
:h motion.txt
```
