---
title: Kernel Development - Basics (Manjaro)
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-12-01T16:33:03Z
lastmod: 2022-12-01T16:33:03Z
featuredVideo:
featuredImage:
draft: false
---

This article goes over the basics for beginning kernel development via *device drivers* in Manjaro.
This article is written so that *I* don't have to remember what I did to get things running.

# Installing Kernel Headers
This section follows: https://linuxconfig.org/manjaro-linux-kernel-headers-installation


To begin, we first have to make sure we have the desired *linux header files*.
We will be using the ones that your Linux distro provides, as I am most familiar with that.

To check that we have the *linux header files* run:
```
pacman -Q | grep headers
```

If it only shows *api-headers*, then we do not have the *kernel headers*.
We need to install them first.

To do so, we must first check our current *kernel version*:
```
uname -r
```

This gives us our Kernel version that we will then use to download the required *kernel headers*.

```
sudo pacman -S linux-headers
```

We begin to install the *kernel headers*, and select the version we want to download based on our kernel version.

We now run:
```
pacman -Q | grep headers
```
and it will show *linux-headers*.

We are now ready to begin developing a device driver.

# Basic Code
This section follows: https://blog.sourcerer.io/writing-a-simple-linux-kernel-module-d9dc3762c234

The basic code to have a device driver is:
Filename: lkm_example.c

```c
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>MODULE_LICENSE(“GPL”);
MODULE_AUTHOR(“Robert W. Oliver II”);
MODULE_DESCRIPTION(“A simple example Linux module.”);
MODULE_VERSION(“0.01”);

static int __init lkm_example_init(void) {
 printk(KERN_INFO “Hello, World!\n”);
 return 0;
}

static void __exit lkm_example_exit(void) {
 printk(KERN_INFO “Goodbye, World!\n”);
}

module_init(lkm_example_init);
module_exit(lkm_example_exit);
```

### Preparing Compilation

To compile this to a *kernel object* we must make a *Makefile*.

```Makefile
KERNEL_DIR=/lib/modules/`uname -r`/build

obj-m+= lkm_example.o

lkm_example.ko:
    make -C $(KERNEL_DIR) M=$(PWD) modules

clean:
    make -C $(KERNEL_DIR) M=$(PWD) clean
```

This is all you need for basic compilation.
Simply run:
```
make
```

To created the *kernel object*: lkm_example.ko.

## Running Module

```bash
sudo insmod lkm_example.ko
```

Most of the output from *printk* goes into the *KERNEL LOG* file.
To view this file, type:
```
sudo dmesg
```

To check if the module is still loaded:
```bash
lsmod | grep “lkm_example”
```

Now to remove it, we run:
```bash
sudo rmmod lkm_example
```

### Automating

We can create a *Make* section that automates these commands:

```
test:
    sudo dmesg -C
    sudo insmod lkm_example.ko
    sudo rmmode lkm_example.ko
    dmesg
```
