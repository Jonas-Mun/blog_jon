---
title: Learning W3M
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-16T20:46:49Z
lastmod: 2022-11-16T20:46:49Z
featuredVideo:
featuredImage:
draft: false
---

I plan to learn W3M to browse the internet to avoid distractions.

# Basics

To run *w3m* and go to a website, type the following in a command line.
```bash
w3m <url>
```

We use *Tab* to go to the next *hyperlink* in the page, and hit *Enter* to interact with the functionality.

Commands:
* Space -> Go down a page
* b     -> Go up a page
* Shift+Tab -> Go back hyperlink
* U     -> Opens address bar
* Ctl-u -> clear address in address bar when opened.
* B     -> Go back a webpage
* s     -> Shows previous destinations
* v     -> Shows HTML source
* Ctl-s -> Search in current webpage
* n     -> Jump to next match of search
* N     -> Go to previous search
* R     -> refresh page
* Ctl-g -> History
* o     -> Options (change how w3m works)
* H     -> Help page for w3m (explains key-bindings)
* q     -> Quit w3m

# Copy Paste

* Esc-e

The above key-binding takes the current page and pastes it into the specified editor in the options.
Here you can copy/paste using the functionality of the editor.

