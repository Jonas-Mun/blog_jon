---
title: Virtual Memory
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-10T15:40:51Z
lastmod: 2022-11-10T15:40:51Z
featuredVideo:
featuredImage:
draft: false
---

To gain an idea of Virtual memory, we will give a basic description of a *process*.

# Process

Processes are created from a *program*, where a *program* is a set of instructions.
That means that a *process* is an instance of a particular program.

There can be multiple *processes* that were created from the same program.

## Process Virtualization

A CPU can only work on one process at a time.
To give the impression that multiple processes are be run, the CPU switches between processes.
Given each process an opportunity to utilize the CPU resource.

The switching between processes is known as **control switch**.
In order to go back to a process, the state of a process must be stored in a data structure.
The following is what is usually stored:

* Registers
	- Base Register
	- Stack Pointer
* State of process

The state of a process is stored in a **Process Control Block**.

### Basic States

The basic states of a process are:

* Ready
* Running
* Blocked

*Ready* signifies that the process is awaiting to be allocated the CPU.
*Running* means that it is currently using the CPU
*Blocked* means that it is awaiting for some event to finish that does not utilize the CPU.

The *blocking* state allows the CPU to swap a process out when it is not utilizing the CPU, in order to have a process that is *ready* to use the CPU.
This allows for the CPU to be utilized efficiently.

### Initializing a Process

When a process is instance, it is allocated memory.
This memory is separated into the following sections:

* Code
* Static data
* Heap
* Stack

TODO: ANSWER
*How does the CPU store the available processes?*

Now that a basic idea of a process is given, we can focus on **Virtual Memory**.
We will be assuming that a *single* process is running. When this is not the case, we will specify.

# Virtual Memory

Historically, paging was created for two purposes:

* Provide protection between processes. (i.e., avoid processes from accessing each other's memory)
* Giving more memory to programs than is actually given in physical memory. (VERIFY THIS!!)

The first instance has become the dominant reason for virtual memory.

The idea is that each process contains its own **address space**.
This is done by creating a **virtual address space** for each process.

In a way, virtual memory uses physical memory as a *cache* for the *secondary storage*.
It follows the same *principle of locality* as caches.

## Address Translation

Because we are using a *virtual address*, we need a mechanism to translate the *virtual address* into a *physical address* that directly maps to the physical memory.

The way to achieve translation is by separating the *virtual address* into sections:

```
       [Virtual address]
|     Seg1     |   Seg2   |   ->    |  Physical Address  |
```

The component that manages this *address translation* is usually called a **Memory Management Unit**.

## Page Design

One common implementation of virtual memory is **Paging**.

The idea is to divide physical memory into fixed-size blocks called **Page frames**.
The Virtual memory is also divided into equal fixed-size blocks called **Pages**

TODO: Show diagram 

The *virtual address* is divided into a *Virtual page number* and an *offset*:

```
Virtual Address

|  Virtual page number  |   Offset  |
```

To translate the Virtual address into a physical address, the *virtual page number* is translated into the physical address of the *corresponding* page frame.

```
Virtual page number ---> Physical Page frame
```

With the given *physical page frame*, it is then added to the offset:

TODO: Show diagram.

How is this translation done?

This translation is done by using a **Page Table** what maps every virtual page number to its corresponding *physical page frame*.
Because the CPU deals with many processes, each process has its own page table to avoid referencing an invalid physical page frame.

### Page Table

For each process, there is a **Page Table** that maps every *virtual page number* to a *physical page frame*.
The way a process is given a *page table* is by having a **Page Table Register** that stores the location of the current process's *page table*.
This means that a *process control block* must store the *page table register*.

All *page tables* are stored in the *main memory*.

With this information given, we detail the general algorithm for accessing data using virtual memory:

1. Fetch Page Table from main memory using the *page table register*. (Uses Memory)
2. Using the Virtual address page as index, acquire the physical page frame address and add it to the offset.
3. Use the physical address to acquire data from memory.	(Uses Memory)

Notice how there are now TWO memory accesses required when using virtual memory.
This makes a Paging design rather slow.

Luckily there is a solution that can reduce the number of memory accesses required.
This involves using a cache known as the **Translation Look-aside Buffer**.

#### Page Table Contents


A page table contains multiple *Page table entries*.
Each  page table entry contains the following:

* Physical page frame
* valid bit -> marks whether address is in address space or not
* protection bit  -> Privileges of page
* Present bit -> If page is in memory or not
* Dirty bit -> Indicates whether page has been modified
* Reference bit -> Whether page has been recently used

#### Page Fault

What happens when the *page* is not present in main memory?
That is, the *present bit* is off.

When this occurs, a **Page Fault** has happened, and an exception is raised.
The OS now takes control and has the duty to place the *page* into main memory.

How does it do this?

When creating a process, the OS creates a data-structure stored in secondary disk (this space is known as *swap space*) that maps all the virtual pages to their location in secondary disk.
The OS uses this data-structure to find the location of the *page* in the secondary disk, and places it in the available space in main memory.

Now, what happens if the main memory is full of pages?
If this is the case, the OS will have to swap out a page for another.
One way of swapping is the **Least-Reference Page**. 
This is implemented by using the *reference bit* to determine if it was recently referenced or not.

TODO: How does the OS update the page tables and TLB??

#### Swapping Pages

### Translation Look-aside Buffer (TLB)

The TLB stores *recently* translated addresses in its cache.
It uses the *virtual page number* from the virtual address as an index.

The data it stores is the *physical page frame* address. There are no tags used.

The general algorithm when using the TLB is as follows:

1. Extract the *virtual page number* from the virtual address
2. Check if an entry holds the *virtual address*
	- Yes? -> Cache hit, acquire physical page and add it to offset
	- No?  -> Cache Miss => Access Page table, from main memory, to acquire the physical page frame address, and add it to the TLB. Retry the memory reference once TLB has been updated.

When a TLB Miss occurs, an **exception** is trigerred, which calls the OS and raising the privilege to kernel mode.
The OS then jumps to a **Trap handler** that updates the TLB with the corresponding physical page frame address.
One it successfully updated the TLB, it resumes execution of the current instruction that triggered the exception

#### TLB Contents

A TLB is **fully associative**. That is, all blocks can be stored anywhere in the cache.

The general contents of a TLB block is:

```
|  virtual page number  |  Physical page frame address  |  other bits  |
```

Other bits can be:
* Valid bit -> determines if entry has valid translation or not
* Protection -> The privilege level of the page
* Address Space Identifier -> used to differentiate translation between processes.


When dealing with multiple processes, there must be a way to avoid a process from accessing a translation that points to a different address space.

There are two solutions:
* Flush the TLB
	- Very inefficient as the CPU is constantly switching processes
* use the Address Space Identifier
	- The Address Space Identifier, is like a Process ID, to check if the current process has access to the specific translation in the TLB

## Whole Picture

By having described caches, and virtual memory, we are now able to connect them all together.

1. A memory request has been issued by the CPU, for a particular process.
2. The virtual page number is sent to the **TLB** to check if it has been recently addressed.
	- 2.1 The TLB Checks the ASID to verify the physical page frame address pertains to that process
	- 2.2 If there is a recent translation, the physical page frame address is added onto the *offset*, creating the physical address.
	- 2.3 The data is fetch from main memory, and the CPU resumes.
3. If no recent translation is in the TLB, a **TLB miss** occurs.
4. The Page table of the process is accessed from main memory, using the *page table register*.
5. The virtual page number is used to access the corresponding entry in the page table.
	- 5.1 The *valid bit* is checked to verify the virtual address is in the *address space*
	- 5.2 The *present bit* is checked if the page is located in physical memory.
		- 5.2.1 If the *present bit* is valid, the physical page frame address is acquired from the page table.
		- 5.2.2 The physical address is formed by adding the physical page frame address and the offset
		- 5.2.3 this physical address is added into the TLB.
		- 5.2.4 The instruction is re-executed, where it uses the updated TLB to get the appropriate physical address.
6. The *present bit* is not valid, creating a **Page Fault**.

When a *page fault* occurs, an exception occurs which brings the OS into the foreplay.

What the OS does is to get the required page from its *swap space* and place it in main memory.
WHen doing so, it then updates the Page table entry with *present bit* on with the updated *physical frame number*.

It then retries the instruction, where there could be a *TLB miss*.

While the *page fault* is being dealt with by the OS, the process will be **blocked**.
Allowing for other processes to run while the page fault is being dealt with.

# High/Low Watermark

The OS does not wait until its memory is full to begin page replacement.
It tries to keep a small amount of memory free.
This is done by having:
* High Watermark
* Low Watermark

