---
title: Programming as a Means to an End
description:
toc: true
authors: ["Jonas"]
tags: ["journal", "programming"]
categories: []
series: []
date: 2021-08-31T22:28:44-05:00
lastmod: 2021-08-31T22:28:44-05:00
featuredVideo:
featuredImage:
draft: false
---

At times I have found myself programming just for the sake of programming.
Not really solving any problems, but just trying to get things to work.
This usually causes me to create buggy code or spaghetti code that makes it hard to maintain or
even add new features. I realized that the more I program without any clear idea,
the more troubles I create for myself in the future. Thus stunting my productivity.

One of the ways I manage to avoid making this mistake is by first observing what exactly needs to be
programmed. Figuring out what exactly is the problem or task and finding ways to provide a solution.
It is like taking a *bottom-up* approach.

Most of the examples I provide come from a game developer's experience. 


# Focusing on the Problem/Task to Solve

Many times it may be tempting to think that programming is the solution, but it could be that the problem
can simply be solved by some other means. 
A silly example is hacing to notify people of events. The options can be:
* Send a pigeon with a message
* Use a mechanical contraption to press a button.

The options sound silly, but the point is to not have to think immediatly about programming. The solution could not even involve
any kind of programming.
It is the problem/task that is the issue, and technology (plus ingenuity) is the tool that allows us to solve it in various
ways. Some may take more time than others, but it may provide a solution. Which brings an interesting thought:
*How technology makes our interactions with one another faster, and thus provide more experiences at a faster rate.*

When beginning to program, we have begun to enter a domain where creating software is the desired manner to solve
the problem/task. This may seem obvious, but I like having a context as to why I am programming what I am programming.
It gives me a clearer perspective as to what needs to be solved.

## What data?
It is important to figure out the data that will be used and manipulated. There is no need to jump
onto organizing data or what kind of objects/functions are needed. It may obfuscate a simpler solution.

By understanding the problem and knowing what and how the data is to be used can its organization occur.
Otherwise, you may be creating a problem out of thin air. The idea is to try and have your solution be
directly tied to the context of the problem. Try building from the bottom up.

It would be like building the roof before laying down the foundations of the house.

### Example

While I was creating a jam game, I had to implement barriers for a hexagonal grid game.
I knew what they were going to be, but I had no idea how I was going to implement them.
In my head they were considered Edges. A complicated thing!

Edges seemed abstract as I did not know how they worked. 

I decided to approach them in a different manner. I thought about
the functionality they provided. This meant focusing on the data/information that was needed in order
to implement them.

I knew an *Edge* would be between two hexagonal cells. Great! I need to store two hexagonal cells.
That is it for an Edge, but the hexagonal cells needed to know if they had an Edge or not.

The hexagonal cells had edges on certain directions, so I needed a way to store the edges on a HashMap:
```
Direction -> Edge
```

That was mostly it. Now I could have the tile entities avoid going through and Edge in their path-finding algorithm.
Something I thought would be hard to implement was fairly simple.

My issue was that I was focusing on the idea of what an Edge was, that I ignored the actual data/information needed
to implement them.


## Tools Available
The main tool is the programming language. What allows us to communicate to the computer and give it instructions.

### Programming Languages
A programming language allows us to communicate with the computer.
The computers have a certain amount of power that we can utilize via programming languages.
All programming languages are communicating with the same concept of what a computer is.
Meaning, they all end up doing the same thing but in different ways.

Each programming language has its manner and style of communication, yet the conext upon which they
operate remains the same. The computer that it operates on remains the same.

This is similar to natural languages. All natural languages exist in the context of the world we live in
and the expressions/emotions we want to communicate to each other.
They all exist in the same context, thus provide the same result but they do so in different ways.
(I may be oversimplifying)

Some programming languages may faciliate certain "expressions" while others do not.


### Other Tools/Patterns
There could be a need to document or communicate the overall functionality or use a specific pattern.
Here UML diagrams or simply plain diagrams can be used.
Perhaps even using MVC or another programming pattern.

The point is to ask if a pattern or tool is actually necessary. This question would creep up after the nature of the problem is understood, not before.

It is not good to enter a problem with all these tools in your mind and wanting to use it no matter what.
Usually this can cause issues, as an excuse would have to be created to justify the tool's use when the tool should really not
be used.

# Building the Infrastructure

Now that we know programming is needed and the data that is to be used, how do we go about building the software?
One can go right ahead and start programming, there is nothing wrong about that.
But from experience, one usually ends up having to revisit the code once they realize what
exactly is needed (Or start all over...). 

The following approaches are useful when trying to create a robust system/framework.
These are approaches I have found when creating some video games.


## Atomic Components or Types
Atomic components are simply components that are responsible for one thing only. I called them
*Atomic* to emphasize their single functional property. 

Take for example a *card* in a card game. The Card is going to be an atomic component with
various data. It has certain behaviours that other systems/components in the framework will be
able to use. 
The advantage of the atomic component is that you know exactly where the card functionality lies in your code.

Atomic Components can also be considered **Types** like in Functional Programming. 
By defining a *type* called card, you are able to implement systems that only deal with the type *card*.
This makes it easier to debug and follow the flow of data transmission/manipulation.

Another example is a *Card Factory*. A card factory takes a list of *Card Data* and outputs a list of *Card* type.

```haskell
card_factory :: [CardData] -> [Card]
```

By creating these types of components, it is easier to understand the functionality and purpose of a given
entity such as the card_factory.
As each component is responsible for a single functionality, if the functionality is needed again you can reuse
the code! No need to build it again from scratch.


## Abstract Common Behvaviour/Pattern

There are times where objects/entities will have similar behaviours. When these behaviours occur,
it is best to abstract it out onto a base class if possible. This allows one to avoid having to
write code every single time they want to create another entity/object with the same behaviour.
They can simply inherit from this base class which already provides the base behaviour that is required.

### Example
When programming for a game jam, our game required entities that would be able to move around the hexagonal cells.
My first approach was thinking about their behaviour, what they would be able to do. Upon realizing that all entities
would be operating in a hexagonal cell, I decided to create a base class called **TileEntities**. 
This made it so that all tile entities would have the same properties available for manipulation depending on
the object that needs to access them. Allowing for polymorphism.

I could have created scripts for every single kind of entity but these would involve duplicate code.
Something that can be avoided.

Their behaviours would be to move, be idle and attack. They way they would do all these things was similar, if not, the same.
So I decided to abstract these behaviours and added it to their base class.

In this base class I added the following states:
* Idle
* Migrating
* Attacking
* MovingToCell

and the following components:
* Animation Player

It was only a matter of implementing these behaviours.


Once I created the base class, I could create as many variations of the tile entity, and they would all be provided
with the base class behaviour. If I wanted to implement a specific behaviour I would create a new class that would
inherit the base class, and add the specific behavoiur onto the new subclass.

```
base class
  - sub class 0
  - sub class 1
```

Not only is there no need for duplicated code, but you provide a level of polymorphism to other objects that
want to deal with **TileEntities**.

### Issues
This approach requires to know exactly the behaviour that is to be abstracted out. If there are variations
of behaviours and you feel limited then perhaps you may be abstracting too much in the beginning.

The goal is to make your life easier when programming. If using inheritence in this way is making you
struggle to implement the required functionality, then it may be best to use something else.

## Data and its Representation
What ultimately gets manipulated is the data:
* Names
* Velocity
* Dates
* etc.

This is what the computer manipulates to provide a desired result. How it is represented is completely seperate
to what data is being used. This is similar to the MVC pattern, specifically the M and V.
There is no need to engrave the data with its representation.

### Card Example 

I had to create a card game for a game jam, and one of the first things I did was create the card class.
Inspired by Unity's concept of DOTS, I focused more on the data that was needed rather than on the object.

Eventually I ended up creating a *Card* class that contained all the data needed such as its:
* Effects
* Name
* Value

I later created a class called *DisplayCard*. This class would be responsible for displaying the data to the players.
It would have the *Card* class as a component.
```
DisplayCard
 - Card
```


## Extensibility and Modularity

Utilizing the *Atomic components* concept makes it easier to have things modular. As most of the
infrastructure is built on the components, it is just a manner of swapping a certain type of component
with a similar type of component.  

As for extensibility, I found having seperated the data from its representation helps to add new data
or manners of representing it.

I feel that the ability to extend and modulate stems from the manner that the infrastructure is built.
Utilizing atomic components and seperating data from its representaion helps foster the ability
to modulate and extend.


The point is to avoid having to change or add new code to the infrastructure when a new feature is needed.

## Duck-Tape Approach
Of course one can simply just code away and create a solution to the problem. 
There is nothing wrong about that. The only issue is the about of memory that is needed to remember where each
functionality is located in the code. Making it harder to debug and reason about but learning along the way.

## Issues
At times it may be unclear the abstractions or components that are needed. When such a case arises,
it may be best to forget about organization or preventing spaghetti code (allowing for duck-tape solutions)
and experiment.
Try to see what works and what does not.


# Playing Around
The *programming as a means to an end* is just an approach I tend to take when creating certain software.
It is not *THE* way to do things, just *A* way. 

It is perfectly valid to play around with the code and see how to create a solution. Especially when one
is unsure of what they are doing or just want to have something working.

In the end, it is just a manner of organizing one's code and thinking through the problem. 

## Cautionary Tale
I once fell in the trap of only focusing on creating the most elegant code. Full of abstractions and
organization. Which meant I was abstracting and organizing my code just for the sake of it. I forgot about
the purpose. This took away the fun of programming for me. 

Afterwards I decided to just code without regards to abstractions or organization. 
I would abstract and organize as I went, if I was sure it would benefit me. It gave me the freedom to
explore new ways of programming and play with new concepts. If it was just to play or test things out,
I would not even bother with abstractions or organization. I would just have fun. If it worked, Great!

# Conclusion
Overall, it is best to first understand the problem and figure out the data that is needed before starting
anykind of code organization or programming.

Once the above is done, then one can begin to build the infrastructure that will support the ability to solve
the problem or provide the desired functionality. 
This can be achieved by creating atomic components that focus on a single functionality or
abstracting certain behaviours to avoid duplicate code. This will help to build a robust system that can
be easily extended or modulated. Remembering that it is best to avoid programming for the sake of programming.

Of course, this is only one approach out of many. It is not THE way to do things. In the end, it is a manner
of thinking and organizing one's code. It is up to the programmer to decide how they want to build their
software. Whether they want to code immediately, think through the problem and code or to organize their code.
It is up to the programmer and their needs.

The code's organization is also just a means to an end, and not the end itself.

# Inspirations
This approach to programming was influenced by many things, but specifically the following.

* Functional Programming - Thinking about Types and Functionality + Modularity
* Team Game Development - Robust Systems (Avoid duck-tape solutions)
* Operating Systems - Modularity + Extensibility

If there is any one I recommend to learn, it is Functional Programming.
It provides a new perspective into programming other than OOP and introduces new ideas.
