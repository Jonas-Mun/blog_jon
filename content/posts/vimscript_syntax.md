---
title: Vimscript's Syntax Highlighting
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-20T20:41:06Z
lastmod: 2022-11-20T20:41:06Z
featuredVideo:
featuredImage:
draft: false
---

FOr a while I have been meaning to learn how to create my own syntax highlithing for logging.
Now I will spend the time to try and master it.
I will follow the tutorial from *Learn Vimscript the Hard Way*, and will be creating the *potion* plugin.

# Detecting Filetypes

At the moment, no filetype is given to *.pn*.
Let us create it:
```
potion $ mkdir ftdetect
potion $ touch ftdetect/potion.vim
```

Inside *ftdetect/potion.vim*:
```
autocmd BufNewFile,BufRead *.pn set filetype=potion
```

This makes sure that every file read or created with *pn* extension will be given the *potion* filetype.

We change the above to be:
```
autocmd BufNewFile,BufRead *.pn setfiletype potion
```

The above command only sets the filetype if it has not already been done. 

# Syntax

First create the following:
```
potion $ mkdir syntax
potion $ touch syntax/potion.vim
```

In *syntax/potion.vim*:

```
if exists("b:current_syntax")
    finish
endif

echom "Our syntax highlighting code will go here."

let b:current_syntax = "potion"
```
Type:
```
:messages
```
to see our message when opening a *potion* file.

The first *if* block and last line are boiler plate. Mostly to ensure it is not re-loaded.

Now we replace our *echom* with:

```
syntax keyword potionKeyword to times
highlight link potionKeyword Keyword
```

This example shows the core concepts of syntax highlighting:
* syntax keyword <customKeyword> <pattern_0> ... <pattern_n>
* highlight link <customKeyword> <GroupName>

To look at *groupnames*, check:
```
:help syn-keyword
:help group-name
:help iskeyword
```

For the example above, only the *iskeyword* characters are matched.

## Advanced Syntax

We can use *regular expressions* to define the patterns we want.

```
syntax match potionComment "\v#.*$"
highlight link potionComment Comment
```

Here we use *match* instead of *keyword*. This is where *iskeyword* comes into play.
Because some aspects of the comment are not included in the *iskeyword* it won't be highlighted.
The remedy is to use *match* for this purpose.

The *regex* uses *\v* for *very magic mode*. It is recommended to always use this mode.

NOTE: *match* does not support multiple expressions in the same line.

ALso, matching should be done by placing the largest after the smallest.
That is, the first match that is found will be applied.
Here is an example:

```
syntax match potionOperator "\v-"
syntax match potionOperator "\v-\="
```

Matchs both *-* and *-=*

```
syntax match potionOperator "\v-\="
syntax match potionOperator "\v-"
```

Only matches *-* when wanting to match *-=*.

NOTE: read 
```
:help syntax
```
to gain a deeper knowledge of it.

# Regions

Say we want to highlight regions, such as *strings*.
We use the following format:

```
syntax region potionString start=/\v"/ skip=/\v\\./ end=/\v"/
highlight link potionString String
```

The *skip* allows the *region* to ignore any characters after *.*.
Remember that *.* stand for any character.

For example *"They said: \"me too \""*

```
:help syn-region
```

