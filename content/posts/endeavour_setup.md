---
title: Setting up EndeavourOS 
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-02T15:00:58Z
lastmod: 2022-11-02T15:00:58Z
featuredVideo:
featuredImage:
draft: false
---

This article will document the steps taken to set-up a working EndeavourOS.

The setup will be divided in the following checkpoints:
* [ ] EndeavourOS Installation
* [ ] AwesomeWM Basic Installation
* [ ] AwesomeWM Basic Hardware Configuration
* [ ] AwesomeWM Keymap Hardware Configuration
* [ ] AwesomeWM Keymap Workflow Configuration

# Installing EndeavourOS

After downloading EndeavourOS from their website, I booted it up in a Live USB and installed it using the *offline* setting.
I tried using the *online* setting, but it produced an error.

The installation of EndeavourOS would fulfill the *initial installation* step

* [x] EndeavourOS Installation

# Setting up AwesomeWM

I decided to manually install *awesomewm* as a windows manager.

## Basic Installation

```bash
sudo pacman -S awesome
```

Because EndeavourOS did not have *xterm* initially installed, I had to install it in order to run the terminal in awesomewm.

```bash
sudo pacman -S xterm
```

## Config File Setup

This step just copies the initial rc.lua file upon installing awesome.

```bash
cp /etc/xdg/awesome/rc.lua ~/.config/awesome/
```

This should be the basic steps to configure *awesomewm*, but you can follow the arch wiki for more information: https://wiki.archlinux.org/title/awesome

* [x] AwesomeWM Basic Installation

## Configuring Awesome and Bar

There are some already created widgets for the *awesome bar* (AwesomeWM's bar) that can be easily installed.
Their website is: https://github.com/streetturtle/awesome-wm-widgets

To install you can follow the instructions or, if you are using *git* to version control your dotfiles, run the following command under *~/.config/awesome/*

```bash
git submodule add <repo-to-awesome-wdiget>
```

### Battery Widget

https://github.com/streetturtle/awesome-wm-widgets/tree/master/battery-widget

The battery widget uses an icon pack. This icon pack is listed under the *battery icon widget*.
This packet can be isntalled by following the insturctions in the website.
The commands I executed were:

```bash
git clone "https://github.com/horst3180/arc-icon-theme" -- depth 1 && cd arc-icon-theme
./autogen.sh --prefix=/usr
sudo make install
```
The commands must be run under a folder where you want to install the font.

Before using the widget, you will need to install *vicious* to due an error if run without it:

```bash
sudo pacman -S vicious
```

For the battery widget, a packet is required:

```bash
sudo pacman -S acpi
```

We are now ready to implemented the battery widget into the awesome bar. This is done by following the instructions in the github repo battery widget.

```lua
local battery_widget = require("awesome-wm-widgets.battery-widget.battery")

...
s.mytasklist, -- Middle widget
	{ -- Right widgets
    	layout = wibox.layout.fixed.horizontal,
		...
		battery_widget(),
		...
```

The variable *battery_widget* is what contains the functionality of the widget.
To have this widget be displayed, it has to be added to the tasklist as shown in the code.


The procedure to install other widgets is similar to installing the battery widget.

* [x] AwesomeWM Basic Hardware Configuration

## Keymap Setups

Some keys may not work such as *brightness* or *volume*. 
To configure these keymaps, you must add the keymaps in the *rc.lua* files (or install a widget and configure it to respond to your keymaps, but this is for later use).

### Setting up Brightness

To setup the brightness, install *acpilight*:

```bash
sudo pacman -S acpilight
```

Normally,*xorg-xbacklight* is used, but I kept getting an error, so I decided to use *acpilight*.
By installing *acpilight*, you are able to call **xbacklight** which will be used to control the brightness.

#### Add to Video Group

Before setting up the brightness, we will have to add the user as a member for the *video* group.

```bash
usermod -a -G video <user>
```

We will now have to check the name of the kernel for the brightness:

```bash
ls /sys/class/backlight/
```

The name of the folder that appears after calling the above command will be the name of the kernel.
For example, if you have intel it would be *intel_backlight*. If you have AMD it would be *amdgp_0* or something like that.

We will now add **udev rules** to be able to run the *xbackligh* command via the keymaps.

Make a file called *backlight.rules* under **/etc/udev/rules.d**:

```bash
sudo touch /etc/udev/rules.d/backlight.rules
```

Begin to edit the file with your designated editor. If you use vim, type:

```bash
sudo vim /etc/udev/rules./backlight.rules
```
and insert the following rules:

```bash
ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="<kernel>", RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness"
ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="<kernel>", RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"
```

Where *<kernel>* is the name obtained when calling:

```bash
ls /sys/class/backlight/
```

#### Lua KeyMap Setup

With the brightness configuration done, we are ready to add the keymap to the *rc.lua* config file, under the keybindings (it is commented)

```lua
...
awful.key({ }, "XF86MonBrightnessDown", function()
    awful.util.spawn("xbacklight -dec 10") end),
awful.key({ }, "XF86MonBrightnessUp", function()
    awful.util.spawn("xbacklight -inc 10") end),
    ...

```

### Setting up Audio

Make sure you have **amixer** installed, as this is how the volume is configured.

If so, run the following command:
```bash
amixer set Master 5%+
```

This should increase the volume by 5 percent.

#### Lua KeyMap Setup

We can now add the keymap to the *rc.lua* config file.

```lua
...
awful.key({ }, "XF86AudioRaiseVolume", function()
    os.execute("amixer set Master 5%+") end),
awful.key({ }, "XF86AudioLowerVolume", function()
    os.execute("amixer set Master 5%-") end),
...

```


* [x] AwesomeWM Keymap Hardware Configuration

## Keymap Workflow Configuration

At the momemnt, the basic keymaps in *rc.lua* are not ideal for my taste.
We will now add and edit some of these keymaps.

### Swapping Master window

I want to swap the master window to the currently working window.
We can add the following keymap to do so:

```lua
awful.key({ modkey, }, "e", function() 
    client.focus = awful.client.getmaster(); client.focus:raise() end),
```

### Mapping DMenu

If you do not have *Dmenu* installed, run:
```bash
sudo pacman -S dmenu
```

and add the following keymap to the *rc.lua* file:

```lua
awful.key({ modkey, }, "d", function()
    awful.util.spawn("dmenu_run") end),
```

### Exiting Window

Currently exiting window is via: *modkey + Shift + c*.
Coming from *i3* this is not ideal, so I change it to: *modkey + Shift + q*.
Since this mapping is already being used by aweomwm (killing awesome wm), I decided to change it:

```lua
...
awful.key({ modkey, "Shift"}, "q", function(c) c:kill() end,
...
```

### New Window becoming a child

The default setting has all new windows becoming a main window.
I want to have the new window become a child of the main window.
There is a line in the *rc.lua* file that is commented.
Simply commenting out the line allows for the desired functionality:

```lua
if not awesome.startup then awful.client.setslave(c) end
```

The above code should be commented in the default *rc.lua*.
* [x] AwesomeWM Keymap Workflow Configuration
