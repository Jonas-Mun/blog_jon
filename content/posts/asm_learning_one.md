---
title: Learning ASM - Part One
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-06T08:48:30Z
lastmod: 2022-11-06T08:48:30Z
featuredVideo:
featuredImage:
draft: false
---

This is part one of learning ASM using the book: *Introduction to Computer Organization*.

# Translating to Assembly language

The following command can be used to translate a source code in C to assembly.

```bash
gcc -Oo -Wall -masm=intel -S doNothingProg.c
```

* -Oo ~ Tells the compiler not do use optimizations
* -masm=intel ~ Use the intel syntax for assembly languages
* -S ~ Stop the compilation when finishing writing the assembly language.

The strings that start with a '.' are known as **Assembler Directives**.
These give the assembler information such as debugging or error situations.

To reduce these directives we use the option: *-fno-asynchronous-unwind-tables*.

```bash
gcc -Oo -Wall -masm=intel -S -fno-asynchronous-unwind-tables doNothingProg.c
```

For security reasons, an **CET** information instruction is placed in the beginning of the assembly language: **endbr64**.
We can remove this to focus solely on the essentials of assembly language, using: *-fcf-protection=none*

```bash
gcc -Oo -Wall -masm=intel -S -fno-asynchronous-unwind-tables -fcf-protection=none doNothingProg.c
```


## Essential Assembler Directives

* *.text* -> Tells assembler to place everything that follows in the **Text** section.

There are various *segments* where information is placed:
* Text (code, READ-ONLY)
* Data (global variables and static local variables)
* Stack (Call Stack)
* Heap 

* *.type* -> Gives identifiers a specific type, written in the link object (.o)

# Startup of a Function

There is a program *prologue* and *epilogue*:

```asm
main:
	push	rbp		# save caller's frame pointer
	mov 	rbp, rsp	# establish our frame pointer

	...

	mov 	rsp, rbp	# restore stack pointer
	pop rbp			# restore caller's frame pointer
	ret			# back to caller
```

The following are links to Intel/AMD x86_64 instruction sets:

* https://software.intel.com/en-us/articles/intel-sdm (INTEL)
* https://developer.amd.com/resources/developer-guides-manuals/ (AMD)

# MOV - instruction Set

```asm
mov reg1, reg2 - move value from reg2 to reg1
mov reg1/mem, imm - move immediate value imm to reg1
```

When an immediate value is being moved to *memory*, the data size with a *size directive* must be specified:

```asm
move byte ptr x[ebp], 123
move qword ptr y[ebp], 123
```

The *size directive* is the **ptr**, as it specifies *how many bytes the memory address points to.

The first instruction above moves *123* to a memory of 1 byte. Whereas the next instruction moves *123* to a memory of 4 bytes.

# Stack Instructions

* push reg1 -> places the value in *reg1* at the top of the stack.
* pop reg1 -> removes the top value of the stack and stores it in *reg1*.
* ret -> Pops the value at top of stack int the instruction pointer *rip*. Control is given to the memory address.

# Returning form a Function

To return from a function, two registers are used:
* *rbp*	- Base Pointer/frame pointer
*rsp* - Stack pointer (The element at the top of the stack.

Each function, when called, uses a portion of the stack called a **stack frame**.
The beginning of this stack frame is specified in the *rbp* register.
When calling a function, the location of the previous function is saved onto the stack.
That is, it is pushed onto the *call stack*:

```asm
push rbp
```

This saves the previous function's *frame pointer*. 
We can now use the *rbp* for the current function's frame pointer:

```asm
mov rbp, rsp
```

These beginning steps is known as the **Function Prologue**.

When we are finishing a function, we will have to *restore* the previous function's stack frame:

```asm
mov rsp, rbp	# restore stack pointer
pop rbp		# restore caller's frame pointer
ret
```

The *ret* instruction returns a value by storing *0* in the *rax/eax* register.

The above instructions are known as the **Function Epilogue**.

# Using GDB

We run the following command to *assemble*, *link * and *execute*:

```bash
as --gstabs -o doNothingProg.o doNothingProg.s
gcc -o doNothingProg doNothingProg.o
./doNothingProg
```

When running gdb, the following commands are useful:

```gdb
(gdb) set disassembly-flavor intel
(gdb) b main
(gdb) r
(gdb) tui enable
```

The first instruction sets the instruction syntax to intel.
The second creates a break in main
The third runs the executable
The fourth brings up the source code, as well as various ways to look into the executable.

TO see all the registers:

```gdb
(gdb) layout regs
(gdb) step
```

This will show the registers and step into the instruction execution.

# Testing Assembly Functions

We will write three assembly functions:

```asm
	.file	"f.c"
	.intel_syntax noprefix
	.text
	.globl	f
	.type	f, @function
f:
	push	rbp
	mov	rbp, rsp
	mov	eax, 0
	pop	rbp
	ret
	.size	f, .-f
	.ident	"GCC: (GNU) 11.2.0"
	.section	.note.GNU-stack,"",@progbits
```


```asm

	.file "g.s"
	.intel_syntax noprefix
	.text
	.globl  g
	.type 	g, @function
g:
	push	rbp
	mov	rbp, rsp

	mov 	eax, 2

	mov	rsp, rbp
	pop 	rbp
	ret
	.size 	g, .-g
	.ident	"GCC: (GNU) 11.2.0"
	.section	.note.GNU-stack,"",@progbits

```

```asm
	.file "h.s"
	.intel_syntax noprefix
	.text
	.globl  h
	.type 	h, @function
h:
	push	rbp
	mov	rbp, rsp

	mov 	eax, 8

	mov	rsp, rbp
	pop 	rbp
	ret
	.size 	h, .-h
	.ident	"GCC: (GNU) 11.2.0"
	.section	.note.GNU-stack,"",@progbits


```

These assembly functions only return a value.
We will test that these values are correct by writing a C file that calls the functions:

```c
/* asm_test.c */
#include <stdio.h>

extern int f();
extern int g();
extern int h();

int main(void)
{
	printf("%d:%d:%d\n", f(),g(),h());

	return 0;
}
```

We now have to link the assembly functions to the main executable.
To do this, we first create the *objects* for each assembly file:

```
as --gstabs -o f.o f.s
as --gstabs -o g.o g.s
as --gstabs -o h.o h.s
```

Now we Link them together:

```bash
gcc -g -o asm_test asm_test.c f.o g.o h.o
```

We can now run our executable:

```bash
./asm_test
```

This ends the introduction chapter of *Introduction to Computer Organization* (Ch. 10).
