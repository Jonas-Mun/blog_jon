---
title: Theme Setup for AwesomeWM
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-02T21:32:40Z
lastmod: 2022-11-02T21:32:40Z
featuredVideo:
featuredImage:
draft: false
---

Ever wanted to change the initial theme of AwesomeWM?
Well I will show you how.

If you run:

```bash
ls /usr/share/awesome/themes/
```

You will find all the initial themes installed with *awesome*.

To change theme, go to your *rc.lua* file and find the line:
```
beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
```

Simply replace all *default* for either one of the available themes (e.g., "zenburn/theme.lua") and restart awesome and you will have a different theme.

# Custom Theme

You can create the following file:

```
cp /usr/share/aweomse/themes/default/theme.lua ~/.config/awesome/
```

to begin customizing your won theme.
Follow the instructions here: https://awesomewm.org/doc/api/sample files/theme.lua.html

## Useful links for color setup
* https://www.reddit.com/r/awesomewm/comments/j0mh8o/how_would_i_use_a_different_taglist_fg_focus/
