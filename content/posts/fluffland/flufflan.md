---
title: Godot Wild Jam 36 - Third Year Anniversary
description: "My experience partaking in the GWJ 36 Third year Anniversary"
toc: true
authors: ["Jonas"]
tags: ["game-jam", "godot", "journal"]
categories: ["game-jam"]
series: []
date: 2021-08-30T12:34:34-05:00
lastmod: 2021-08-30T12:34:34-05:00
featuredVideo:
featuredImage: images/Wildling_noBG_v1.png
draft: false
---

<a href="https://godotwildjam.com/"> Godot Wild Jam</a>

Initially, I did not plan to be a part of this game jam but a team was looking for a *Primary Programmer* and I
thought I could volunteer. Most of the times when I was in a team, I was a secondary programmer. Helping out
with polishing or with other tasks. This meant I had to accustom to the style of the programmer who was
programming.
Begin the lead programmer, I would be able to use my style and my methods to create the game that was needed.
It was an opportunity for me to show what I could do. That and I wanted to try out my new *neovim + LSP* setup.

# Joining a Team
The team I joined consisted of well-know people in the community and past winners. I was intimidated when joining.
I was worried that my skills would not be enough. 
Before joining I asked myself if I would be able to be a good programmer. I haven't programmed for a whole month,
so I was rusty. Plus, if I would be able to dedicate the time needed to deliver the game as I planned to take it easy
and avoid any crunching.

To avoid crunching I created a schedule where I would dedicate some of my time to my daily life and to programming.
I would later find out that I had to scrap this idea...

# Jam Starts

When the jam started, my team-mate decided we think of 3 ideas each for a game and write them down. We would later
discuss those ideas. I thought it was a good idea as the timezones were vastly different. Two of us were in the *GMT+5* and another two were in *GMT-5*. As the theme of the jam was announced at 15:00 GMT-5, our two members would have already been tired. This gave us time to think and go wild with ideas on our own.

I liked this approach. The other method was to have a meeting and brainstorm. I find this method limiting because it gives
pressure to think of something and have to listen to other's ideas while still thinking of your own.

## Saturday Meeting
With our ideas jotted down, three of us started a meeting to discuss which ideas sounded great.
I belive three of them sounded great, while the others not so much. One thing we wanted to avoid doing was something
that involved gardening/plants. It seemed like an obvious choice that many people would do it. We wanted to do something
different. In the end, we were not sure what to choose so we decided to have another meeting later on in the day.

### Second Meeting
The second meeting involved three of us, but this time the other one that was absent.
We discussed our decisions and started brainstorming again. We landed on a sort of RTS-creeper engulfing type game.
Something similar to **Creeper World**, but you would be responsible for fostering your growth to make it uncontrollable.
You would expand and capture cells. But there would be an opposing force preventing you from expanding.

This was the general idea. The idea seemed abstract enough that we could coat it with some theme such as bunnies.
We all liked the idea, and decided to go with it. The only problem was that we did not iron out the logic of the game.
This would come to hunt us during the week. The basic idea was there, but not the specific details.

I made a GDD so that our other team-mate would read on what we decided what to do.

Our artist wanted to ahve it be hexagonal. I did not realize at the time how much I had to learn.
## Gameplay for Uncontrollable Growth
I noticed to main mechanics for most of the ideas:
* Managing growth
* Fostering Growth
* Riding alongside the Growth

Most of our ideas involved managing the growth.

# Game to Make
The game we were going to make was an RTS-Hexagonal grid game. You would be responsible for fostering an uncontrollable
growth throughout the lands. An opposing force would try to stop you, and you would have to struggle to consume
everything.

We did not have the exact details of the game. I did not step up and decided because I wanted to focus solely on
programming, but I realize now that I should have spent some more time communicating how the game would actually play.
Perhaps I did not communicate this thoughts because I knew that a hexagonal grid was needed, and it could work for any
kind of themes. As I did not know how to make a hexagonal grid, I focused on learning about it. 

This was a challenge I decided to take. I did a square grid before, but not a hexagonal grid.
Good thing I found **Red Blob Games** a while back. This had everything I needed to make a hexagonal grid.
I could not have done it without their blog.

# Making the Game
Throughout Sunday to Wednesday I implemented the following:
## Sunday
* Hexagonal Grid

## Monday
* Pixel to Hex Coordinate
* Hexagonal Cells
* Growth
* Neighbour Cells
* BNet 

## Tuesday
* Bunnies
* Population Resources
* Merged UI
* Structures to Cells

## Wednesday
* Clickable units
* units move to other cells


It seems like I did a lot, but most of these implementations lacked polish. The main reason for this
is because when I got it working ok, I moved on to the next mechanic. I was focused on speed rather than
on sound implementation. It also made me feel like I was being productive.
This is a habit that I have noticed of myself. Try to get it working barely, and say it is done without actually
checking if it works as intended. This may be due to the pressure and wanting to move on.
Because of this approach, most of the code felt disorganized. If I spent my time trying to implement it well
I may have avoided various bugs in the future.

Although the pressure of the jam did cause mt to move on too fast, the lack of clarity of the game's logic also
prevented me from implementing the spefici logic at the time. In my mind, I could implement a mechanic, but it
could not have been the correct one in the end. This thought, at times, stopped me from polishing some of the 
mechanics. I am not saying it is the sole reason for why I left some of the machanics un-polished and disorganized. 
Rather, the lack of the game's logic played a part as well. It demoralized me as I was not sure if it was
going to be used.

## My Schedule

* 7:00  -> 8:30        - Morning Breakfast
* 9:00  -> 11:30       - Coding
* 12:00 -> 2:30        - eat/cook
* 15:00 -> 17:00/18:00 - Coding
* 19:00 -> 20:00       - Coding

# Mid-week Meeting

Due to a lack of clarity in the game's logic, D4yz and I had a meeting to iron out the details.
This gave us a more clear idea of how the game is going to be played. 

One thing we did discuss was the progress of the game. I felt things were going slow and he agreed. We would have
to quicken the pace if we want to have everything ready by the deadline. This made me realize that I would have to
ignore my schedule and focus my days on programming the game.

Another issue that crept up was: *How are we going to build levels?*. I initially thought of have some interpreter
that given an array of symbols, would build a level. 
Something like:
```
[
    [g,g,g,c,g,g,m,g],
    [g,g,g,g,w,w,g,g],
]
```
Depending on the symbol, the specified cell would be placed. For a programmer, this would be fine but as others would have to build levels, this may not be ideal. It would not allow us to visual see how the level would look, and would
be hard to make changes based on what we see.



At the moment, the grid was automatically generated. We had no control on how it would look other than the default
cell. The symbolic interpreter would work, but it felt limiting. It did not feel like we had complete control over
how the level was going to look like.

After the discussion, I realized that the auto-generated hexagonal grid would be of no use if we could not
manually change/add things the way we want to. We would be fighting the auto-generative grid rather than benefiting
from it. This is when I realized that we would have to have a way to manually create the levels as we want to provide
the desired gameplay. Thus the level maker was born.

Not only to make it easy to build levels, but to test out the mechanics.
Without the level maker, we would have not levels, and thus no game to play.

Making the level maker was top priority for the next day.

I did not mention it in the meeting, but throughout the week I felt I could not implement all the mechanics.
I began doubting myself, thinking it was a mistake to have actually joined. Regardless, I kept going. Doing
my best. I may not have had a solution at the moment, but I was going to find one.

# Making the Game - Part 2
To implement the remaining mechanics, I decided to isolate myself and focus on programming.
Taking things one step at a time. Focusing on the details that need to be implemented and finding a way.

I started off by building the level maker. This took me almost the whole day to get it right.
I wanted it to be interactive so that we can build levels easily and visually see how the level is going to look.
Once the level maker was created, we could have the freedom to test various aspects of the game with ease.


##Thursday
* Bunny Upgrades
* Path Finding
* Map Maker
* Military Structures

## Friday
* Scuba Bunny
* Structure Destruction
* Win/Lose Triggers
* Implemented Edges
* Boxer Bunny
* Scuba Bunny Behaviour implemented
* Added Audio/SFX

# Final Days

## Saturday
* Bug fixes
* Nuke military Base
* Changes to game logic + Special Resources


## Sunday
* Bug Fixes
* Minor gameplay changes

On Sunday it was stressful. We got some playtesters and the bugs kept coming. At one point I felt overwhelmed!
I told myself to focus and prioritize on which bugs were most important. This helped me focus on the issue and
solve the bug.

At the end I was exhausted. We had a farewell meeting and discussed how we felt. I enjoyed working with them,
and they enjoyed working with me.

# Result
We got 7th place out of 74!!

# Lessons Learned
* Error Checking - I used godot's debugger to find where null is being used. Not ideal with niche crashes.
* All aspects of the game must be in harmony/polished to provide a great experience to the player, regardless of the complexity of the game.
* Iron out the game's details as soon as possible.
* Implement the mechanic as well as you can first.
* Having a way to manually add things to an auto-generated system can be of great use.


# Conclusion
I enjoyed this jam. I created a hexagonal grid, something I never done before plus I implemented an RTS game!
Something I thought I would not do. My team-members were great! All skillful. 
There are some lessons I learned that I hope I will be able to apply in the future.
