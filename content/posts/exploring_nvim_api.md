---
title: Exploring Neovim's API through Harpoon
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-15T16:27:18Z
lastmod: 2022-11-15T16:27:18Z
featuredVideo:
featuredImage:
draft: false
---

In this article I plan to explore Neovim's API using **The Primeagean**'s Neovim plugin: *Harpoon*.
We will be using Neovim Docs: https://neovim.io/doc/user/lua.html

We will start with *two* files:

1. dev.lua
2. utils.lua

In the next article we will look at:
3. init.lua
4. mark.lua

# dev.lua

## M.reload()

The reload uses *plenary's* reload function to reload a module.
I assume it allows neovim to source the plugin again.
Let me go check it out...

In essence it acquires neovim's list of plugins and sets the specifc module to nil.
This means that neovim will have to run harpoon again because it does not have *harpoon* cached in the list of plugins.

Code can be found here:
https://github.com/nvim-lua/plenary.nvim/blob/master/lua/plenary/reload.lua

## set_log_level
The first *vim* function we encounter is:

```
vim.env.HARPOON_LOG
```

under **set_log_level()**:

Two questions are asked:
* How does one set an *env* variable
* Where is *HARPOON_LOG* stored?

One can set an *env* variable via:
```
vim.env.FOO = "bar"
print(vim.env.FOO)
```

It also uses vim's global variables:

```
vim.g.harpoon_log_level
```

This answers question 1., but what about two????
Well, it actually does not set it.
It is up to the user to do so.

The default value obtained from **set_log_level** is *warn* if the user has not set a value.

## Logging
Logging works by creating a *log_levels* array.
This array is used in *set_log_level*, used globally, where it checks if either a value in the global variables:
* HARPOON_LOG
* harpoon_log_level

If not, it returns *warn*.
The value is stored in *log_level*.

This value is stored in *M.log*, where it is passed to **Plenary**'s log system.

We must look into *plenary.log.new* to see how it has been created.

### Plenary.log

The function *new* generates a *table* with logging information.
It adds the following into the table:
* "harpoon"
* *log_level*

*log_level* would be *warn* if user did not set any Harpoon log variable.

We now move to: *os.time()* which gives the data and time *codified* into a number.

We can now move onto...

## override(key)

In this function it seems that the *M.log* can store functions in the table.
The *override* function tends to get a function based on the keys from *log_levels*.
* trace
* debug
* info
* warn
* error
* fatal

This means that *plenary.log* stores functions for each log level.

We now come to the instruction that gives the function's name:

```lua
    M.log[key] = function(...)
        fn(log_key, ...)
    end
```

We have one main question:
* What does *function(...)* mean?

According to: https://www.tutorialspoint.com/lua/lua_functions.htm it denotes a *variable arguments*.

That means we can pass as many arguments as we want.
Now, how does the function know about the many arguments???

I guess simply passing the *...* is equivalent to passing all the arguments.

### Use of log.key

When overriting the function in plenary's default log functions, *override* passes the *log_key*.
This is the date and time obtained via *os.time()*.

It acts like a *closure* to the original function.

For all log levels defined in *Harpoon*, the default functions in the *log table* are overwritten via the following code:

```lua
for _, v in pairs(log_levels) do
    override(v)
end
```

## M.get_log_key()

This one just returns the *log key*.

## Software Design

There are two concerns I have:

1. The use of global variables in local functions.
2. Having functionality be ran outside of functions.

## Basic functionality

**dev.lua** provides the following functionality:
* Logging
* Reloading of Module

Which begs the question: Why not call it: *log*?

NOTE: I am not bashing ThePrimeagen. The plugin he made is one that I don't think I can even develop.


# utils.lua

This file uses plenary's *jobs* and *path* functionality.
Lets leave them for now.

The first *vim* functionality is:

```lua
local data_path = vim.fn.stdpath("data")
```

The question is:
* What does *vim.fn.stdpath()* do?

Let us go to the docs!
```
:h stdpath()
```

So it gets the *path* based on the given type.

in this case it gets the *data* path.

This *data* path is assigned to the module's *data_path* variable.

## M.project_key()

We now get to the following vim function and new api:

```
vim.loop.cwd()
```

We now have two questions to ask:
1. What is *vim.loop*?
2. What does *cwd()* do?

To the docs!!
```
:h vim.loop
```

We get the following description:
```
`vim.loop` exposes all features of the Nvim event-loop. This is a low-level
API that provides functionality for networking, filesystem, and process
management. Try this command to see available functions: >
```

The keyword for us is: *filesystem* because the function *cwd* is being used.

So what does *cwd* do?
after running the following command:

```lua
print(vim.loop.cwd())
```
I gather that it returns the **c**urrent **w**orking **d**irectory.

Alright we got this settled, now we move on to the next function...

NOTE: We are skipping *M.branch_key()* because it uses a function defined later.

## M.normalize_path(item)

This function is only one line:

```lua

    return Path:new(item):make_relative(M.project_key())
```

We now have three questions:
1. What does *Path:new(item)* do?
2. What does the *:* mean?
3. What does the *make_relative* do?

### The 'Path:new(item)' meaning

What does it mean!?
Let us first figure out what *new* does...

It is a way for calling methods in Lua, but has a different behaviour from *.*.
We dig a little deeper.

From good old stackoverflow:https://stackoverflow.com/questions/4911186/difference-between-and-in-lua

Given the following code:

```lua
x = {foo = function(a,b) return a end, bar = function(a,b) return b end, }
```

```
The colon is for implementing methods that pass self as the first parameter. So x:bar(3,4)should be the same as x.bar(x,3,4).
```

So that means that the module **Path** is passed to the function.
Let us test it out.

```lua
local M = {}
M.add = function(x,y,z)
    print(x)
end

print(M:add(1,2))
```

This returns the address of *M*. 

If we changed it to:

```lua
print(M.add(1,2))
```

We would get *1* as output.
This means that the *x* can be considered the *mod* address of *M*:

```lua
local M = {}
M.add = function(mod,y,z)
    print(mod)
end
```
I have no idea what it does... I assume it createa s path with the given *item*.
Then somehow makes that path *relative* based on the project key.

#### Meta-table
To be more exact, the function *Path:new()* creates a *Meta-table* via:

```lua
setmetatable(obj,Path)
-- obj is a table
return obj
```

This begs the question:
1. What is a *Meta-table* in Lua?

From: https://www.lua.org/pil/13.html we use the example:

```lua
t = {}
print(getmetatable(t))   --> nil
```
Now we use the following example:

```lua
local t1 = {}
setmetatable(t,t1)
print(getmetatable(t))  --> t1
```

This means that the *obj* has *Path* as a *Meta-table*.

```lua
    return Path:new(item):make_relative(M.project_key())
```

I don't know how this works.
I need to play around *meta-tables* to better understand how this works.
I'll leave it for now.

## M.get_os_command_output(cmd, cwd)

We are introduced to *type* in Lua.
```
It gets the type of the variable and outputs it in a string.
```

Another function of Lua is used:
```lua
local command = table.remove(cmd, 1)
```

Now, in Lua a *table* is also considered and array!!
What *get_os_command_output* expects in *cmd* an array.
The function *table.remove(cmd,1)* removes the first value of the array.

Now we move to:

```lua
    local stdout, ret = Job
        :new({
            command = command,
            args = cmd,
            cwd = cwd,
            on_stderr = function(_, data)
                table.insert(stderr, data)
            end,
        })
        :sync()
```

We have to look at **Job**...

### Job

Job is taken from *planery*'s functionality.

We have two questions:

1. What does *new* do?
2. What does *sync* do?

To answer (1), we know it accepts a table. This table is passed:
* command
* arguments
* current working directory
* standard error

The *standard error* initially takes an empty table and inserts the data.

TO answer (2) *sync* uses *self.wait()* so I assume it just waits for the job to finish and return the result.

With this model in mind, we go to the following function...

## M.branch_key()

This function uses *M.get_os_command_output()* as follows:

```lua
    local branch = M.get_os_command_output({
        "git",
        "rev-parse",
        "--abbrev-ref",
        "HEAD",
    })[1]
```

By observing this, we now see that the *Job:new()* runs a terminal command and is able to store the output.

What *branch* contains is the *current* branch in git.
The value of *branch* would be *master* if we were using git.

The *branch_key* function would return either the current_directory or the current directory alongside the branch.

```lua

    if branch then
        return vim.loop.cwd() .. "-" .. branch
    else
        return M.project_key()
    end
```

## M.split_string(str, delimiter)

We will see the result with the following arguments:
* "hello monsieur Holo"
* ' '

```lua
print(vim.inspect(M.split_string("hello monsieur", ' ')))
```

The output is:
```
{ "hello", "monsieur" }
```
So this outputs and *array* of the strings, seperated by the delimieter (in our case 'space').

Now let us delve into the function.
The functionality of interest is:

```lua
    for match in (str .. delimiter):gmatch("(.-)" .. delimiter) do
        table.insert(result, match)
    end
```

We now have two questions:
1. What does (str..delimeter) mean?
2. What does the *gmatch* function do??

For question (1), we are concatenating the string with the delimiter.
In our example, this would become:
```
"hello monsieur "
```
Notice the extra space after *r*.
We now lead to the function *gmatch*.

From the following page: http://lua-users.org/wiki/StringLibraryTutorial

We know that the function relates to *strings*.

We focus on the following function:
```
s:gmatch(pattern)
```

So from this, the pattern in our case is:
```
(.-) .. delimiter
```
So it splits based on parenthesis, full stop, dash, and the given delimiter.
With this it splits the string to produce an *iterator*  which is used.

Now let us play with the iterator with the following string...

```
"hello-dudes.not me(letsrun)"
```

in the following function:
```lua
print(vim.inspect(M.split_string("hello-dudes.not me(letsrun)", ' ')))
```
My guess is the following tokens:
* hello
* hello
* not
* me
* letsrun
NOPE!

We get:
```
{ "hello-dudes.not", "me(letsrun)" }
```

There seems to be something about *patterns* in Lua.
Let us go and delve into PATTERNS!

### Patterns
In Lua, patterns act as *regular expressions*.
Lua implementes *regular expressions* via *character classes*.
One such *character class* is *.* which:

```
'matches any character'
```
Let us test it out!
```

local st = "hello me"
for m in st:gmatch(".") do
    print(m)
end
```

OUTPUT
```
h
e
l
l
o

m
e
```

We now turn to *repetitions* in order to solve the following:
```
"Even with character classes this is still very limiting, because we can only match strings with a fixed length. To solve this, patterns suppoert the four repetition operators."
```

We will only be focusing on *-*:
```
- Matches the previous character (or class) zero or more times, as few times as possible
```

Let us show an example:
```lua
local st = "hello me"
for m in st:gmatch(".-") do
    print(m)
end
```

We get *nothing* from the output.
This is because it tries to match *as few times as possible*.
What that means exactly, I still do not understand.

With this information we can say what *.-* does:

```
It matches nothing.
```

Why *nothing*?
Tries to match as few times as possible, so I think that means that it will find an excuse to not match anything unless it has to.
Because *.* is used, then it will repeatedly go to the next character because it has not yet found an excuse to give a match.

Only when we add some *pivot* does it match.
Take for example:
```lua
local st = "hello_me"
for m in st:gmatch(".-_") do
    print(m)
end
```
This outputs:
```
hello_
```

Now what do the *brackets* mean?
From the docs:
```

```
Used when we want to extract a specific combination of symbols.
Thus we now know what the following means: "(.-) "

```
Match all characters until a space is found.
```

```lua

local st = "hello me"
for m in st:gmatch("(.-) ") do
    print(m)
end
```

Gives us:
```
hello
```
What happened to the *me*? Well here is why the *delimiter* is appended at the end of the given string:

```lua

local st = "hello me "
for m in st:gmatch("(.-) ") do
    print(m)
end
```

Gives us:
```
hello
me
```

Back to the original code:

```lua

    local result = {}
    for match in (str .. delimiter):gmatch("(.-)" .. delimiter) do
        table.insert(result, match)
    end
    return result

```

This matches the sequence of characters up to the given delimiter.

P.S. Lua has its offline documentation!!!!!
```
:h gmatch
```

## M.is_white_space(str)

This function has only one line:

```lua
    return str:gsub("%s", "") == ""
```
We have two questions:

1. What does *gsub* do?
2. What does "%s" mean?

Before we delve into, let us first test out the function with:

```
Hello me mates.
```

```lua
print(M.is_white_space("Hello me mates."))
```

We get false.

So it just checks whether the string is white space or not.
Now we go back to the function...

### gsub(pattern, repl)

Let us play with the function:

```lua
local st = "hello me"
st:gsub("%s", "")
```

Gives us:

```
hellome 1
```
Where does the *1* come from????
To understand this, we may have to figure out what *%s* means.

From the docs themselves!
```
%s: whitespace
```
(I thought it stood for string...)

So for every occurence of a *whitespace*, we replace it with nothing.
That is why we have them joined together.

Now take the example: 
```
local st = "hello me too"
st:gsub("%s", "")
```

We get:
```
hellometoo 2
```

I assume *2* is the number of substitutions done.
Now let us add a white space only:
```lua
local st = " "
st:gsub("%s", "")
```

```lua
 1
```

So for some reason it outputs the substitution...
I am confused... no more because in the docs themselves:

```
gsub also returns, as its second value, the total number of substitutions made.
```
Aha! That solves the last riddle.
So now we are able to answer what the following code does:

```lua
    return str:gsub("%s", "") == ""
```

It deletes all spaces, by replacing them with *nothing*.
If there are characters, then these do not get replaced which means that output is not empty.
And when the output is not empty, then the string given is not a whitespace!

## Summary of utils.lua

As the name suggests, it provides functionality that may be re-used throughout the plugin.
It utilizes *Plenary* to call bash commands such as *git* and its *path* functionality to do something with the *current directory*. We may better understand it later on.

It also creates custom *string* functions that splits a string based on a delimeter and checks if a string is white space or not.

As we delve into the plugin, we may better understand their purpose and how they are used.


# Conclusions

THe file *utils.lua* seems to be good.
It does not depend on global variables, nor does it do two things at once.
The only nitpick I have is:
* The seperation of the two string functions.
    - These functions could be stored in *string.lua* but I am not sure if it is actually worth it.

It mostly concerns itself with directories, as well as getting the current *branch* by calling *git*.

As for *dev.lua*, there are three main issues:
* It utilizes global variables in functions.
* SOme of the functionality runs when reading the script and not via functions.

These two I see as issue because the behaviour can not be determined explicitly.
There are side-effects...

The last issue is:
* It contains a *logging* functionality.
    - This behaviour can be extracted out.

With an understanding of these two files, we can now go onto the next two:

3. init.lua
4. mark.lua
