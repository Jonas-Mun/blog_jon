---
title: Learning Org.mode in Emacs
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2021-08-08T15:58:44-05:00
lastmod: 2021-08-08T15:58:44-05:00
featuredVideo:
featuredImage:
draft: false
---

# Installing emacs

In manjaro, I installed the emacs that is available in Pacman
```
sudo pacman -S emacs
```

# Org.mode Tutorial Setup

I set up emacs with the following config: <a href="https://orgmode.org/worg/org-configs/org-customization-guide.html">Org Setup</a>

This tutorial is what I am using to learn about Org.mode:
<a href="https://orgmode.org/worg/org-tutorials/org4beginners.html">Org.mode Tutorial"</a>

## Adding Agenda 
To activate agenda **C-c a**, I had to add the following code to my emacs config
```
(global-set-key "\C-ca" 'org-agenda)
```

## Adding file to Agenda view
To have the TODOs be viewed in the Agenda View, have the following command in the *.org* file.
```
C-c [
```

# Basic Commands

* C-x C-s - Save document
* C-x C-f - Open/Find document

## Org Header
To have emacs treat the file as an ORG file, add the following header:
```
MY PROJECT -*- mode: org -*-
```

## Org config
To make sure all files ending in *.org* are considered and Org file, add the following:
```
(require 'org)
```

# Lists
A list in Org mode can be considered a Header. There are various levels:
```
* First layer
** Second Layer
*** Third layer
```

## Folding
Folding parts is done via:
* TAB - Fold
* Shift+Tab - Unfold

```
* Header 1
** Header 2
*** Header 3
```

TAB:
```
* Header 1
```

TAB
```
* Header 1
** Header 2
```

TAB
```
* Header 1
** Header 2
*** Header 3
```

## Moving and Promoting Lists
You can move Lists around by: **M-Up/M-Down**

And promoting lists by: **M-left/M-right**

## Inserting
TO insert a new lists: **M-RET***

# TODOs
