---
title: Installing Spark Ada
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2021-09-30T23:32:55+01:00
lastmod: 2021-09-30T23:32:55+01:00
featuredVideo:
featuredImage:
draft: false
---

This article will go over how to install Spark Ada version 2015 in Manjaro.
Ada is available via the www.adacore.com website.

Best to go to www.adacore.com and into the download section.

# Installing Ada
If you want to download the latest version of Ada, the simply clicking on the Download tab will show the current versions
for all platforms.

I believe version 2021 gnat file comes with SPARK. Simply download the file and run the executable file.
If you can't run it, you will have to make the file executable.

```
chmod +x <gnat-bin-file>
```

## Other versions
If you want to install other there is a link at the near bottom saying
```
More packages, platforms, versions and sources.
```

For installing version **2015**, you will have to download both **GNAT GPL Ada** and **SPARK GPL**.
Make sure to select your platform.

Once done downloading, you will have to extract the folder and run the **doinstall** script file for each one.
It will ask you which directory you want to install it in.

That is it.

To run SPARK, call *gnatprove*.
