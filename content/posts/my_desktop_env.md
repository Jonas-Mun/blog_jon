---
title: My Desktop Environment
description:
toc: true
authors: ["Jonas"]
tags: ["journal"]
categories: []
series: []
date: 2021-07-30T19:56:59-05:00
lastmod: 2021-07-30T19:56:59-05:00
featuredVideo:
featuredImage:
draft: false
---

Lately I have been looking at ways to improve my productivity and comfortability while in the computer. Before my setup was Manjaro and XFCE. 

The Manjaro + XFCE environment worked for the most part. I was able to do all tasks that I wanted, and do some programming. After a while, I noticed some areas where I was slow or annoyed me. 
I wanted to fix this, and so I set out to find a desktop environment that satisfied the following requirements.

* Productive
* Comfortable

# i3
Once of the issues I had with Manjaro + XFCE was the mess of windows I had when programming. For this reasson I decided to use i3.

## i3 Setup


### Nitrogen - Wallpapers

## i3 Gaps

### Picom
