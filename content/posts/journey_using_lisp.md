---
title: Journey Learning Common Lisp
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-12-04T17:47:57Z
lastmod: 2022-12-04T17:47:57Z
featuredVideo:
featuredImage:
draft: false
---

I have been eyeing Lisp for various years, every since I learned about it.

With Advent of Code going on, I thought it would be a perfect chance to use it and play around with the language.
This article serves more as a checkpoint of my knowledge and the resources I used to learn the language.

# Why Learn Common Lisp

I decided to learn Common Lisp because I thought it would be a good tool that I could have in my repertoire.
Although this is not the first time I decided to try and learn Common Lisp or a Lisp dialect.
So why is learning Common Lisp going to be different this time, compared to all my other attempts?

The main difference is that before I thought Lisp was *the* language to use and if anyone could master it they would become an almighty programmer!!
As my years of programming grew, and I began to understand things a lot more (wisdom began to flow through my neurons.) I realized Common Lisp, and lisp in general, is a tool.
A tool that if I am able to use well can help me. Just like any other language, but I think learning Common Lisp can be more advantageous when compared to other languages.
Sure, Common Lisp has its flaws, but this time I am okay with it. I seek to learn it, not because I will unlock the secrets of coding, but because I think it can become an advantageous tool
for me in the future.
I don't plan to use it for production-level code at the moment, but mainly to play around with concepts, ideas or models.

Also, I am having so much fun learning Common Lisp! I noticed I enjoy applying recursion.
So far, it has taught me different perspectives as well.

# Journey so far

## Land of Lisp
I began this journey this year by reading **Land of Lisp**.
I had my eye on this book for a while, and thought it would be a good way to get into the mood of learning Common Lisp.

NOTE: **Land of Lisp** is not ideal for a beginner programmer, but for a programmer with experience already.

So far I have learned the following:
* Always try to deal with data that the language is capable of manipulating with east.
    - In Lisp's case try to stay in the land of lists. This is the main data structure that Lisp deals with.
* Maintain Symmetry between external representation and internal representation
* There are two namespaces for functions and variables.
* Data mode and Code mode
    - Data mode can be designated using a *'*
* Quasiquoting 
    - We can evaluate code inside data by using the backtick *`* and comma *,*: `(A B ,(+ 2 3) 9)     \`
    - asdwfadf
* Nil -> false, everything else true The
    - There are various ways of representing nil: nil, () 'nil, '()
* You can evaluate various S-expressions with *progn*
* mapcar - 
* apply - Apply a function to a set of items in a list.

These are the main things I learned from **Land of Lisp**
There are other things such as *global parameters* using *defparameter* and defining functions using *defun*.
As well as *let*, *flet* and *labels* which is used to define variables in scope, define functions, and define functions that can be recursive, respectively.

So far I have gotten to chapter 6 in the book.
It teaches Common Lisp by writing games.

## Advent of Code 2022

For Advent of Code 2022, I had to learn how to read input from a file.
I read the chapter *Input/Output* from **Practical Common Lisp** for this: https://gigamonkeys.com/book/files-and-file-io.html

I learned the following:
* read - Reads a stream by converting the text into an *internal representation* in Lisp.
    - Specify what to output if stream is empty.
* read-line - Reads text until a newline appears.
    - It des not convert text into and internal representation, just stores it as a string.

It was emphasized that *read* would be ideal to use as it makes it easier to manipulate the data in lisp.


This basic knowledge allowed me to write a function that can read a whole file.

### Use of Bash to Massage Data

In a stream of **ThePrimeagen**, he used *bash* to solve some of the challenges.
He specifically used *sed* for this where he was able to manipulate the data file to change its contents.

With the *lesson* that it is best to have data be in a manner that the language is most comfortable, I decided to *massage* the data into S-expressions to avoid having to parse strings.
To do this, I also used *sed* where I would be able to add parenthesis to each line.
Now I can call *read* for all lines and already contain a data structure that I can manipulate!

This experience gave a new insight: The preparation of data can facilitate the way in which you solve the problem.

I also stumbled on the ability to deal with *case-sensitive symbols*.
By default, Common Lisp converts all text in a file in *upper case*.
This can be an issue if information is contained in a letter which is either upper or lower case.

To keep the case, we would have to surround the *symbol* in bars *| |*:
```
'(a) -> (A)
'(|a|) -> (|a|)
```

# Conclusion

I am liking learning Common Lisp a lot!
Hopefully I may get the chance to touch upon macros or other aspects that are unique to Lisp.
Until then, I will spend time in Lisp land!
