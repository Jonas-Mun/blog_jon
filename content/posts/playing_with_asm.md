---
title: Linking C file with Assembly file
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-07T16:36:01Z
lastmod: 2022-11-07T16:36:01Z
featuredVideo:
featuredImage:
draft: false
---

In this article I will show how one can link a *C* file with a function defined in an assembly file.

The following C file will be used:

```c

#include <stdio.h>

extern int add(int x, int y);

int main(void)
{
	int x = add(1,2);

	printf("value: %d", x);

	return 0;
}

```

Before creating an assembly file, we must know how the arguments are stored.
In Linux x84_64, the arguments are stored, starting from the last argument, in the following registers:

1. rdi
2. rsi
3. rdx
4. rcx
5. r8
6. r9

Information taken from: https://cs61.seas.harvard.edu

We now build the following assembly file:

```asm
	.intel_syntax	noprefix
# Constants
	.equ	sSize, -16

# Arguments
	.equ	firstArg, -8
	.equ	secondArg, -4

	.text
	.globl	add
	.type 	add, @function

add:
	# Function Prologue
	push	rbp		# Store caller's frame pointer
	mov	rbp, rsp	# Obtain add's frame pointer

	# Create space for local variables
	add	rsp, sSize	# This was causing seg fault => I was not resetting the stack pointer approprieatly in the Function Epilogue.

	mov	DWORD PTR -4[rbp], edi	# Store last argument
	mov	DWORD PTR -8[rbp], esi	# Store first argument

	mov	edx, DWORD PTR -4[rbp]
	mov	eax, DWORD PTR -8[rbp]

	add	eax, edx

	# Function Epilogue
	mov	rsp, rbp	# Make sure it is not: mov	rbp, rsp. This can cause segmentation fault.
	pop 	rbp
	ret
	.size	add, .-add
```

With this file, we now have to build the object file and link it with the *C* file:

```bash

as --gstabs -o my_add.o my_add.s
gcc -o my_add main.c my_add.o
```

We can now run: *./my_add* for it to output the value.
