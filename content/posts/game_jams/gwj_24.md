---
title: "Godot Wild Jam #24"
toc: true
authors: ["Jonas"]
tags: ["game-jam", "unity", "journal"]
categories: ["game-jam"]
series: []
date: 2020-08-25T15:15:21-05:00
featuredImage: images/Wildling_noBG_v1.png
draft: false 
---

The 24th game jam, marking the 2nd year anniversary of the Godot Wild Jam!
For this jam I decided to be a part of a team. My goal when looking for a team was to find artists, composers and level design as those are my weak points.

In the end I teamed up with someone who had skills with game-design, audio/SFX, and some management. Making me realize that I would have to take more than I initially planned when joining a team.
Although I ended up doing more, I was curious to see if I could make the art and improve my pixel art skills in the process.
A challenge I was willing to take.

# Lessons to apply from previous GWJ 

Last time I took part in the Godot Wild Jam, I created a platformer.
The lessons I learned from my experience are the following:
* Prioritize core game mechanic (movement, combat)
* Start creating art/assets when the core mechanics have been created
* Reduce scope of game if there is not enought time
* Don't stress about how many days are left, focus on finishing the task.

# Beginning of the Jam - Friday

My team was ldrg and I. Our time difference was 6 hours, we decided to meet up and organize how we would communicate and interact with one another while developing.

We spent most of the time discussing tools and hours to communicate and meet up. Trello wa sto be used for project management, and github for version control. Discord was our main means of communicating so we set up a server for our small team.

This meeting took quite a while, but it was necessary in order to avoid any obstacles that would appear in the future. Especially when we had a 6 hour time difference.

Once we had a structure to the team, we decided to meet up the next day and discuss the kind of game we would like to make. Giving each of us at least a day to brainstorm on our own and later discuss what we have come up with.

# Deciding which game to make
Saturday was spent deciding the kind of game we wanted to make with the theme: **Family**.
In the end in boiled down to two:
* Top down Shooter (Star fox)
* Puzzle game (Family of Ducks)

## Top down Shooter
This game was simple enought to make. The main concern was the AI.

## Puzzle Game
A puzzle game would require to get the puzzle and movement right. It would
take time to experiment with puzzles. I personally would have preferred to make this game, but it would take more time compared to our previous option.

## Decision

We decided on the Puzzle game (Family of Ducks).
One of the motives for choosing this game was because I felt it was going to be challenging to make a steering movement behaviour. Something I had always
wanted to try and make. We knew the type of game was going to have a big scope compared to the *Top down Shooter*, but we felt we could do it.

# Making the game
After the brainstorming we created a list of core mechanics that the game would have.
Having this list would tell us whether the gameplay aspect of the game would be complete.
The mechanics were as follows:
* Duck Movement
* ducklings follow Duck
* Commanding Ducklings (Stop, Follow)
* Switches
* Key/Lock
* Box (scrapped)
* Flock control

I highly recommended we focus on the core gameplay mechanics of the game first, and later on the art.
My last experience with the GWJ taught me this lesson. I focused too much on the art, and not enough time
on the core gameplay mechanic, which made the game rather boring.

My team-mate would take time to research how to make levels well. I also though it best to dedicated a lot of time
in designing levels. This lesson I too learned from my previos game jam. Level design takes time, and is best
not to rush it. It is just as important as the gameplay mechanics.

## Deadline
I gave myself until **Wednesday** to have all the core mechanics implemented.
Allowing me to focus on creating the art without having to worry about whether the game would still be missing some
gameplay. 

My reasoning behind this is as follows:

Last GWJ I spent some time playing around with pixel art. Spending a day or two experimenting with the character art to see what I liked.
Taking this observation I decided to give myself one day to experiment and see what I like, and have the next day either create all character assets, tiles, objects
plus animations or just the character assets.
Giving ample time to relax and play around.

Having all the core-mechanics done by Wednesday would allow me to focus heavily on the asset/art creation.

# Duck Movement

## Directional
The first version of the duck movement was having the duck move based on 8 directions:
* Up
* Down
* Left
* Right
* Diagonally

The direction would be given by the keys pressed, having the duck *snap* towards the direction give.

## Steering
After the directional movement was done, I then implemented the steering. 

The idea was to have the Duck have a *forward* vector, and have it interpolate to the given direction.
Giving it the steering behaviour that I wanted to achieve. I learned about Vector interpolation from a youtube video
about **Geometric Algebra**.

The *forward vector* also helped in having the ducklings follow the Duck from behind.

Duck movement at this point was complete

# Ducklings
For ducklings I first had them follow a point in the game. Where the point would be directly behind the Duck.
This was simple to do

## Flock
I spent so long trying to implement a *flock* behaviour. At first I tried to implement *Boids algorithm*, but I kept
failing after numerous tries.

There were two reasons for this:
* Implementing everything at once. (Not gradually, so I got confused)
* The behaviour I wanted to implement was not the one Boids provided

What I mean by the second point is that, the Boids algorithm provided more than I needed, and I was new to *Boids algorithm* I would
be fighting to morph it to have a subset of its behaviour.
Better to just implement the *follow* behaviour on my own. I had help from GDScript with one of their tutorials on steering behaviour that had a bit of
follow.

The basic algorithm was having a *goal* that the duckling would go towards, and stop once it was near. 
This would work for multiple ducklings, so it would look like a horde of ducklings going towards a position.

## Follow

Once I had the moving of ducklings implemented, I turned my attention to **Follow**.

I had experience before implementing a *follow* behaviour. In fact, it is my first tutorial I made for Unity, so it was rather easy to implement
due to my experience.

The duckling would have to have a *forward vector* in order to calculate the position directly behind it. This position serves as the goal
for the duckling that is meant to follow. This same behaviour applies to the next duckling, and so on.

## Flock Manager
As there would be vaiours ducklings waddling around, I thought it best to have a **Flock Manager** that would contain all ducklings.
The main reason for it was to control when the ducklings would *stop* or *follow* based on the command given by the player.

# Level Engine/Mechanics

I created a level engine to make it easy to come up with levels and experiment with their layout.
It had the following mechanics:
* Switch
* Lock/Key

There were several iterations to make it easy to add new items or connect switches to doors/barriers.
This would allow me to focus on other aspects of the game, while my team-mate could play round with the level engine, and experiment.

My team-mate gave me feedback on it, and I did my best to improve its design so they would have no problems or obstacles when implementing
their level design.

# Mid Checkup

It would have been Wednesday when all the core mechanics were implemented.
During my time programming, my team-mate spent their time coming up with level layouts, and thinking of ways how to introduce the mechanics to the player.
He would show me his prototypes, and I would critic it. Providing feedback on the level, as well as how it can be improved.

I understood level design is rather difficult, especially if you are starting out, so having a lot of time learning and desgining levels would be beneficial
in the end. It was on Wednesday where my team-mate would be able to implement their design onto the game.

I also created documentation about the level engine and how to build a level. This would help my team-mate learn the level engine and how to use it.
It proved helpful as I would be able to focus on other tasks, and clarify any concerns he would have when reading the documentation.

# Thursday - Art

## Duck
The first sprite I created was the Duck. I ended up creating 8 sprites, one for each direction.

As this was the first time I created art for a *top-down* game, I spend quite a while experimenting with what worked.
So much patience was involved, but the end result was satisfactory.

One lesson I learned is trying to have symmetry of the object, if possible. This seems easy to do if the proprotion of the object are similar, but for a duck!?
I had to look at duck images and try to see if I could replicate their body shape in pixel art.

Patience and experimentation helped out in the end.

## Duckling

Similar process was involved as the Duck. The only difference is that the ducklings were to be smaller.

I found it difficult to portray the idea of a duckling withing 16x16 bits. The advice of sculpting and using color to convey an image really helped.


What was most difficult were the diagonal direction sprites.
It took a while to get it right, but what helped was trying to have the sprite be at an angle. Imagine a straight line going at a 45 degree angle.
Having the sprite be as if it was following that line. 


## Directional Animation
What would be the point of sprites, if I would not be able to show them in game?

So the next step was to create a script that would change the sprite of the duck/ducklings based on the direction.
After that was done I then spent the time creating the animations for each direction...
Patience. Lots of patience.

## Tileset
I envisioned the color of the game to be mainly green, but not too bright. Sort of a washed/soft green that is pleasing to look at.

### Bush
The bush took a while to get right. I first started making small bushes on a tile, but they were not the best. In fact, they were horrible.
Realizing that I did not know how to make bushes, I looked up a tutorial in youtube to learn how to make a bush.

I stumbled upon this video, and followed step by step. It proved very useful, and allowed me to create a ssatisfactory bush. I even was able to apply what
I learned to create hedges. 

#### Color
As the background tiles (grass) would have a washed/sfot green, I though it best for the bushed to have a stronger green to stand out and indicate:
This bush is not the same as the background.

# Check up
Having created all the above, I felt relaxed. It was a nice feeling realizing that most of my tasks were finished.
I could put programming aside and focus on the art and UI. A nice change of pace.

# GUI/UI
By this time it would have been the end of Friday and I had not done the GUI/UI.
I spent most of the time learning how Godot's UI works. It took a while to learn the uses of:
* HBoxContainer
* VBoxContainer
* MarginContainer

After experimenting, I think I understood how I can achieve the desired UI.

## Art
For the art of the UI, I took it easy. Decided to spend time thinking what would be the best color to use for the UI.
At first I thought of brown for the butons, but it looked a bit ugly.
I asked the question: "What colors would complement the game's color pallete?"

The answer was white, orange, and yellow.
I utilized these colors as they were the colors of the duck and ducklings.

With only two days left of the game jam, I felt I needed to have at least the basic look of the UI so I spent most of the night learning Godot's UI,
and creating the art for buttons.

# Music
I reached out to someone to see if they could create music for the game.
Told them it would be similar to the game Braid with a simplstic tune.

They said they could do it and delivered.

# Staturday
Saturday came, and most of the game components were ready. It was only a matter of connecting them together to provide a seamless experience to the player.

# Sunday - Last day
From 8AM to 4PM, I worked on the game. Making changes where needed and adding some polish. 
My team-mate was also busy working on the game.

By the end our communication faltered a bit, so there was a bit of a panic to get things done. Especially with the levels, as some were not working
due to how the level engine was designed.

A last minute change of collision caused the duck to collide with the ducklings. It was not the best idea to do, and should have used the
rectangular collision instead. But panic set it, and things were done.

# The end of the Jam
We submitted with about 18 minutes left, and felt relieved. 

I felt exhausted. In the last two days I felt my energy draining, and by Sunday I felt like I could do almost nothing.
This may be to do with how much I pushed myself to get everything done.

# Lessons Learned/ Experience
When it comes to teamwork, it is best to not attach yourself to your creation too much, as it would impede others from contributing to the idea of the game.
I feel I put too much to the game. The scope we had in mind made me work hard to have everything ready. Making me feel as if I was creating everything,
and my team-mate not so much. 

What was realy happening was I was attaching myself to the game, and wanted it to succeed for personal reasons. This made me feel some resentment,
but I understood it may be exhaustion speaking. 

I will try to explain why:

My team-mate was learning how to deisgn puzzle levels. Level design on its own, is a tough task. I experienced this when creating my platformer
for the previous GWJ I participated in.
Having to come up with puzzles adds another aspect of complexity, in my opinion. Requiring a lot of trial and error to get it right.

My team-mate specified that they would only work for 4 hours on the game jame a day. Whereas I could spend most of my day working on it.
This caused me to do perhaps more that I intended in the beginning, which caused the exhaustion in the end.

I pushed myself hard, which may be in relation to the scope of the game as well as the amount of team-mates I had, causing me to become exhausted by the end.

## Discpline in priorities
I had a tendency to just focus on what I felt like working on in the past. This often led me to delay important
tasks that needed to be worked on first. Such is what happend with my previous GWJ game, the platformer, where I made the assets/arts and then the core gameplay.

This time I was able to focus on my main priority despite having a desire to work or learn about something else. Allowing me to
implement the core mechanics of the game on time.
It required discipline by my part to keep myself on track. At times I had this inkling to work on art or watch pixel art tutorials, but I decided to leave
that aside and go back to that thought once my main priority was accomplished.

### TODO List
Having a todo list helped to ensure that I would focus on what needed to be done.
This is because the top priority was not one that I decided for the sake of it. Rather it was a top priority because it was an important
component to the game, and without it the game would not be playable or fun. 

The todo list reminded be on the tasks that needed to be accomplished.

TODO list: Very helpful.

## Writing down lessons learned
It helps having a list of new things learned while jamming. This way I can go back and see the new discoveries I found, as well as any lessons
I learned throughout the game jam.

## Communication
Communication was a vital component that allowed us to create the game in the end.
Having a structure that would allow us to communicate when needed and discuss ideas help tremendously.

## Notes/ Checkups
Having notes that were done for the day as well as daily check-ins helped us understand the stage that the game was in.
Whether we would need to work more or to scrap ideas if we felt there was not going to have time.

## Patience in Art
Art requires a lot of patience to get it right. 

## Automation if possible
The level engine had to be connected manually. This caused errors when wanting to create a new level, which ultimately led to broken levels 
on the final version of the game jam. Having it be automated would have saved time and avoid the broken levels

## Behaviour Encapsulation
Encapsulating a behaviour helped isolate bugs when they were discovered. As well as making changes or extend the beahviour without affecting other
components of the game. This can also be termed as a module approach.

# Conclusion
Overall, the game jam was a success and we managed to create the game we set out to make. It was big in scope for the size of our team, which led to some exhaustion for me,
but despite this, all the core mechanics were implemented.
I learned many new lessons, and the value of notes and todo lists.

There were in total 70 submissions, and I played as many as I could.

It was an exhausting jam, but one that I enjoyed a lot.
