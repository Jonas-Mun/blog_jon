---
title: "Game Jam - Historically Accurate - Retrospect"
toc: true
authors: ["Jonas"]
date: 2020-01-04T22:41:45-06:00
tags: ["game-jam", "history"]
categories: ["game-jam"]
series: []
draft: false
---

Hosted by HenryTheHistoryCoder at itch.io  

The theme of the Game Jame was the **Fall of the Roman Empire**.

## Game Vision
I initially had envisioned the game to be about a peasant who experiences the death of his family and has to flee from his province. He has to avoid numerous raiders before reaching safety. 
Once safe, he is taken by a group of soldiers who train him to fight against raiders, while helping other provinces from being raided as well. This would all occur while the Fall of the Roman Empire was occuring.

It would be a 3rd person action game with a combat system. Something like in Assassin's Creed with gameplay similar to Diablo II.

It sounded like a fun idea but in hindsight it was rather ambitious. Especially since I just learned the basics of the Unity Engine, and made a very basic top-down game.

## Days of inactivity
I was away for the first three days of the jam. I used this time to think of ideas, as well checking myself if the game would be too much for me since I decided to be making all game assets. 
These assets included: Music, 3D models, Logic, Terrain.

## The Beginnings
With my hands on the computer, I started to implement the basic functionalties I needed:
* Terrain Map (Battlefield)
* Hover Icon
* Clicked Icon
* Info upon click
* Provinces
* Player Province

A simple list, but I did not start there. I started with the scripts and envisioning the software architecture. Having a *Game Manager* with a *Quest Map* that contained the terrain and all the provinces in it. 
Place the provinces in locations that were concoted from somewhere... Quest Map may do...

It all felt rushed, even though I felt I was doing it in a systematic manner.

Looking back, I may have started with the bare essentials (list above) and later though of ways to connect them together. That way I would not have imagined a scenario that may have never existed.
Either way, I was stuck and with the deadline pressuring me I felt I had to go with it. Worse part is, I was not sure what kind of gameplay the game would have.
My hope was to create a combat simulator that took the number of raiders + soldiers and calculated the number of soldiers lost every turn. This made the game rather boring, but I felt I had no other option. Having only the skeleton of a game (Main menu, text, music) but no gameplay. It may have passed as *Good effort*, but I would have been dissapointed.

I felt doomed, and my morale was low.

## Defence Against Raiders - The afterthought title
Fortunately a sign from the Discord server in the form of **RabidColonialist** recommended it to be real-time instead of turnbased. (like the beginnings of Diablo I) It suddenly sparked my curiosity and I started to see many fun possibilities! It made the player think fast; decided where to send soldiers and how many; can they repel the raiders!? I liked the idea and started working on it.

My morale was high. The only problem was that there were two days left for the game to be finished. I did not even have my models completed, and with the mess of code I had (references everywhere. No cohesion!) it made it a lot more complicated than it should have.
But I persevered.

At this time the Discord server started getting a lot more active (or I was just feeling the pressure). Feeling like many people were working hard to make their game as best as they could make it, made it a lot more exciting.
I spent all night, the day before the games were to be submitted, creating models:

* Visigoth (Textures + animation)
* Roman Soldier (Texture + animation)

Many thanks to **Ditzel Games** for his tutorial on 'Blender to Unity'.

And making the game barely work.
I can not recall the order, but I remember spending a lot of time making the: models, texture and animations. As well as making the gameplay work in the most minimal way possible.
I went to sleep with a peaceful mind that at least I had working gameplay. Knowing that if I overslept too much, I still could submit a working game.

## The Last Day
Polish polish polish!
Adding Text, removing assets, fixing bugs and trying to submit my game to **itch.io**
It was done. A barely functioning game with messy code, but it worked.

## Conclusion

#### Software Architecture
I learn a lot creating a game for this game jam. One of the most obvious one for me was my mess of code. I believe this mess of code stems from envisioning a design from the top-down. 

Spending too much time thinking how things should be organized and not thinking about the *things* that need to be organized.
Meaning I should start with the assets that I have and think how I would have them interact.

For example:
I have the following assets:

* Terrain
* Provinces

*How can I organize this?*

Those two assets can be under a **MapSetup**. Where MapSetup is *only* in charge of displaying the terrain and provinces.

Then the question becomes: *How can I initiate the map?* 

I can use a Game Manager that decides when the terrain should start or not.

Using a bottom-up approach rather than a top down approach.


#### Scope of Game

The scope started big and then got smaller as I realized all the things I needed to create **and** test. Having it be 3D meant creating models and animations; taking alot of time.

To avoid this situation I would say to start with the gameplay mechanic, yet this is what I did but I did not have a solid idea of a gameplay mechanic during most of development. Which may explain why the gameplay was not so rich as I wanted it to be.

*How could I avoid this situation?*

Try and think about the gameplay mechanic with no grand vision. No models, no theme. Just the gameplay mechanic. It can be 2D or 3D, I don't think it matters, unless the gameplay is tied to the dimension.
Use dummy models to represent the basic idea, and do not think of any other part of the game until the gameplay is good enough.

Also, the game can be very simple. No need to make it incredible. 

This is my guess. I will implement this idea on the next game jam and see how it goes.


## End Remarks

This was my experience in creating a game for a game jam.
One of many stories amongst all the people who participated.

Overall 13 entries have been submitted, and they look good. At the time of this writing I have played a few and have found them to be fun and very creative.

A huge thanks to **HenryTheHistoryCoder** for hosting this game jam, and for all the participants who created some fun games to play.
