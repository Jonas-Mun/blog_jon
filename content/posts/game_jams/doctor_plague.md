---
title: "Doctor & Plague - Hitorically Accurate Game Jam 2"
toc: true
authors: ["Jonas"]
tags: ["game-jam", "unity", "journal"]
categories: ["game-jam"]
series: []
date: 2020-04-14T01:00:20-05:00
featuredImage: images/doctor_plague.png
draft: false
---

# It Begins a new...

Easter approached and a new game jam, hosted by **Rabid Colonialist**, begins.

For this game jam I was in a team of *five* people. We worked under the umbrella of **Singularity Games**.

# The Black Plague

The theme for this game jam was the **Black Plague**.
We set out to brainstorm ideas for the kind if game we would make. The team already had an idea on what the main story would be: A doctor travels to cities in search for a cure, all the while doing his best to cure those infected.

It seemed the game would be a third person game (If I remember correctly), but our graphics designer, **Victor**, came up with the idea of having it be a card game. A main inspiration was a card game called **Hand Of Fate**. A action game with a bit of a card mechanic.

The main gameplay was set. A card game, but how would it be related to the theme? After a while of thinking, we came up with the story that you are the doctor who is playing against **Death** in order to save lives. This was a brilliant idea. We loved and decided that was going to be the main story that the card game would be based upon.

# Prototype

## Initial Card System.
The card system was based upon *Main* Types which contain *Archetypes*. Where a main type contains an effect, and the archetype contains another effect as well.

Have cards be of a type which contain *archetypes* that play effects. If three of the same cards were in play, then s *Stack* effect would occur based on the *archetype*.

The main types were:
* Doctor
* Death

The archetypes for **Doctor** were:
* Mystic
* Tool
* Medicine

The archetypes for **Death** were:
* Mystic
* Sickness
* Bleed

## Visual Prototype
To have a sense of how the game would look like, I created a quick visual prototype to have an idea of what the player would see when playing.

It consisted of a table, a 3D representation of a person (blocky) and a card on the table.

{{< figure src="doctor_plague/1_prototype.png" alt="proto-imagasdawe">}}

The idea would be to have the player face the doctor.

# Creating the Environment
**Eperson** wass the 3D modeller in our team, and was the person who created all the 3D assets used to create the environment in which the game would be played in.
Below is the evolution of the environment.

## Iterating on prototype

{{< figure src="doctor_plague/Environment/1_add_to_proto.png" >}}

## Adding walls

{{< figure src="doctor_plague/Environment/2_Added_walls.png" >}}

## Creating more space

{{< figure src="doctor_plague/Environment/3_Tables.png" >}}

## Adding 'People'

{{< figure src="doctor_plague/Environment/4_People.png" >}}

**Eperson** did a good job in creating the environment for the game.

# Software Architecture Philosphy

With my previous experience in the last game jam, I tried to make sure to follow a module approach in 
creating behaviours for the game. Utilizing the *Bottom-up* approach as I went along.
Creating the entities first and then thinking of the best way to integrate them.

I began with the *Camera Behaviour*.

## Camera Behaviour
The camera behaviour allowed the player to switch from the forward view to *Top* view and back.

This would allow the user to look at the cards on the table that they have played.

### Implementation
It was implemented using Unity's Animator as it would make my job easier.

The camera behaviour had *two* positions it had to go *to* and *from*.

* Front
* Top

**Front** position would have two animation states: *Front_Idle* and *GoTo_Top*
**Top** position would have two animation states: *Top_Idle* and *GoTo_Front*

The reason for the *GoTo_Top* was to have the camera transition from the *Front* view to the *top view* via the animator.
Once it finished its transition it would then remain in *Top_Idle* until it decides to transition to the
*Front* view via the *GoTo_Front*.

So for every new position for the camera behaviour you would need two states:

* Idle
* GoTo

Where *GoTo* would be started from the previous position that you want to transition from.


## Card System

The Cards would be of two **Main** types and various **Archetypes** which would have various effects.

### Implementation
Initially I tried to write a script for various archetypes for *every* card. This would take for ever to make, and since each type of card would have a main effect, I would be writing the same behaviour in every script for each card.
To avoid the mess of creating a script for every indivdual card, I turned my attention to **Inheritance**.

#### Inheritance-Based System

I first tried to utilize inheritance to store the properties:

* Main Type -> Doctor -> [Effect]
* Main Type -> Death -> [Effect]

* Death -> Arch1 -> [Effect]
* Death -> Arch2 -> [Effect]
* Death -> Arch3 -> [Effect]

* Doctor -> Arch1 -> [Effect]
* Doctor -> Arch2 -> [Effect]
* Doctor -> Arch3 -> [Effect]

All the cards would be under the *Card* and can be passed around the game where their behavoiur would
be extracted based on their final derived class.

{{< figure src="doctor_plague/Card_System/Card.png" >}}

{{< figure src="doctor_plague/Card_System/Card_Inheritance.png" >}}

It seemed promising at first. Where the final derived class would play the effect. There was just one issue due to how I wanted to design the card system.

As each card would be passed around as their *Base* class (**Card**), if I wanted to access the final derived behaviour of the card I would have to check each and every derived class starting from the Main Type all the way down onto the Arch type.
Meaning a lot of switches or ifs just to get the final effect of the card.

```
PlayEffect(Card card) {

    switch(card) {
	case card.Doctor: 
            card.Derive(Doctor);
            card.Arch1:
                card.Derive(Arch1);
                card.PlayEffect();
            card.Arch2:
                card.Derive(Arch2);
                card.PlayEffect();
            card.Arch3:
                card.Derive(Arch3);
                card.PlayEffect();

        case card.Death:
            card.Derive(Death);
            card.Arch1:
                card.Derive(Arch1);
                card.PlayEffect();
            card.Arch2:
                card.Derive(Arch2);
                card.PlayEffect();
            card.Arch3:
                card.Derive(Arch3);
                card.PlayEffect();
        default:
            break;
    }
}
```

Given an Arch3 of type Doctor, I would have to do the following checks:

1. MainType == Death?
2. MainType == Doctor?

3. Doctor == Arch1?
4. Doctor == Arch2?
5. Doctor == Arch3?

6. Play Effect.

everytime I wanted to play a card effect. It could even be that the archetype would have no effect at all, wasting time checking.

This is exactly the kind of scenario I wanted to avoid. The reason for avoiding it is because if I wanted to add a new card, I would have to venture into the *Check System* and manually add the new card that is to be checked. This can become tedious in the long run when there are many cards. As well as adding an extra step when adding a new card.

#### Component-Based System

To avoid the sequential checks I did some research into card systems in youtube. Came across a <a href="https://www.youtube.com/watch?v=aPXvoWVabPY&t=447s">video</a> from *<a href="https://www.youtube.com/channel/UCYbK_tjZ2OrIZFBvU6CCMiA">brackeys</a>* about treating cards as data. This gave me the idea of having the *Card* class that contains data bout the current card.

It would have the following components:

* Main Type -> [Effect]
* Archetype -> [Arch Effect]
* Title
* Description

{{< figure src="doctor_plague/Card_System/Card_Component.png" >}}

This system allow us to play the main effect without having to go through a sequential check.
Same applies to the *Archetypes*.

The way it works is that each *Type* has a **Base Effect** function. This function is what is called to affect the game.
For each new card being derived by a *Type*, the derived card will *override* the **Base Effect** and apply its own effect.

{{< figure src="doctor_plague/Card_System/Base_Effect.png" >}}

It works for our game because we plan for the *Main Type* to have a single effect based on the value it has.

What is flexible about this system is that I don't have to write any checks if I want to add certain properties into the cards.
I simply add a new component inside the *Card* class and have the game logic call that effect when required.

{{< figure src="doctor_plague/Card_System/Card_Component_New_prop.png" >}}

It is beautiful!

No need to venture into the depths of the game logic to play the effect nor have a sequential check to derive the final effect.

#### Display Card
As **Card** only contains data about the card, to displa the card in game requires a *Game Object* that contains
the **Card** as a variable. We called this: **Display Card**

It is the **Display Card** that is seen in game, and not the **Card** data.
Here everything that affects the look of the card in game is placed.

{{< figure src="doctor_plague/Card_System/Display_Card.png" >}}

## Card Factory

With the Card System in place, there needed to be a way to store all cards available in-game.

I was inspired by **<a href="https://catlikecoding.com/unity/tutorials/">CatlikeCoding's tutorials</a>** of *<a href="https://catlikecoding.com/unity/tutorials/object-management/">Object Management</a>* with the use of factories to store
available objects in a *Factory*. So I created a **Card Factory** that would store all available cards in game. 
Whenever a card is needed, simply call the factory for a card and it would return the *Display Card* with the appropriate card data.

{{< figure src="doctor_plague/Card_System/Card_Factory.png" >}}

To save memory in-game, I decided to only store the *Card* data object and not a prefab.
Meaning if I wanted to diplay a card in game I would have to do five steps:

1. Get desired card from factory
2. Instantiate a prefab with desired card model 
3. Add properties to prefab based on Card data (If any)
4. tag the Card data object onto the prefab. 
5. Send it away

{{< figure src="doctor_plague/Card_System/Card_Factory_Flow.png" >}}

As we would be having different main types I decided to have different lists for each main type.
The reason is to avoid having to search through all the main types when I only need to go through a single main type.
Increases efficiency in getting a card.

Now the question becomes: How does a *Card* get to be in the **Card Factory**?

## Card Builder

The Idea behind the card builder was to read a file that contained all the data about all cards, and convert it into a *Raw Card Data* object. This *Raw Card Data* would then be converted into the final **Card** object that would be stored in the Card Factory.

{{< figure src="doctor_plague/Card_System/Card_Builder_Example.png" >}}

The *Raw Card Data* would contain the following:

* Main Type - *Enum*
* Arch Type - *Enum*
* Title
* Value     - float
* Desciption

To convert he *Raw Card Data* into **Card** involved checking the Main and Arch type using switches and enums.

**Main Type**
```
AcquireMain(MainTypeEnum type) {
    switch(type) {
        case Doctor:
            return new Doctor()
        case Death:
            return new Death()
        default:
            No Main Type
    }
}
```

**Arch Type**
```
AcquireArch(ArchTypeEnum type) {
    switch(type) {
        case Arch1:
            return new Arch1()
        case Arch2:
            return new Arch2()
        ...
        default:
            No Arch
    }
}
```

The many switches and enums was not a concer to me as the *Card Builder* would be converting raw data into a desirable object.
As long as it was contained in the Card Builder I did not mind the many switches.

### Obstacle
Here I ran into a problem during development. I did not know how to implement the *Card Builder* properly.

The reason for getting stuck was that I was thinking of two things at the same time which made it harder to know exactly what I needed to do.
Those two things were:

* Conversion from Raw Card Data to Card - Main Functionality
* Reading a file and creating a Raw Card Data from it. - Other Functionality

It seems simple, but I can assure you that at the time I was stuck. 

What I ended up doing was creating the **Card Builder** and creating, manually, various *Raw Card Data*.
It was a temporary solution to have cards be in game.


The funny thing is, this is exactly what I needed to do, yet I was worrying about reading the file *and* creating a *Raw Card Data* at the same time. 

This initial design I scrapped for another which would be scrapped for this again, but in a different form...

### Re-implementing the scrapped design
Why was that? 
I think it is because I tried to do too much at once that I could not focus well enough on the details. So I ended up creating something that was not really needed yet worked and achieved a result. 
I will get back to it later.

## Turn System

As the game involved turns, I created a Turn System using Finite State Machines.
I utilized the design from *Holistic3D* course given in **AI for Beginners**.

My initial states were:

* Begin
* Player Turn
* Effect -> Plays card effect after a turn
* AI Turn
* End of Game

This really was not the initial design. I had many more unnecessary states that I removed later on.

### So many states in the beginning
The reason seems to be because I jumped right into creating the FSM and not in designing the behaviours in each of the states. I have to remember that states represent behaviours.

Anyway, **Effect** was in charge of playing effects and checks if the game is over.

{{< figure src="doctor_plague/Card_System/Turn_System.png" >}}

## GUI System

I took inspiration from my previous game and created the *GUI Manager* that would be in charge of all aspects of GUI.
Making sure it function correctly. My Team mates were the ones that made the GUI look nice with custom designs for the title as well as for the buttons.

### GUI Components
* Main Menu
* In-Game
* Game Over


{{< figure src="doctor_plague/Card_System/GUI.png" >}}
# The Infrastructure

Integrating the above modules together would create the infrastructure in which we would create our game.

* Card System
* Card Factory
* Card Builder
* Turn System
* Camera Behaviour
* GUI

## Integrating the components

The **Main Manager** will integrate the components together.

{{< figure src="doctor_plague/Card_System/Main_Manager.png" >}}

While I was busy creating the infrastructure, my team mates were busy making the 3D models and creating the game mechanics for the card game.
It wasn't until Wednesday/Thursday when we had a concrete set of rules for the game to be based upon.

# Wednesday - Retrospect

As I am detailing the modules that were created, it seems as if it was a breeze to create the various systems for the game.
Most if them were, as they were either simple or I had experience in the past creating similar systems.

The one I had difficulty with was the **Card System**. Trying to figure out how best to have a flexible Card System 
took a lot of experimentation.

Once the Card System was created; containing all the data for a card in a **Card** class, creating the **Card Builder** was fairly straightforward. 

The only issue I had with the Card Builder was that I was thinking of both deserializing via JSON file AND converting that file into a format that the Card Builder can convert into a **Card** object. 
Meaning I was mixing two types of functionalities together which made it difficult to implement the Card Builder. 

The idea was that the deserializing part would convert the data into a **Raw Card Data** Scriptable Object. This object would then be used by the *Card Builder* to build the **Card** object used in game. This **Card** object contained all the data necessary to display the appropriate information in game.

* Gven a file           -> Deserialize  => Raw Card Data
* Given a Raw Card Data -> Card Builder => Card

For now the *Deserialize* module was not implemented so I simply created various *Raw Card Data* SOs to test the Card Builder.

Here one can notice the module approach. The final output of a module is what is used for the input of another module. One can replace a module as long as the connection
is maintained.

# Gameplay

The infrastructure was set, but there was no gameplay. To implement the gameplay I made the following components.

## Game Arena
Where all the game behvaiour takes place.


For the protoype, I had it set up with the following data:

* Row of cards (For players)
* Logic
* Currently played card


## Actors

To represent players or AI, I thought of an Actor class that would be in charge of storing all information about the current player/ actor playing in the *Game Arena*.

They had the following properties:

* Cards in Hand
* Center Hand
* Card Played

*Center Hand* was the center point in which the cards would be 'hovering' in front of the actor. I created an algorithm that would arrange the cards location such that it
made a neat row depending on the center of the actor's hand. It was inspired by **Pascal's Triangle**. This can be its own tutorial.

## Logic
The logic had its own class with a single test:

* Stack Effect

## Input Manager

As the Player Actor was going to expect input, I decided to create an *Input Manager* that would encapsulate certain behaviours I wanted to create.
It could have been inside the Actor's class, but I felt that encapsulating the Input behaviour into its own class would allow for more flexibility in the future.

## Card Movement Behaviour

As I wanted to move cards from one location to another, I decided to create a static class that would move the card for me as I really was not storing the card on the class.
This made it easy to utilize the same kind of movement for various actors as well as for other modules that may require to show movement from one position to another.

Initially the Game Arena was in charge of this movement, but it was already doing too many functionalities. (The Game Arena is where I took a long time to figure out how to implement it in the future)


# The Prototype.
{{< figure src="doctor_plague/2_Final_Prototype.png" >}}

The prototype displayed the kind of game we wanted to create. It only consisted of a row of cards in hand, allowing the player to select cards using the **Input Manager**.
Despite the prototype, the official rules of the game were not in place until Wednesday. I started working on them on Thursday.

But why not on Wednesday? Well, I decided to focus on creating the language support for the game so that I could focus heavily on the gameplay mechanics.

# Language Support

Having multiple files to represent the names of cards in different languages. This was the missing link to produce cards from a file and create them in a game.

I had to learn how to deserialize a JSON file into a class object using Unity's JSON utilities. It took a while, as unity did not have the best documentation on these utilities.
Luckily I found a <a href="https://www.youtube.com/watch?v=M-r4l-OcZtw">tutorial on youtube</a> made by **<a href="https://www.youtube.com/channel/UCGIF1XekJqHYIafvE7l0c2A">First Gear Games</a>** that exaplined how to use the JSON utilities.

My idea of integrating this module into the *Card Builder* was to bypass the *Raw Card Data* and directly conver the deserialized object into the **Card** object.
But there was one issue that would revert this decision, and that is that not only did I implement a language support but I also implemented a custom card creation.

Meaning two languages can have different values for the same card... My teammate **Esperson II** pointed this out. His solution was to only deserialize the text, and create the
information in Unity's editor using a SO called **Card Info**... The last part is the exact solution I had in the beginning for the **Card Builder**... 

The good news is that I only had to change from *Raw Card Data* into *Card Info*, and voila. Language support implemented in one day.


Now to focus on the gameplay mechanics.


# The Gameplay Mechanics

* Piles -> places to place cards
* Piles can be **Open** or **Closed**

* Red, Blue, Green Cards -> Each has specific properties
* Red - first card on a piles
* Red and Blue alternate in piles
* *Green* Cards close piles (Special Cards)
* Recently closed piles - if not countered, affect the population.
* Cards must go in an ascending order: 2 -> 4 -> 6 -> 8 -> 10
* Value 10 closes pile
* City Cards effect the state of the game
* City Cards remove closed piles on the table.

## Data Structures

To help simulate a pile of cards I could have create three lists for each actor, but that would have mean't overloading the Game Arena with a lot of functions and data structures.
I decided to create a data structure that represented both a Pile and the list of piles for each actor.

### Pile
Contained the following properties:

* List of Cards
* Pile ID
* Open/closed status
* Special Card, Queue(Removed)
* Functions
 -- Remove Top Card
 -- Clear Pile
 -- Pile Location
 -- Look at Top Card

### PileList

As actors would have a number of piles to play with, I decided to encapsulate that data into its own data structure

* Dictionary of Piles (id, pile) [A list would also have worked]
* Functions
 -- Get Pile
 -- Pile closed?
 -- Add to Pile
 -- Remove from pile

## Game Arena

The mistake I made in creating the game arena was that I was utilizing the prototype's version of it. Making that version and molding it to fit the
functionalities that I wanted to create. Leading to me overloading the Game Arena with various functionalites instead of a single one.

I had a hard time creating the Game Arena to suite my needs. I believe the reason was that I wanted the Game Arena to represent the place where all the action of the
game would take place, but I did not think what the process of the Game Arena would actually follow. After half a day in thinking of how to do it, I decided to take a break from my cave, and just went out to think what I wanted to do.

I came up with the following logic flow for an action from an actor:

1. Card is chosen
2. Check Card is valid to play on selected pile
3. No?, tell actors they can't play card
4. Yes?, place card in the correct pile.
5. Activate Negate if possible.
6. Close pile if needed


End of Round effect:

1. Check for Closed piles effects
2. Counter if possible

That is it. I was thinking of this cool, leet Game Arena that would be in charge of all the cool game mechanics. Thinking it was this grandiose thing. I was just looking at it top-down... The Game Arena simply followed rules to play a card on the piles or not.


## Logic

The logic of the game was applying the rules so that they were followed. This is what made the game come to life.
It was utilized by the Game Arena.

## Turn System

The turn system had to be refactored to suite the various components that each state had to manipulate.
Each turn followed a process:

1. Choose Card
2. Check card is playable (Via Game Arena)
3. Play Card
4. End Turn?

## AI

The AI would be a dumb one. It would check their hand and apply the rules of the game to check if there are any cards playable.
I implemented this behaviour using a *depth search* approach with rules as nodes. If the search resulted in true, that card would be played.

It would continue to do this search until there are no more cards left in hand and it would end its turn.

## City Cards

The city cards would be the last mechanic to be implemented. The reason was because I had no idea how to do it at the time, and did not want to rush its implementation.
Allowing me to come up with a design that would create the desired behaviour.

### Their own Data Structure

I concluded that it would be best to have their own data structure with their own counter, so that the Game Arena would not have to worry about keeping track on when
the next city card would be played. Rather that would be activated byt the **Effect** state by checking if a city card was to be played before the start of the next round.

It consisted of the following:

* List of City Cards
* Counter - until next City Card is played
* Remove Next City Card
* Activate City Card

Integrating it into the game simply consisted of adding it into the Game Arena and applying the effect after the end of each round. (Module approach is lovely)

# Testing

The last phase was testing for bugs before submitting. Here we were on the last day. 

It was cool being the one who received bug reports and immidieatly when on the hunt to fix. I felt like I was on a mission to squash bugs.

# Submitting

We submitted the game on time. Relief flowed through me and everyone on the team. A whole week of work to get a decent game working...
I was tired, we were all tired. It was a great moment.

# Lessons

## Tutorial

The biggest lesson from this game jam was to have a tutorial **IN** your game. If not, no one would have any idea how to play. Especially for a card game. A text tutorial did not seem viable.

This was the biggest complaint we got, and I felt it to be a major one as people would not be able to experience the fun of the card game that we at Singularity Games created.

## Prototype

Even with a prototype, do not build your next iteration of the game mechanic on it. Rather look at the prototype and see what it is that you like from it and what needs improving.
Build *from* it.

The reason for this is if you build your next iteration of your game on the prototype, all those shortcuts or hard fixes will come to hunt you in the future. This is what happened to me, and what caused my *Game Arena* to become bloated and hard to debug.
Not only that, but as the prototype was created to implement a *certain* gameplay, if that gameplay changes, you would end up suiting the gameplay to fit the prototype which is not nice.

## Prioritizing

The Language Support was not shown in the last version of the game, which is a shame as it was rather decent. This meant that one day spent creating the language support could have been a day in making the gameplay, and an extra day in polishing.

If we decided to scrap the language support for the jam, we could have created a more polished product for the game jam.

Better to think of the core gameplay and implement it first, before delving into various support systems.

## Obstacles

The main obstalce we had was the language barrier. I was the only one that did not speak Portuguese, but lucky for me my teammates could speak in English and had the patience to explain to me their ideas. 

# Conclusion
This journal was my perspective on creating **Doctor & Plague** for the *Historically Accurate Game Jam* hosted by **RabidColonialist**. This tale is one of many, and I am sure my team mates have their own tales to tell.

The end result is satisfactory for us all in the team. We all worked hard to create a card game.
Various games have been submitted, and it is far more than the previous jam. The games all look great.

The lessons I learned from the previous jam, I was able to apply them here, which helped tremendously in debugging and adding functionalities.
I will be going over the code architecture that was used to create the game, and improve upon it as I did in my previous game **Defence Against Raiders**.

I will say that this game was ambitious for the various mechanics it has, as this is the first time I have done something like this. Despite this, we at Singularity Games succeeded in creating a card game. 

# Final Look

**<a href="https://singularity-games.itch.io/doctor-plague">Game</a>**

{{< figure src="doctor_plague/Final_Look.png" >}}

