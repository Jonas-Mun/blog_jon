---
title: Learning Vim - Vimscript
description:
toc: true
authors: []
tags: ["vim"]
categories: []
series: ["learning-vim"]
date: 2021-07-26T21:27:40-05:00
lastmod: 2021-07-26T21:27:40-05:00
featuredVideo:
featuredImage:
draft: false
---

This session I learned about the vimscript language. Mostly the basics that the language can do.
It made me realize how nasty of a language vimscript can be. No types!!!!

# Variables

In vimscript, variables are created using the **let** keyword.
```
:let mine=10
:let funny=10
:let mice="hello there"
```

To display the variable we can types

```
:echom mine
:echom mine | echom funny | echom mice	" Execute three commands conescutively"
```

## Option-Settings as Variables
In vim options such as **number** and **tabwidth** are considered options.
If we simply type:
```
:let number = 3
:let number = 0
```
we will not be changing option setting, rather we would be creating a variable independent from the options *number*

To have the option become a variable we will have to add **&** before the name:
```
:let &number = 0
:let &number = 1
```
See how it now affects the option.

## Local Variables
To specify a local variable for the buffer, we add **&l** onto the option variable:
```
:let &l:number=1
```
This will only affect the current buffer.

## Registers
We can also use registers as Variables
```
:let @a="hello"
:echo @a
```

# Variable Scope
To specify a variable in a local buffer, we add **b:** before the name.

```
let b:fun = 23
```

One can notice a pattern when specifying a type of variable:
```
<type>:<var_name>
```

# Conditionals
The beautiful branching in languages: **if**
```
if <conditional>
    <code0>
endif
```

```
if 1
    echom "It works!"
endif
```

Here is where it gets nasty...
When using a string, nothing would happen:
```
if "hello"
    echom "As it should be"
endif
```

If a string contains a single DIGIT, it will be considered a number and NOT a string...
```
if "myas12"
    echom "This is madness...
endif
```
it will output "This is madness". In vimscript, if there is any digits in the string, it will convert the numbers and ignore the rest of the characters.

## Else/Elseif

```
if <conditional0>
    <code0>
elseif <conditional1>
    <code1>
else
    <code2>
endif
```

# Comparison
It gets a bit more nasty with comparisons. Especially **==**

```
if 10 == 10
   echom "Works as it should"
endif

if 10 > 8
   echom "works as well"
endif

if "foo" == "bar"
   echom "first"
elseif "foo" == "foo"
   echom "second"
endif
```
So far so good, but things become nasty when changing an option.

```
set noignorecase
if "foo" == "FOO"
   echom "vim is case insensitive"
elseif "foo" == "foo"
   echom "vim is case sensitive"
endif
```

still good.

```
set ignorecase
if "foo" == "FOO"
   echom "Oh, no..."
elseif "foo" == "foo"
   echom "yes"
endif
```

Based on the **ignorecase** setting, the behaviour of **==** changes. This can be horrible as plugins or settings can vary between users.

## Solution
Vim provies two operators:

* ==? - case insensitive
* ==# - case sensitive

```
set ignorecase
if "foo" ==# "FOO"
   echom "first"
elseif "foo" ==# "foo"
   echom "second"
endif
```
"Choose one, stick to it".

# Functions
Functions! Yes, beautiful. 

In Vim, a function MUST start with a capital letter.

```
function Meow()
   echom "Meow"
endfunction
```

If we want to create a function and overwrite a previously defined function we add the **!**

```
function! Meow()
   echom "Meow"
endfunction
```

to call this function we use the **call** command.
```
:call Meow()
```

## Returning a value

```
function! GetMeow()
    return "Get Meow"
endfunction
```

when calling *GetMeow()* nothing will be displayed, because there are no side effects. Rather we can do:
```
echom GetMeow()
```

# Function Arguments
Adding a parameter is simple as any other languages, but calling them you must add **a:** before the argument.
```
function! Select(one, two)
    echom a:one
    echom a:two
endfunction
```

## Varargs
To have a variable list of arguments, add **...** inside the parameters.
```
function! Many(...)
    echom a:0
    echom a:1
    echom a:000
endfunction
```
To access an element from the variable argument is: **a:<index>**. To gain access to the list of arguments: **a:000**.

We can even add arguments, and still call Varargs:

```
function! Some(foo, ...)
   echom a:foo
   echom a:0
   echom a:000
endfunction
```

## Assignment
The arguments are immutable. In order to modify a value given by an argument, you must first assign a variable to it:

```
function! AssignGood(foo)
    let foo_t = foo
    let foo_t = "Yo"
    echom foo_t
endfunction
```

# Numbers
Their behaviour is similar to **int** in C. 

You can include exponential:
```
echo 15.45e+3
```

# Strings

## Concatenation
Use **.** to concatenate two strings. Do NOT use *+*, as that only works for numbers.

```
echom "hello" . " World"
echom 10 . "Dude"
```

## Special characters
Escape sequences using **\\**
```
echom :foo \"var\""
```

### Difference between echom and echo.

**echom** will echo the exact characters. Whereas **echo** will replace the escaped sequence with the correct command.

## Literal Strings
Literal strings are those where no commands or escape sequences are replaced. They are shown as depicted.

They are depicted using **'** quote

```
echom '\n\\'
```

# String Functions
There are various functions that deal with Strings. Some of the common ones are:
* join()
* split()

```
:help functions
```

# Conclusion
Vimscript can be a nasty language, but if learning vimscript means taking advantage of vim's potential then I believe it is worth learning it.
