---
title: Memory and Cache
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-10T13:57:35Z
lastmod: 2022-11-10T13:57:35Z
featuredVideo:
featuredImage:
draft: false
---

This article is a continuation of the micro-architecture overview.

In the last article we used the following equation to discuss performance:

$CPU execution time = Instructions of program * CPI * clock cycle period$

This equation can be followed in two cases:

* No memory instructions are in the program
* The time to access memory is equivalent to the clock cycle period.

To be more precise, when the clock rate of memory is less than the CPU's clock rate,
there is a delay in passing data to the CPU.
This is because the memory's clock rate can not send data in time for every CPU's clock cycle.

## Example

Take for example a memory with a clock cycle period of 1000ps, and a CPU with a clock cycle of 250ps.

If the CPU is executing a *load* instruction, it will have to wait 1000ps to retrieve the data.
This requires $\frac{1000}{250} = 4$  clock cycles for every load instruction!

One way of mitigating the clock cycles lost from accessing memory is to place a memory component between the CPU and the main memory.
This hardware component is called the **Cache**

# Caches

A cache is simply a memory component that stores data.
It uses the **Principle of Locality** to improve CPU performance.

## Principle of Locality

Principle of Locality can be split into two concepts:
* Temporal Locality
* Spatial Locality

*Temporal locality* states that the recently referenced data will be accessed again.
*Spatial locality* states that the data round the recently reference data will be used.

It uses these two concepts to store data in the cache.

For example, the storage of instructions. 
Due to the nature of program instructions, acquiring an instruction means that the next instructions will be used.
Instead of just storing the reference instruction only, it stores some of the next instructions as well.
This allows the CPU to only access the Cache, avoiding accessing the main memory.


With this in mind, one may ask:
*How is a cache built?*

## Internals of a Cache

A simple cache is seen as a *table* with entries, where the smallest unit of information is called a **block**.
The cache is used by using the *memory address*.


```

 idx
+---+--------+--------+
| 0 |  tag0  |  data  |
+---+--------+--------+
| 1 |  tag1  |  data  |
+---+--------+--------+
| 2 |  tag2  |  data  |
+---+--------+--------+
| 3 |  tag3  |  data  |
+---+--------+--------+
```

where *idx* is termed the *index*.

In this example, a *block* is an entry of the cache. 

There are two questions asked when dealing with caches:

1. How can we access a block of information in the cache?
2. How do we know if an item is in the cache?

This two questions are related.

To answer question 1., the index used for the cache corresponds to a section of a given *memory address*.
For a basic cache implementation, the index would be:

$(Memory address) mod (Total Entries in cache)$

If the total number of entries in the cache was a *power of 2*, then the total bits required from the memory address would be:

$log_2 n$

Where $n$ is the total entries in the cache.

For example, say the total entries of a cache is *4096*, given a 32-bit address space.
Then the required bits for the index would be:

$log_2 (4096) = 2^{12}$

Be $12$!

Now, how do we verify that this item is the one we seek?
This is where the *tag* comes into play.

### Tag

The tag contains the remaining bits in order to verify that the data pointed by the memory address is the one we seek. 

For the previous example, the number of bits used for the tag would be: *32-12 = 20 bits*.

### Cache Basics Summary

A basic cache contains the following:
* Index
* Tag
* Data

The *Index* is used to find an entry, and the *Tag* is used to verify that the entry is the correct one.

In reality, a cache entry contains a *dirty bit* that tells whether the data has been modified.
This helps when deciding when to write back to memory when data is being swapped or during cache coherency.

### Accessing Data

Say we have a *8-entry* cache, with a 16-bit address space.

1. *How many bits are required for the index?*
2. *How many bits are required for the tag?*

1. $log_2 8 = 3$
2. 16 - 3 = 13

Take the following Cache:

```
 idx
+-----+-------------+--------+
| 000 |   empty     |  data  |
+-----+-------------+--------+
| 001 |   empty     |  data  |
+-----+-------------+--------+
| 010 |   empty     |  data  |
+-----+-------------+--------+
| 011 |   empty     |  data  |
+-----+-------------+--------+
| 100 |   empty     |  data  |
+-----+-------------+--------+
| 101 |   empty     |  data  |
+-----+-------------+--------+
| 110 |   empty     |  data  |
+-----+-------------+--------+
| 111 |   empty     |  data  |
+-----+-------------+--------+
```
We have the following memory addresses to access to load data from:

```
1. 1100 0011 1010 0000
2. 0000 0101 1110 0001
3. 0010 1000 0111 0000
4. 1100 0011 1010 0000
```

When loading the first address, we notice the entry is not in the cache because it is *empty*!
Before we continue, I have to say that we cheated a bit by using the term *empty*.

To determine if the entry contains invalid data/unavailable, the cache entries would contain a **Valid Bit**.
Thus the cache above would be:

```
 idx    V
+-----+---+------------+--------+
| 000 | 0 |  -----     |  data  |
+-----+---+------------+--------+
| 001 | 0 |  -----     |  data  |
+-----+---+------------+--------+
| 010 | 0 |  -----     |  data  |
+-----+---+------------+--------+
| 011 | 0 |  -----     |  data  |
+-----+---+------------+--------+
| 100 | 0 |  -----     |  data  |
+-----+---+------------+--------+
| 101 | 0 |  -----     |  data  |
+-----+---+------------+--------+
| 110 | 0 |  -----     |  data  |
+-----+---+------------+--------+
| 111 | 0 |  -----     |  data  |
+-----+---+------------+--------+
```

Going back to the loading of data, we check the *valid bit* at index *000* and realize that no data is stored.
This means that we stumbled onto a **Cache Miss**.
When a *cache miss* happens, the OS will have to fetch the data from memory and update the cache.
The time it takes to recover from a *cache miss* is known as the **miss penalty**.
That is, the time taken to access main memory, fetch data and store it into cache, then restarting the instruction to fetch the data from the cache.


In our case, it is a *cahce miss*, so we call the OS to fetch the data from the main memory and update the cache, then restart the instruction:

```
 idx    V
+-----+---+-------------------+--------+
| 000 | 1 |  1100 0011 1010 0 |  data  |
+-----+---+-------------------+--------+
| 001 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 010 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 011 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 100 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 101 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 110 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 111 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
```

Now we are given the following memory address: *0000 0101 1110 0001*

Just like last time, the Valid bit for *001* is a miss! We fetch the data from memory and update the cache entry.

```
 idx    V
+-----+---+-------------------+--------+
| 000 | 1 |  1100 0011 1010 0 |  data  |
+-----+---+-------------------+--------+
| 001 | 1 |  0000 0101 1110 0 |  data  |
+-----+---+-------------------+--------+
| 010 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 011 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 100 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 101 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 110 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 111 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
```
Now we get the following memory address: *0010 1000 0111 0000*
We use the *000* bits to index the cache and realize that an entry is already stored!
We have to replace this entry with the desired data.
This is the same as a *cache miss* as the data is not in the location we want.

```
 idx    V
+-----+---+-------------------+--------+
| 000 | 1 |  0010 1000 0111 0 |  data  |
+-----+---+-------------------+--------+
| 001 | 1 |  0000 0101 1110 0 |  data  |
+-----+---+-------------------+--------+
| 010 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 011 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 100 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 101 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 110 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 111 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
```

Now we get the next instruction: *1100 0011 1010 0000*
Because the index *000* is already taken, we have to call it a *cache miss* and replace the value.

```
 idx    V
+-----+---+-------------------+--------+
| 000 | 1 |  1100 0011 1010 0 |  data  |
+-----+---+-------------------+--------+
| 001 | 1 |  0000 0101 1110 0 |  data  |
+-----+---+-------------------+--------+
| 010 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 011 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 100 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 101 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 110 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
| 111 | 0 |  -----            |  data  |
+-----+---+-------------------+--------+
```

The type of cache we are using is called a **Direct Mapping cache**.
That is, for every memory address, there is one entry in the cache to compare.

**Cache misses** are a factor that contribute to the performance of the CPU.
This is because a *cache miss* resorts to accessing main memory, which can stall the execution of an instruction.



## CPU Performance using a Cache
We now go back to the initial equation:

$CPU execution time = Instructions of program * CPI * clock cycle period$


To take into account the memory-stalls in the CPU performance, we utilize the following equation:

$CPU execution time = (CPU execution clock cycles + Memory-stall clock cycles) * clock cycle period$

The *cpu execution clock cycles* should be $Instructions of program * CPI$ (I think...).

*Memory-stall clock cycles* can be calculated as follows:

$Memory-stall clock cycles = (Read-stall cycles + Write-stall cycles)$

It is made up of the cycles spent *reading* from memory and *writing* to memory.

These are calculated as follows:

$Read-stall cycles = \frac{Reads}{Program} * Read miss rate * Read miss penalty$

$Write-stall cycles = (\frac{Writes}{Program} * write miss rate * write miss penalty) + write buffer stalls$

In some cases, the *read* and *write* stalls are the same so the above equation can be reduced to the following:

$Memory-stall clock cycles = \frac{Memory Access}{Program} * Miss rate *  miss penalty$

OR

$Memory-stall clock cycles = \frac{Instructions}{Program} * \frac{Misses}{Instruction} *  miss penalty$

Notice how a factor that affects the *CPU Execution time* is the **Miss rate** and **Miss Penalty**, when dealing with memory stalls.

There are two ways of reducing memory stalls when using caches:

* N-way associative cache - Reduces *miss rate*.
* Multi-level caches - Reduces *miss penalty*.

## N-Way Associative Cache

An *n-way associative* cache can store multiple blocks per entry.
In the *direct mapping* cache, (an 1-way associative cache) there was one block per entry. 



If we were to use a 2-way associative cache, there would be *two blocks* per entry.
For example, take our previous cache with *8-entrie* with a 16-bit address.

```
 idx
+-----+-------------+--------+------------+--------+
| 000 |   empty     |  data  |  empty     | data   |
+-----+-------------+--------+------------+--------+
| 001 |   empty     |  data  |  empty     | data   |
+-----+-------------+--------+------------+--------+
| 010 |   empty     |  data  |  empty     | data   |
+-----+-------------+--------+------------+--------+
| 011 |   empty     |  data  |  empty     | data   |
+-----+-------------+--------+------------+--------+
```

Notice how this reduces the number of bits needed for the index. From 3-bits to 2-bits.

### Example

Take the state of our cache as follows:

```
 idx
+-----+---+-------------------+--------+---+-------------------+--------+
| 000 | 1 |  0010 1000 0111 0 |  data  | 0 |  1100 0011 1010 0 | data   |
+-----+---+-------------------+--------+---+-------------------+--------+
| 001 | 1 |  0000 0101 1110 0 |  data  | 0 |  -----            | data   |
+-----+---+-------------------+--------+---+-------------------+--------+
| 010 | 0 |  -----            |  data  | 0 |  empty            | data   |
+-----+---+-------------------+--------+---+-------------------+--------+
| 011 | 0 |  -----            |  data  | 0 |  empty            | data   |
+-----+---+-------------------+--------+---+-------------------+--------+
```

So far we have accessed the following sequence:

```
1. 1100 0011 1010 0000 (ACCESSED)
2. 0000 0101 1110 0001 (ACCESSED)
3. 0010 1000 0111 0000 (ACCESSED)
4. 1100 0011 1010 0000
```

Now, when accessing the memory in the fourth instruction, we no longer have a cache miss.
We get a **Cache hit**!!

Thus, reducing the number of cache misses occurring.

A cache whose associativity is the total number of entries is called a **Full-associative** cache.

## Multi-level Caches

At the moment we only have one cache between the CPU and the main memory.
When a cache-miss occurs, then the OS will have to fetch the memory from the main memory. 
The time taken to fetch the data back is known as the **Miss penalty**.

What if we have a cache after the first cache, that would be responsible for storing more data than the first cache?
That is, we create two separate caches, each with a specific design choice:

* L1 Cache -> Used to reduce the *hit time*. (Time taken to find the item. Less items would generally be stored in L1 cache.)
* L2 Cache -> Used to reduce the *miss penalty*, by generally being larger (hence, reduces *miss rate* as well)

Say L2 has stored a ton of data in its cache.
When there is a cache miss in L1, the L2 cache is used to check if it contains data.
Because L2 is faster than the main memory, not a lot of clock cycles are lost as opposed to only using L1 cache and the main memory.

The *miss penalty* is reduced!

This brings up an important question:
*How do we measure how much multi-level caches improve performance?*

### Multi-Level cache Performance

We now use an example from the Book: Computer Organization

#### One-Level Cache
We are given a processor and a primary cache with the following specs:
* CPI -> 1.0
* 4GHz
* Primary cache miss rate per instruction -> 2%

The following is the memory specs:
* 100ns access time.

What is the *total CPI* given these specifications?

This can be found by using:

$$ Total CPI = CPU execution clock cycles + Memory-stall clock cycles $$

We first have to find out the total clock cycles lost when using memory.

As 4GHz is encompasses in nanoseconds, we can get $$ \frac{1}{4} = 0.25ns$$. Meaning the clock cycle period is *0.25* nano seconds.
TO get the memory-stall clock cycles:

$$ \frac{100}{0.25} = 400 $$ clock cycles.

That is, 400 clock cycles are lost when accessing memory.
As only $$ 2\% $$ of these instructions access memory, then:

$$ 400 * 0.02 = 8$$

on average 8 clock cycles are lost per instruction.
We now have enough information to calculate the total CPI:

$$ Total CPI = 1.0 + 8 = 9 $$

#### Second-Level Cache

Say we have a secondary cache with the following specs:
* 5ns access time
* 0.5% miss rate

We first must calculate the stall clock cycles when accessing this second cache.

$$ \frac{5}{0.25} = 20$$
20 clock cycles spent accessing the second cache.

With this information we can now calculate the Total CPI. We use the following equation:

$$ Total CPI = Base CPI + Primary Memory Stall + Secondary Memory Stall$$

With the information we have obtained, we substitute the equation to get:

$$Total CPI = 1.0 + 2\% * 20 + 0.5\% * 400$$

$$Total CPI = 1.0 0.4 + 2.0$$
$$Total CPI = 3.4$$

The total CPI when using a second-level cache is 3.4.
That is, *on average* 3.4 clock cycles are executed per instruction.

To compare performance, we use the following ratio:

$$ \frac{9}{3.4} = 2.6  $$

Using the second-level cache is 2.6 times faster than only using one cache.
