---
title: Terminal Workflow Setup
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-16T21:36:28Z
lastmod: 2022-11-16T21:36:28Z
featuredVideo:
featuredImage:
draft: true
---

In this article I plan to detail the tools I use when working.

# Initial tools

* mpv - Video Player
* ytfzf - Search engine for youtube in Terminal
* yt-dlp - Downloader for youtube (Used alongside ytfzf)
* newsboat - Update on specific websites, including youtube channels, using RSS.
* w3m - Web browsing in terminal

These tools are what enable me to focus on my tasks.
I tend to get distracted a lot when using a normal browser.

As for editing:

* tmux
* vim/neovim
