---
title: C++ Knowledge
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-26T15:31:11Z
lastmod: 2022-11-26T15:31:11Z
featuredVideo:
featuredImage:
draft: false
---

In this article I will be going over *Inheritance* in C++ to fill my gaps of knowledge.

# Derived Classes

Say we have the following class:

```
class B {
    public:
        int x;
        B(int p_x = 0) : x(p_x);
};

class C : public B {
    public:
        int y;
        C(int p_y = 0) : y(p_y);
}
```

When we instantiate C:
```cpp
C mc = C();
```

What is being done is:
1. Allocate memory to store C and B
2. Call Constructors starting from the Base-case class. In this case it is *B*

## Passing Arguments to Base Class

How can we call C and change *x* in the constructor??

We can *not* use the *initializer list* to do this.
We must resort to *assignment* in the *C* constructor:

```
class C : public B {
    public:
        int y;
        C(int p_y = 0, int p_x) : y(p_y) {
            x = p_x;
        }
}
```

Why is this the case? It is to do with *const* and *references*.
When initializing a *derived class*, the initialization starts from the top-most base class and goes downwards.
By using the initializer list, we would be re-initializing values already initialized in the base class.

This is not ideal for *consts* which must be initialized *once* and never be changed. 

The solution proposed is not ideal because the variable *x* is assigned twice.

## Specifying Base constructor

We can avoid the double assignment by specifying the kind of constructor we want the Base class to use.
This can be done as follows:


```
C(int p_y = 0, int p_x) : B(5), y(p_y)  {
    x = p_x;
}
```

### About friend

THe *friend* keyword makes the method be able to acces both *private* and *protected* members of the class.
This is mostly used to override the *output stream* *<<* operator:

```
friend std::ostream& operator <<(std::ostream& os, const C& c) {
    os << C.y << C.x;
    return os;
}
```

# Types of Inheritance
The common kind of inhertiance was *public*:
```
class C : public B {
    ...
};
```

But there are other kinds of inheritance.
* private - All things in base class are private
* protected 

# SUMMARY

* When initializing a derived class, memory is allocated to store all the inherited classes.
* The initialization of a derived class starts form the top-most base class and works downwards.
* THe derived class can specify the Base class constructor to initialize variables.
* Deconstruction of derived classes starts from the bottom-most derived class and works its way up.
