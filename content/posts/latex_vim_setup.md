---
title: Latex Vim Setup
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2023-04-22T21:43:22+01:00
lastmod: 2023-04-22T21:43:22+01:00
featuredVideo:
featuredImage:
draft: false
---

Plugins needed:
* vimtex - https://github.com/lervag/vimtex

```
" vim-plug
Plug 'lervag/vimtex'
```

Need to install zathura:
```
sudo apt-get install zathura-poppler
```

Once latex has started, you can being compilation via:
```
<leader>ll
```

# Math Advice

## For Definitions and Theorems
```
\usepackage{mathtools}
\usepackage{amsthm}

\theoremstyle{remark}
\newtheorem*{remark}{Remark}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}{Corollary}[theorem]

\newtheoremstyle{break}
  {}{}%
  {}{}%
  {\bfseries}{}%
  {\newline}{}

\theoremstyle{break}
\newtheorem{definition}{Definition}[section]

\begin{document}
...

\begin{definition}[title]
...
\end{definition}

\end{document}
```

## Blocks

```
\begin{quote}
...
\end{quote}
```

