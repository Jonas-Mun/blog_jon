---
title: "Understanding_vulkan_framebuffer"
date: 2020-12-21T07:07:39-06:00
draft: true
---

# Frame Buffer
With the **Render Pass** created, a frame buffer will provide the
attachments that the Render pass will use.

{IMAGE_0}

The **FrameBuffer** references the **VkImageView** that represent the
attachments in the RenderPass.

{IMAGE_1}

The Framebuffer used is dependent on the current **Image** that will
be used for presentation. As such, a framebuffer for each 
image in the swapchain will have to be created.

{IMAGE_2}

# Command Buffers
Commands, such as operations and memory transfers, must be recorded
before sending them to the GPU. 

{IMAGE_3}

To do so, a **Command Pool** must be created in which will contain
all the commands of a given type. This type is dependent on
the type of queue in which the command buffer will be submitted to.

{IMAGE_4}

The command pool is stored in a **VkCommandPool**.

## Command Buffer Allocation

For this tutorial, the command buffers will have to be binded to
the **Frame Buffer** due to a specific drawing command.

Meaning for every *frame buffer* there is a **Command Buffer**.

{IMAGE_5}

### Creating Command Buffers

To begin creation, first the command buffer needs to be allocated
using the **VkCommandBufferAllocateInfo**.

Specifying the command pool and the number of buffers.

```
VkCommandBufferAllocateInfo allocInfo{};
...

vkAllocateCommandBuffers(logicalDevice, &allocInfo, commandBuffers.data());
```

{IMAGE_6}



## Recording Commands

The **vkBeginCommandBuffer(VkCommandBuffer, VkCommandBufferBeginInfo)**
function is used to begin the recording for a specific *Command Buffer*.

## Starting a Render Pass
To begin a render pass, first a **VkRenderPassBeginInfo** must be
created that contains:
* renderpass
* frame buffer
* Swap chain extent
* clear color

Once it has been created, a render pass can begin using the following function:

```
vkCmdBeginRenderPass(VkCommandBuffer, VkRenderPassBeginInfo, subpass_type);
```

## Drawing Commands

### Binding pipeline
To beging drawing, the **graphics pipeline** must first be binded to the
command.

```
vkCmdBindPipeline(VkCommandBuffer, bind_type, VkPipeline);
```

### Drawing

A simple function:

```
vkCmdDraw(VkCommandBuffer, vertexCount, instanceCount, firstVertex, firstInstance);
```

## Finishing recording
Once all the desired render pass commands have been recored,
we can end it by calling: **vkCmdEndRenderPass(VkCommandBuffer)**.

Then the recording can be finished with:

```
vkEndCommandBuffer(VkCommandBuffer);
```
