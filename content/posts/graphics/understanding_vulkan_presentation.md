---
title: "Understanding Vulkan Presentation"
description:
toc: true
authors: ["Jonas"]
tags: ["vulkan", "c++", "graphics"]
categories: ["graphics"]
series: ["understanding-vulkan"]

date: 2020-12-04T05:32:04-06:00
draft: false
---

This is going over the vulkan tutorial. Specifically the **Presentation** section in a visual manner.

# Current Setup
From the last section of the tutorial we have the following setup:

{{< figure src="understanding_vulkan_presentation/1_starting.png" >}}

At the moment we have access to gpu and a queue in which we can send commands to. Yet there is no manner to display the rendered images.

# Windows System
For this we use a *Window System* that renders the desired images

{{< figure src="understanding_vulkan_presentation/2_Window_System.png" >}}

So we need to have Vulkan be able to communicate to a *Windows System* in order
to render images.

## Vulkan Surface Extension
The *Window SYstem* is dependent on the current platform it is on.
As Vulkan is a platform agnotstic API, it uses an extension to be able
to communicate to a window.

This extension is a **VkSurfaceKHR**. From the tutorial:
```
"...represents an abstract type of surface to present rendered
images to"
```

{{< figure src="understanding_vulkan_presentation/3_Surface.png" >}}

From the surface extension, Vulkan can handle various platforms:
* Win32
* GLFW

{{< figure src="understanding_vulkan_presentation/4_surface_platform.png" >}}

Each window system has its own manner to create the **VkSurfaceKHR**.

GLFW
```
VkSurfaceKHR surface;
if (glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS) {
    ...
}
```

Win32
```
VkSurfaceKHR surface;
VkWin32SurfaceCreateInfoKHR createInfo{};
...
vkCreateWin32SurfaceKHR(instance, &createInfo, nullptr, &surface);
```

## Presentation Queue Family
Presentation commands are different from the graphics commands, so we must
query the gpu to check whether it supports a queue family for presentation operations.

{{< figure src="understanding_vulkan_presentation/5_presentation_queue_family.png" >}}

It could also be that the same queue from the *Queue Graphics* family also supports operations for operations.

{{< figure src="understanding_vulkan_presentation/5_5_presentation_queue_family_same.png" >}}

The presentation queue family must also support the *surface* that is being used.

{{< figure src="understanding_vulkan_presentation/6_surface_support.png" >}}

## Presentation Queue
Once we have found the physical device (gpu) that supports the desired queue families, we can then have a handle to each specific queue we desire to use.

{{< figure src="understanding_vulkan_presentation/7_queue_handle.png" >}}
# Swap Chain
When rendering to the window, the data of all the pixels is stored in a **Frame Buffer**.

{{< figure src="understanding_vulkan_presentation/8_frame_buffer.png" >}}

From the *Frame Buffer*, the window uses the data and displays it.

In Vulkan there is no such thing as a *default* frame buffer, so it must be constructed. Here is where the **Swap Chain** comes into play.

Taken from the tutorial:
```
swap chain is a queue of images waiting to be presented to the screen
```

{{< figure src="understanding_vulkan_presentation/9_Swap_Chain.png" >}}

*...the general purpose of the swap chain is to synchronize the presentation of images with the refresh rate of the screen*

## Checking for Compatibility
We already checked if our GPU supports presenting images to a window.
As the swap chain is tied to the windows sytems and surface, it s an *extension*
to the Vulkan API.

**VK_KHR_swapchain**

The physical device must be checked to see if it supports a *swap chain*.

{{< figure src="understanding_vulkan_presentation/12_swap_chain_support.png" >}}

## Surface and Swapchain Compatibility
It could be that the surface does not support the swap chain that the physical device does.

**From what I understand**, the swap chain will contain the image data that is to
be rendered to the windows. The *surface* is a manner to present images to the window.
These two must be able to support the same kind of images that they will handle.

{{< figure src="understanding_vulkan_presentation/13_Image_support.png" >}}

The properties to check are:
* Basic Surface Capabilities - min/max number of images, min/max width and height of images
* Surface formats - pixel format, color space
* Available presentation modes

### Surface Format

The Image data may be organized in different manners:
* RGB
* BGR
* BGRA
* RGBA

As well supporting different *color spaces*

### Presentation

How the images are going to be submitted to the screen is dependent on the 
mode the swap chain will be at:
* Immediate 
* FIFO
* FIFO Relaxed
* Mailbox

### Swap Extent

The Resolution of the swap chain images.

## Creation
With all the properties and compatibility checks, we can now create the swap chain

{{< figure src="understanding_vulkan_presentation/14_Swap_chain_Creation.png" >}}

## Retrieving swap chain Images
We can store handles to each swap chain images.

Vulkan has a **VkImage** structure that stores the image data.

```
std::vector<VkImage> swapChainImages;

vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr);
swapChainImages.resize(imageCount);
vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImages.data());
```

{{< figure src="understanding_vulkan_presentation/15_Image_Handle.png" >}}


# Image Views
In order to use the **VkImage**, we have to create a **VkImageView** object in order
to "say" how the image can be used and which parts of the image to access.

As the **VkImageView** are tied to the **VkImage**, we have to iterate through
all the **VkImage** in the swap chain and create a **VkImage** out of it.

{{< figure src="understanding_vulkan_presentation/16_Image_View.png" >}}

The **VkImageViewCreateInfo** contains the information needed to create a **VkImageView**
object.

