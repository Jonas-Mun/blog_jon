---
title: "Understanding the need for Graphics APIs"
date: 2020-07-24T20:50:43-05:00
draft: true
---

These series of posts are a way for me to understand the graphics pipeline, and how it came to me.
There may be errors, and if so please feel free to point them out so that I may correct them.

# The CPU & GPU - device drivers

These two devices nee to communicate, but the question becomes: How?

A **device driver** allows the CPU to communicate with the GPU.

IMAGE

Now imagine we have an application called *Super jumping man*. For the moment, our app is able to utilize the GPU
through the device driver to render graphics onto the screen.
Everything works as intended, but what if we decided to use a different GPU, say GPU_Next?

*Super jumping man* will not be able to run on our new GPU_Next because the commands it used to render on the screen
were based on the device driver for our previous GPU.

We can have *Super jumping man* support the new GPU_Next by having a seperate version that utilizes the device driver of GPU_Next.
IMAGE
The issue is that now the application is supporting two versions of the same game, which can be tedious to support in the long run.
Not only is it tedious but this must be done for all the available GPUs in the market. A daunting task.

IMAGE

To avoid having to support various GPUs for a specific application, APIs are utilized.

# APIs
The API is what the application, in our case *Super jumping man*, will use to render frames on the screen. 
It is the API that is in charge of communicating with the various GPUs available through their device drivers. Removing the need for the
application to manually support all GPUs in the market. Rather it only has to support the given API.

IMAGE

Why go through all this hassle, can't the CPU render frames to the screen?
The answer is yes, and we shall see just how it has been done in the past.
