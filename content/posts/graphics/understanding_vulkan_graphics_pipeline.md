---
title: "Understanding Vulkan - Graphics Pipeline"
description:
toc: true
authors: ["Jonas"]
tags: ["vulkan", "c++", "graphics"]
categories: ["graphics"]
series: ["understanding-vulkan"]

date: 2020-12-18T05:53:14-06:00
draft: false
---

This article will no go in depth into how a graphics pipeline works.
It willl outline the basic functinality of the graphics pipeline and
then delve into how Vulkan creates a graphics pipeline.

# Graphics Pipeline
The basic function of a graphics pipeline is by taking input (Vertex data)
and producing a rendered image.

{{< figure src="understanding_vulkan_graphics_pipeline/0_Graphics_Pipeline.png" >}}

Two stages of the graphics pipeline are programmable:
* Vertex Shader - How to manipulate vertices
* Fragment Shader - How to manipulate final color per pixel

The rest are known as *fixed functions*

# Graphics Pipeline in Vulkan

## Shader Modules
In Vulkan, **Shader Modules** are used to provide the *Vertex* and *Fragment* functionality. These shaders are contained in a **Spirv** byte format, so a conversion from GLSL to SPIR-V is required.


{{< figure src="understanding_vulkan_graphics_pipeline/1_spirv.png" >}}

With the *SPIR-V* data, a **VkShaderModuleCreateInfo** stores
the information for the creation of the shader module.

{{< figure src="understanding_vulkan_graphics_pipeline/1_1_spirv_structure.png" >}}

```
VkShaderModuleCreateInfo createInfo{};

...

VkShaderModule shaderModule;
vkCreateShaderModule(logicalDevice, &createInfo, nullptr, &shaderModule);
```

{{< figure src="understanding_vulkan_graphics_pipeline/2_Shader_Module.png" >}}

For the tutorial, a shader module for *vertex* and *fragment* are created.

{{< figure src="understanding_vulkan_graphics_pipeline/3_Shader_Module_Tutorial.png" >}}

### Assigning Shader Modules
At the moment we have our shader modules we want to use, but they are
not being used at the moment. We have to specify when they will be
used during the pipeline.

This is achieved by using the **VkPipelineShaderStageCreateInfo**, that
stores information of an *individual* shader module and when it should be
used in the pipeline.

{{< figure src="understanding_vulkan_graphics_pipeline/4_Shader_Assignment.png" >}}

## Fixed Functions
The next stages in the pipeline have to be configured in Vulkan

### Vertex Input
Describes the vertex data that will be binded to the vertex Shader.
This includes the vertex buffer and the amount of attributes.

Whenever a Vertex buffer is to be used, it has to be mentioned in the
**Vertex Input** stage of the pipeline.

It is create using: **VkPipelineVertexInputStateCreateInfo**

### Input Assembly
Described the kind of geometry that is to be created from the input, and whether
*restart* is enabled. (I don't know what it means, yet)

It is created using: **VkPipelineInputAssemblyStateCreateInfo**

### Viewports and Scissors
#### Viewport
THe viewport is the region of the framebuffer where the data will be rendered to.

It is tied directly to the *extent* of the **Swap Chain**.

#### Scissors
Define the region in which pixels will be stored in the frame buffer.

If the *scissor* region is less then the viewport, then only the pixels in the
*scissor region* will be displayed.

{{< figure src="understanding_vulkan_graphics_pipeline/5_Scissors.png" >}}

**VkRect2D**

It is related to the *extent* of the **Swap Chain**

The *Viewport* and *Scissor* rectangles are combined in the: **VkPipelineViewportStateCreateInfo**.

### Rasterizer
The rasterizer is responsible for rendering the geometry into a series of pixels.

I won't go into depth as I am focusing on the setup of a graphics pipeline in Vulkan.

The structure **VkPipelineRasterizationStateCreateInfo** is used to
create the rasterizer

### Multisampling
Involves sampling a the geometry in a pixel multiple times to gather an average result.

This is how *anti-aliasing* is done.

The structure used to create is: **VkPipelineMultisampleStateCreateInfo**.


### Depth and Stencil

Uses a **VkPipelineDepthStencilStateCreateInfo**.

### Color Blending
Combines the color from the fragment shader to the previous framebuffer.
It uses two structures:
* **VkPipelineColorBlendAttachmentState**
* **VkPipelineColorBlendStateCreateInfo**

# Dynamic State
Certain sections of the pipeline may change throughout the application.
Vulkan provides a **VkDynamicState** structure that defines which state
in the pipeline will change:

(Example from the tutorial)
```
VkDynamicState dynamicStates[] = {
    VK_DYNAMIC_STATE_VIEWPORT,
    VK_DYNAMIC_STATE_LINE_WIDTH,
};
```

This structure is reference in the **VkPipelineDynamicStateCreateInfo** structure.

# Pipeline Layout - Uniform Data
Uniform values are set for the shaders to use. The values can changed and must
be specified.

{{< figure src="understanding_vulkan_graphics_pipeline/9_Uniform_Buffer.png" >}}

A **PipelineLayout** stores information about the *uniform data*. 

**VkPipelineLayout**

If no data is to be used, then an empty pipeline layout will be created.

A **VkPipelineLayoutCreateInfo** is used to create a the pipline layout.

# Render Pass
When rendering an image, there can be situations when multiple passes must occur
to produce the desired final render.

{{< figure src="understanding_vulkan_graphics_pipeline/11_Render_goal.png" >}}

It first renders the image:

{{< figure src="understanding_vulkan_graphics_pipeline/12_First_Render.png" >}}

Then based on the previous image it renders another image:
{{< figure src="understanding_vulkan_graphics_pipeline/13_Second_render.png" >}}

and produces the final rendered image.

## In Vulkan

Vulkan has a **VkRenderPass** Object that contains multiple *sub-passes* that render the image
based on the attachments provided by the **VkRenderPass**.

{{< figure src="understanding_vulkan_graphics_pipeline/14_vulkan_render.png" >}}

There can be various sub-passes, but for the tutorial only one is used:

{IMAGE_15}
{{< figure src="understanding_vulkan_graphics_pipeline/15_render_tutorial.png" >}}



# Creation
With all the stages of the pipeline describe, we can create a graphics pipeline and store
it in a **VkPipeline**.

To create a graphics pipeline, a **VkGraphicsPipelineCreateInfo** is filled with the
stages of the pipeline. 

Code from the tutorial:
```
VkPipeline graphicsPipeline;
VkGraphicsPipelineCreateInfo pipelineInfo;

vkCreateGraphicsPipeline(logicalDevice, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline)
```

{{< figure src="understanding_vulkan_graphics_pipeline/10_graphics_pipeline.png" >}}

