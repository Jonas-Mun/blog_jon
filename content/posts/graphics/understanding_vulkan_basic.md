---
title: "Understanding Vulkan Basic"
description:
toc: true
authors: ["Jonas"]
tags: ["vulkan", "c++", "graphics"]
categories: ["graphics"]
series: ["understanding-vulkan"]
date: 2020-12-01T09:30:13-06:00
draft: false
---

This article is a manner to help me understand the setup of Vulkan.
As I am still learning how the vulkan model works, there will
most likely be mistakes along the way.

# Basic setup

## Window
To keep in sync with the tutorial, what is needed is a window upon which
the rendering will occur. Without it, the GPU may have not target
to render to. 

This is achieved using GLFW's window system.

## Instance
The CPU must communicate with the GPU, the manner of doing so is by
using an API. (). 

{{< figure src="understanding_vulkan_basic/1_cpu_api.png" >}}

Here is where Vulkan comes in to play. 
A *Vulkan Instance* is created to allow our *application* to  utilize the Vulkan library.

{{< figure src="understanding_vulkan_basic/2_cpu_vulkan_api.png" >}}

The **VkInstance** is created by specifying the following:
* VkApplicationInfo - information about the current application
* VkInstanceCreateInfo - information about the current instance

Here extensions (windows system) are specified as well as the validation layers.

## Physical Devices
We have the ability to communicate to the GPU via the vulkan instance.

However there is no GPU currently selected to use, so we must pick a *Physical Device* (GPU) that we will use.
Without this device, there is no way to use a gpu...

A **VkPhysicalDevice** must be chosen based on the desired features, properties and queue families it supports.

{{< figure src="understanding_vulkan_basic/3_cpu_vulkan_gpu_select.png" >}}

### Queue Families
CPUs send command buffers to the GPU via queues in order to do operations.

{{< figure src="understanding_vulkan_basic/3_5_Queues.png" >}}

Each queue supports only a certain number of operations, thus supporting certain types
of commands

{{< figure src="understanding_vulkan_basic/3_6_Queue_Types.png" >}}

Vulkan categorizes these queues with similar operations in families.

{{< figure src="understanding_vulkan_basic/3_7_Queue_Families.png" >}}

To begin using queues, we must check whether the physical device is able to support
queues of a certain family. (Queues with certain available operations).

{{< figure src="understanding_vulkan_basic/4_queue_family_select.png" >}}

If a gpu supports the desired queue family, we can decide to use it for our rendering purposes.


In Vulkan these properties of the physical device are found using **vkGetPhysicalDeviceQueueFamilyProperties**.

## Logical Device
Now that we have access to the physical device, we can use it to send commands?

No

An interface to the physical device must be created in order to communicate with
the physical device. 

{{< figure src="understanding_vulkan_basic/6_logical_device.png" >}}

This is considered to be the **Logical Device** and is called in
vulkan as a **VkDevice**.

The *Logical Device* is created using the *physical device* and certain descriptions on
what the logical device should contain.

### Queues
Here, queues are described using the **VkDeviceQueueCreateInfo**.
It allows to specify the number of queues to use in the family as well as the 
priority given for each queue

{{< figure src="understanding_vulkan_basic/7_logical_queue_device.png" >}}

### Features
The set of device features from the *physical device* must be chosen as well.
This is done using the **VkPhysicalDeviceFeatures** structure.

{{< figure src="understanding_vulkan_basic/8_logical_device_features.png" >}}

### Creation
Once the *queues* and *features* have been described, the logical device can now be created.


### Queue Handle
A queue handle can be acquired to send commands through.
This handle is known in Vulkan as **VkQueue**, and acquired as follows:

```
VkQueue graphicsQueue;
vkGetDeviceQueue(logicalDevice, queueFamily, queueIndex, &graphicsQueue);
```

Selects the queue from the logical device based on the family.

{{< figure src="understanding_vulkan_basic/9_logical_device_queue_handle.png" >}}
