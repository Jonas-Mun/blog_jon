---
title: "Understanding_vulkan_final_render"
description:
toc: true
authors: ["Jonas"]
tags: ["vulkan", "c++", "graphics"]
categories: ["graphics"]
series: []
date: 2020-12-21T08:13:43-06:00
draft: true
---


{IMAGE_0}

With everything setup, we can now begin to draw the frame.
This can be done by a function called *drawFrame* as shown
in the tutorial.

This function does the following:
* Acquire an image from the swap chain
* Execute the command buffer with that image as attachment in the framebuffer
* Return the image to the swap chain for presentation.

These steps are executed **Asynchronously**. 
This is a problem because some operations depend on the previous 
one finishing. Thus a method for *syncrhonizing* the operations
must be found.

This is achieved by using one of the following:
* Fences - syncrhonize between the application and GPU
* Semaphores - Syncrhonize operations within or across command queues

As we will want to synchronize among the draw commands and presentation,
**Semaphores** will be used.

# Semaphores
Semaphores are created using **VkSemaphore**

We will need two semaphores for the following:
* Signal that an image has been acquired and ready for rendering
* Signal that rendering has finished and presentation can happen

```
VkSemaphore imageAvailableSemaphore;
VkSemaphore renderFinishedSemaphore;
```

## Creating Semaphores
A **VkSemaphoreCreateInfo** is used to create a semaphore:

```
VkSemaphoreCreateInfo semaphoreInfo{};

vkCreateSemaphore(logicalDevice, &semaphoreInfo, VkSemaphore);
```

# Acquiring Image from Swap Chain
An index will be acquired specifying the swap chain image, if
it is available.

*From the tutorial*
```
uint32_t imageIndex
vkAcquireNextImageKHR(LogicalDevice, SwapChain, timeout_nano_secs, imageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);
```

# Submitting Command Buffer
The **VkSubmitInfo** configures the Queue submission syncrhonization

The following is specified:
* Wait Semaphores
* Pipeline Stage to wait
* Signal Semaphores - once command buffer has finished execution

Once the Queue has been syncrhonized, it can be submitted as:

```
vkQueueSubmit(Queue, 1, VkSubmitInfo, VK_NULL_HANDLE);
```

# Subpass Dependencies
In a subpass there are two *mini* subpasses that occur in the beginning and
in the end of the specific subpass.
Two dependencies are responsible for what happens in those *mini* subpasses.

The issue is that at the start of the render pass, the transition occurs at
the start of the pipelin, but at that time an image has no yet been acquired.

## Solution
Make the render pass wait for the a specific stage of the pipeline.

## Dependency
A subpass dependency is created using a **VkSubpassDependency**.

This depenency is passed to the **RenderPassInfo** in the fields:
* dependencyCount
* pDependencies

# Presentation

Now we can submit the result to the swap chain to be shown onto the screen.

To do so an **VkPresentInfoKHR** is created containing the following fields:
* waitSemaphoreCount
* pWaitSemaphores - Signal when the image can be rendered.
* swapchainCount
* pSwapChains
* pImageIndices

And the final function:

```
vkQueuePresentKHR(presentQueue, VKPresentInfoKHR);
```

Before cleaning up, you must wait until the device is idle, otherwise
a semaphore may be in used.

```
vkDeviceWaitIdle(device);
```

# Frames in Flight

At the moment, the CPU is sending operations faster than the GPU and execute them.
Making the queue slowly fill up and using the semaphores and command buffers for multiple frames.

{IMAGE_0}

A solution is to support multiple frames *in-flight*.

## Implementation
Set a maximum frames in flight, and have each frame contain their own
semaphore.

{IMAGE_1}

We must also keep track of the current pair of semaphores being used:

{IMAGE_2}

For every finished frame drawn, the current_frame must be advanced:

{IMAGE_3}

## CPU-GPU Syncrhonization

It could be that the GPU may be working on *frame i*, and the CPU is
also using *frame i* at the same time. 
The CPU must wait for the GPU to be finished with *frame i* before working on it again.

To syncrhonize the CPU with the GPU, a **VkFence** is used for every frame.
```
std::vector inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
```

In the *drawFrame*, the following functions are applied in the beginning:

```
vkWaitForFences(logicalDevice, 1, inFlightFences[currentFrame], TRUE, timeout);
vkResetFences(logicalDevice, 1, inFlightFences[currentFrame]);
```

## Fence not submitted

When creating a fence, fences are not set to an *unsignaled state* and
must be done upon creation.

```
vkFenceCreateInfo fenceInfo{};
fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
...
```

## Rendering to an in-flight image
It could be the the image that is passed next may still be used.
To solve this, a method of tracking each image in a swap chain being
in flight must be created.

```
std::vector<VkFence> imagesInFlight;
```

Now we can check whether a previous frame is using the image
that has been currently assigned.

{IMAGE_4}

# Conclusion

That is most of what it takes to syncrhonize between the CPU-GPU and the 
GPU-GPU.

This is not meant to be followed word by word, but aims to help
visualize what is going on. It is best to use the tutorial.
