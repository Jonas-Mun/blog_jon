---
title: "Understanding Vertex Buffer in Vulkan"
description:
toc: true
authors: ["Jonas"]
tags: ["vulkan", "c++", "graphics"]
categories: ["graphics"]
series: ["understanding-vulkan"]
date: 2020-11-28T04:06:35-06:00
draft: false
---

This is an article to help me understand the process it takes to utilize a
vertex buffer in the Vulkan API. Most information is taken from the
vulkan-tutorial.

# Vertex Data
The purpose of the vertex buffer is to be able to pass vertex data from the CPU onto the GPU.
Having a vertex buffer helps store the vertex data in the GPU for it to be used.

{{< figure src="understanding_vertex_buffer_vulkan/cpu_to_gpu.png" >}}

A single *Vertex* can contain various attributes, where each attribute is of 
a different size and format to others.

## Vertex Data Description
A vertex has various attributes describing certain information:

This can be of the following kinds of attributes:
* Position - (Necessary)
* Color
* UV coordinate

For the sake of this article, the Vertex Data we are going to use will have:

* Position: Vector2
* Color   : Vector3

Contained in a structure:

```
struct Vertex {
    glm::vec2 pos;
    glm::vec3 color;
};
```
It is this data structure that must be **described** to the *pipeline*
Without the description, the pipeline won't know when a *vertex attribute* begins and ends.

{{< figure src="understanding_vertex_buffer_vulkan/vertex_data.png" >}}

In vulkan, the descriptions are created with the following *descriptors*

* VkVertexInputBindingDescription - general description of the Vertex data and its use
* VkVertexInputAttributeDescription

### VkVertexInputAttributeDescription
For every attribute, a description must be created.
It contains:
* binding
* location
* format
* offset

This information helps the pipeline dicern where an attribute starts and ends.
Usually stored in an array: 
```
std::array<VkVertexInputAttributeDescription, 2> attributeDescriptions;

/* '2' specifies the number of attributes */

```

specified in the **VkPipelineVertexInputStateCreateInfo** for the following fields:

* vertexBindingDescriptionCount
* vertexAttributeDescriptionCount
* pVertexBindingDescriptions
* pVertexAttributeDescriptions

All these descriptions allow the pipeline to utilize the vertex buffer in a specified format given by the descriptions.

Without describing the Vertex data the pipeline would not be able to know what to expect from the data stored in the vertex buffer.

{{< figure src="understanding_vertex_buffer_vulkan/vertex_data_description.png" >}}

# Vertex Buffer
A buffer will be used to store the *Vertex Data*. Buffers can be used to store
other kinds of data, but for our purpose it will store *Vertex Data*.

Buffers are just regions of memory to store data that is to be read by the gpu.

{{< figure src="understanding_vertex_buffer_vulkan/vertex_buffer.png" >}}

{{< figure src="understanding_vertex_buffer_vulkan/vertex_buffer_with_data.png" >}}

(Abstract representation of the vertex buffer)

## Creating a Buffer in Vulkan
In Vulkan there are various steps to prepare a buffer.

### VkBuffer
The *VkBuffer* is created by supplying a **VkBufferCreateInfo**.
It contains the following fields:

* Size - amount of data to be stored
* Usage - How the data is going to be used
* Sharing Mode - Whether to be shared across queue families or not

From what I understand, the *VkBuffer* acts as an interface to the region of memory available.
This may become clear in the following steps.


### Memory Allocation
With the buffer created, what is next is to allocate the memory required.
As at the moment, the buffer has no memory allocated, yet it has
information about the desired amount of memory and how it should be used.

{{< figure src="understanding_vertex_buffer_vulkan/vertex_buffer_memory_requirements.png" >}}

#### Memory Requirements
Given the **VkBuffer**, we must query the memory requirements needed to 
satisfy the given buffer created.

This is done by creating a **VkMemoryRequirements** that stores the memory requirements
for the buffer. It contains three fields:
* size
* alignment
* memoryTypeBits

We then can combine the memory requirements for the buffer and the application to search for the appropriate memory provided by the *Physical device*.

This is done by using a **VkPhysicalMemoryProperties** and filling it up as follows:
```
VkPhysicalDevicememoryProperties memProperties;
vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);
```

{{< figure src="understanding_vertex_buffer_vulkan/vertex_buffer_gpu_memory.png" >}}

#### Allocating
Once the required memory is acquired from the *physical device*, based on the buffer and application requirements, we can then allocate the memory to be used.

A **VkMemoryAllocInfo** is created to specify what needs to be allocated
* allocationSize - provided by **VkMemoryRequirements**
* memoryTypeIndex - acquired by quering the *VkPhysicalMemoryProperties* with the **VkMemoryRequirements**

This structure is used to allocate memory in a **VkDeviceMemory**

{{< figure src="understanding_vertex_buffer_vulkan/vertex_buffer_gpu_memory_allocated.png" >}}

## Binding the Allocated Memory with Buffer
Right now there is a region of memory that can be used, but it is not bounded to
the vertex buffer **VkBuffer**. 

```
vkBindBufferMemory(logicalDevice, vertexBuffer, vertexBufferMemory, 0);
```

{{< figure src="understanding_vertex_buffer_vulkan/vertex_buffer_gpu_buffer_binded.png" >}}

Now the vertex buffer is given a region of memory that is *associated* to it.

## Filling the vertex buffer
The method of filling the vertex buffer will be taken from the tutorial,
specifically this code:
```
void* data
vkMapMemory(logicalDevice, vertexBufferMemory, 0, bufferInfo.size, 0, &data);
memcpy(data, vertices.data(), (size_t) bufferInfo.size);
vkUnmapMemory(logicalDevice, vertexBufferMemory);
```

**vkMapMemory(..)** allows to map the *buffer memory* to a memory accessible to the CPU.

{{< figure src="understanding_vertex_buffer_vulkan/vertex_buffer_gpu_buffer_binded_data.png" >}}

This allows us to fill the **data** with the vertex data and have vulkan map the
vertex data onto the *buffer memory*

{{< figure src="understanding_vertex_buffer_vulkan/vertex_buffer_gpu_buffer_binded_data_filling.png" >}}

{{< figure src="understanding_vertex_buffer_vulkan/vertex_buffer_gpu_buffer_binded_data_filling_physical.png" >}}

Once that is done, it then unmaps the memory via, *vkUnmapMemory(..)*.


This is what it takes to setup a vertex buffer in Vulkan.
(This is one method of filling the buffer)

# Bind the vertex buffer during Rendering
What is left is to bind the buffer with the pipeline via the **vkCmdBindVertexBuffers**.

The code sample looks something like this from the tutorial:
```
VkBuffer vertexBuffers[] = {vertexBuffer};
VkDeviceSize offsets[] = {0};
vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, vertexBuffers, offset);
```

Once binded, the number of vertices must be applied to the **vkCmdDraw(..)**.

```
vkCmdDraw(commandBuffers[i], static_cast<uint32_t>(vertices.size()), 1, 0, 0);
```

That is all for creating a vertex buffer.
