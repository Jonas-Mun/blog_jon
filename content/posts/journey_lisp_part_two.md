---
title: Journey Through Lisp - Part Two
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-12-07T17:38:03Z
lastmod: 2022-12-07T17:38:03Z
featuredVideo:
featuredImage:
draft: false
---

This time I spent chapter 5 through 6.5.
In it, I learned how to create a basic *text-based* game engine.

# Organization of Location
The task was first split into multiple sub-problems:
* Looking around
* Picking up items
* Performing actions
* Walking

## Associative Lists

To store descriptions of locations, we used an *associative list* that, given the first element of a list as the *key* would return the remainder of the list.

```lisp
(assoc 'garden *nodes*)
(GARDEN (YOU ARE IN A BEAUTIFUL GARDEN. THERE IS A WELL IN FRONT OF YOU.))
```

## Edge Template - Quasiquoting

We used a *template* to describe *edges* that change the current location of the player.

This was done using *quasiquoting*:

```lisp
(defun describe-path (edge)
    `(there is a ,(caddr edge) going ,(cadr edge) from here.))
```

This uses the *tilted quote* to specify that some data can be evaluated.
This data is marked by the comma *,*.
It *flips* the data to *code mode*.

There was alot more, but that would require to go over the chpater again.

# Custom REPL

This was mind blowing!

We first created our won custom *REPL* that did the following:

```lisp
(defun game-repl()
    (loop (print (eval (read)))))
```
Simple, right?
Well, this is where the term *REPL* comes from!
**R**ead, **E**val, **P**rint, **L**oop.

It *reads* data in the *internal representation* of Lisp and executes it.
It then prints it out to the user and repeats (hence the loop).

## Homoiconic
What allows for this basic REPL is Lisp's *homoiconism*.
That is, the same data structure to store data is the same data structure for commands.

What is emphasized is that the printing is done in *computer friendly* mode.
We have to create our own *print* and *read* if we want to have a custom *user-friendly repl*.

This can be seperated into:
* Convert read input into *internal representation*. In our case, we convert it into *lists*!
* use the power of *eval*
* conver the evaluated value into a *human-friendly* way.

Note, use of *eval* and *read* is dangerous as it can execute commands that may be dangerous.

# New functions worth remembering:

* member - returns the list if the given element is found:
```lisp
(member 'a '(a b c))
=> (A B C)
```
* read-from-string - converts the string into an internal representaiton of Lisp
```lisp
(read-from-string "hello")
=> HELLO
(read-from-string "(hello)")
=> (HELLO)

```
* concatenate - concatenate elements together
```lisp
(concatenate 'string "(" "hello ")")
=> "(hello)"
```
* char-upcase
* char-downcase
* coerce - converts the given data into the specified type
```lisp
(coerce "A B C" 'list)
=> (#\A #\Space #\B #\Space #\C)
```
* string-trim - removes the given characters:
```lisp
(string-trim "() " "(a b c"))
=> "a b c"
```

* fresh-line
* print-to-string/prin1-to-string - takes the data and returns it in string form
```lisp
(prin1-to-string '(a b c))
```

* quote - Proves the quote *'* to represent *data mode*.
```lisp
(list 'quote '(a b))
=> '(A B)
(quote '(a b c))
=> '(A B C)
```


