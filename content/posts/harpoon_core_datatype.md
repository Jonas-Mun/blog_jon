---
title: Harpoon Core Data-Type
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-20T13:14:46Z
lastmod: 2022-11-20T13:14:46Z
featuredVideo:
featuredImage:
draft: false
---

After going over the following files:
* utils.lua
* dev.lua
* init.lua

We were able to garner a data-type merging that provides the core functionalities of the plugin.
This article will attempt to give an overview of this data-type and see how it is extend to include the other features.

# HarpoonConfig

The main data used is *HarpoonConfig*.
In fact, this is only an instance of the data-type *config*.

We get a lot of information from *ensure_correct_config*.
We are able to garner that it has the following:

1. global_settings
    - mark_branch => The git branch currently in the project.
2. projects
        - [cwd]
        - mark
            - marks {}
        - term
            - cmd {}

```lua
print(vim.inspect(HarpoonConfig))
---
{
    global_settings ={
        enter_on_sendcmd = false,
        excluded_filetypes = { "harpoon" },
        mark_branch = false,
        save_on_change = true,
        save_on_toggle = false,
        tmux_autoclose_windows = false
    },
    projects = {
        [<cwd_0>] = {
            mark = {
                marks = { {
                    col = 0,
                    filename = "<filename_0>",
                    row = 15
                },  {
                    col = 0,
                    filename = "<filename_1>",
                    row = 3
                } }
            },
            term = {
                cmds = {}
            }
        },
        ....
        [<cwd_n>] = {
            mark = {
                marks = { {
                    col = 0,
                    filename = "<filename_0>",
                    row = 2
                },  {
                    col = 0,
                    filename = "<filename_1>",
                    row = 4
                } }
            },
            term = {
                cmds = {}
            }
        }
    }
}
```

From this we gather that:
* A project is represented by the directory
* marks are stored in an array. I don't know why it has *mark.marks*...

We have the following data-types:
* Project
* Term
* Mark
* GlobalSettings

Combined together we get:
* Config
    - GlobalSettings
    - Project0 - (cwd0)
        - [Mark]
        - Term
    - Project1 - (cwd1)
        - [Mark]
        - Term

In the current *config* data, projects are stored under *projects*.
I do not understand why have nested *marks*. I think only having *marks = { }* would suffice.

This is the basic data-type we are going to be dealing with.
For the creation of a project, we could have:

We would have the following functions for Project:


```lua
local function get_project(config, cwd)
    return config.projects[cwd]
end

local function add_project(config, cwd, proj_data) 
    config.projects[cwd] = proj_data
end

local function create_project_data(p_marks, p_term) 
    local tbl = { marks = p_marks, term = p_term)

    return tbl
end
```

We can then do the following:

```lua
add_project(conf, "/test/branch", create_project_data(p_marks, p_term))
```

Now we have to know how to operate with marks.

```lua

local function add_mark(project, mark)
    project.marks.push(mark)
end

local function get_mark(project, id)
    return project.marks[id]
end

local function create_mark(p_col, p_row, p_filename)
    local tbl = {}
    tbl[col] = p_col 
    tbl[row] = p_row
    tbl[filename] = p_filename

    return tbl
end
```

Something like that.

We could even add a verifier for each data-type, so *config* would be verified by using the verification function that corresponds to each data-type stored in *config*.

As for *term*, we will leave it for now.

