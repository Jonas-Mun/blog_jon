---
title: Learning Vim - Operator-Pending and Status Line
description:
toc: true
authors: []
tags: ["vim"]
categories: []
series: ["learning-vim"]
date: 2021-07-25T21:38:14-05:00
lastmod: 2021-07-25T21:38:14-05:00
featuredVideo:
featuredImage:
draft: false
---

This article goes over the recent chapters I read from **Learning Vimscirpt the Hard way**. By writing down what I have learnt, I hope to be able to understand the topic a lot more.

The Chapter can be found here: <a href="https://learnvimscriptthehardway.stevelosh.com/chapters/15.html">Operator-Pending Mappings</a>

# Operator-Pending Movements
This topic of VIM describe how commands in vim are seperated in two:
* Operator
* Movement

The Operator does a certain function, whereas the Movement moves the cursor in a position where it
executes the command.

An example is:
```
ce
```

*e* is the movement and *c* is the operator.

## Creating your own movement

To create your own movement is similar to mapping a new command.

```
omap     <name> <command>
onoremap <name> <command>
```

## Movement from anywhere
The basic operator-movement shown above allows you to create a movement that starts where your cursor is at.
What if you want to create a movement, regardless of where your cursor is at?

To achieve this, we would use the following idiom:
```
:<c-u>normal! <command><cr>
```

Here the *normal!* allows us to execute the following <command> as if we were in **Normal** mode. The <cr> executes the command.

The <c-u>, I am not sure exactly what it does. According to *help omap-info* it removes the range the VIM can insert.

# More Operator-Pending Mappings
This chapter introduces a more powerful way to map operator-pending mappings.

## Execute
The *execute* command takes a string and executes the string as a command.

```
:onoremap <name> :<c-u>execute "<command>"<cr>
```

For a better example:
```
:onoremap ih :<c-u>execute "normal! ?^==\\+$\r:nohlsearch\rkvg_"<cr>
```

Here the *execute* takes a string of command and executes it.

## Why Execute and not Normal!
Because *normal!* does not take into account special characters like <cr>.
Execute allows us to specify <cr> via \r.

# Playing Around.
When using execute, I noticed that any special character/command that requires an **\\** will require an extra one.

```
\={2,}		" regex for 2 or more ='s"

\\={2,}		" regex for 2 or more ='s if using in execute string"

```

Which is how I managed to create the following movement:

```
:onoremap ih :<c-u>execute "normal! ?^[-=]\\{2,}$\r:nohlsearch\rg_vk0"<cr>
```

* [-=] - regex for accepting strings with '-' or '='
* \\\\{2,} - containing 2 or more of the preceding pattern

If this command was not done using *execute* but through **?** then the regex pattern **^[-=]\\\\{2,}** would have been:

```
?^[-=]\{2,}
```

# Status Line
Afterwards I learned about how to edit the status line.

It uses:
```
:set statusline=<items>
```

Items are what is displayed along the status line. You can incrementally add new items:
```
:set statusline=<item0>
:set statusline+=<item1>
:set statusline+=<item2>
```

To view the items and options:
```
:help statusline
```

# Conclusion
Learning about Operator-Pending movements felt like a big step into learning more about how VIM can be used. I still don't know how I will use them, but maybe along the way I may find a use. So long as I am vigilant of the task I am doing and thinking how I can be more productive, will I be able to take advantage of VIM's potential.

