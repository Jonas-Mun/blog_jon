---
title: Harpoon - Marks
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-20T17:19:39Z
lastmod: 2022-11-20T17:19:39Z
featuredVideo:
featuredImage:
draft: false
---

# mark.lua

There are two things to take into account:

```lua
local M = {}
local callbacks = {}
```

## emit_changed()

Let us delve into this function

```lua
    if harpoon.get_global_settings().save_on_change then
        harpoon.save()
    end
```

So, immediatly we are working with the *global* data structure of *HarpoonConfig*, where we check the settings.
The *harpoon.save()* uses the *update_b4* and saves *HarpoonConfig* into the *json* file.

The next block checks callbacks:

```lua
    if not callbacks["changed"] then
        log.trace("_emit_changed(): no callbacks for 'changed', returning")
        return
    end
```

So if nothing has been set, then we stop here.
I suppose the *callbacks* would provide the functionality for applying the *changes*.

This does seem to be the case:
```lua
    for idx, cb in pairs(callbacks["changed"]) do
        log.trace(
            string.format(
                "_emit_changed(): Running callback #%d for 'changed'",
                idx
            )
        )
        cb()
    end
```

Here *cb* is the function to be called when the *emit_changed* is called.

### What does it do?

*emit_changed* would save *HarpoonConfig* if needed, and then apply the functions pertaining to the changes when the function was called.

One question remains:
1. Do these functions get removed?

## filter_empty_string(list)

So from the code I gather that it removes the empty strings, but I keep getting *{}*.

Does this function actually do what it is supposed to do?
I think the clue lies in how *table* works:

```lua
            table.insert(next, list[idx].filename)  -> next = {}
```
Nevermind, I just gave the wrong data.

It takes the following:
```lua
local t_tbl = filter_empty_string({"",{filename="hello/man.txt"},{filename="moin/dude.tt"}})
```

and outputs:

```lua
{ {filename="hello/man.txt"},{filename="moin/dude.tt"} }
```

Interesting, so somehow a string can be placed inside an array of tables!!!!
Lua! What is this!!??

### What does it do?

It takes a list of tables AND strings and removes the empty strings, but keeps the tables.
It even keeps strings that are not empty, so it outputs a list of strings and tables!!!!
What is this madness, Lua!!??

## get_first_empty_slot()

```lua
local function get_first_empty_slot()
    log.trace("_get_first_empty_slot()")
    for idx = 1, M.get_length() do
        local filename = M.get_marked_file_name(idx)
        if filename == "" then
            return idx
        end
    end

    return M.get_length() + 1
end
```

Well well well, We meet again *HarpoonConfig*. Did not catch that?
What does *M.get_length()* do? Let us *enhance*...

### M.get_length()

```lua
function M.get_length()
    log.trace("get_length()")
    return table.maxn(harpoon.get_mark_config().marks)
end
```

We have two questions:
1. what does *get_mark_config()* do?
2. What does *table.maxn* do?

For the first question, it gets the field *mark* from the current working directory project:
- get_mark_config()
    - mark_config_key() -> project.mark

    (mark_config_key gets the project root directory to access the desired project)

Now what about question (2)?

Let us play with an example:
```lua
local tbl = {cwd = "mine", dude = "list"}
print(table.maxn(tbl)) -> 0
```

So it does not list total entries.

Let us try:

```lua
local tbl_two = {"mine", "list"}
print(table.maxn(tbl_two)) -> 2
```

It outputs teh total elements OR the MAX  key as told in the docs.
Now we know that *marks* is a list containing *mark* data-type, so that means we want to get the max mark id. In this case it is the last one, thus *n*.

Let us go back to the following *for* loop:

```lua
    for idx = 1, M.get_length() do
        local filename = M.get_marked_file_name(idx)
        if filename == "" then
            return idx
        end
    end
```

So the for loop goes over all the marks in the specified project.
Notice how we have no idea what that project is. That projet is obtained through a ton of hoops using the global config *HarpoonConfig*.

So now we move to the next line:
```lua
        local filename = M.get_marked_file_name(idx)
```

Based on the *idx*, we know it is acquiring the *mark* stored in *project.mark.marks*.
Lets look into it...

### M.get_marked_file_name(idx)

```lua
function M.get_marked_file_name(idx)
    local mark = harpoon.get_mark_config().marks[idx]
    log.trace("get_marked_file_name():", mark and mark.filename)
    return mark and mark.filename
end
```

So we know that the *idx* is the id of a mark in the *current working project*.
Well well well, it seems we are calling *harpoon.get_mark_config()* a lot here.
The reason is that we need the *HarpoonConfig* that stores data of the projects.

So *mark* is indeed the the mark of the current working project.

Now we have one question:
1. What does *mark and mark.filename* do?

So we are returning the whole data of *mark* as well as the filename from it.
Let is do a test:
```lua
local function test_and()
    return 23 and 9
end

local nine = test_and()
print(nine) -> 9
```

So it stores the latest value??
To the docs!
Could not find any.
So lets assume it returns the right-most value. Maybe we might encounter how it is used later on.

Ok, so back to the code:

```lua
    for idx = 1, M.get_length() do
        local filename = M.get_marked_file_name(idx)
        if filename == "" then
            return idx
        end
    end

    return M.get_length() + 1
```

So in our case, get obtain the *filename* that pertains to the given *mark*.
If the mark has an emptu filename, then we return the index of the mark!
Just as the function says!

Otherwise, we just return the next index.
Here we are calculating two values twice.

### What does it do?
It gets the *marks* from the *current working project* and returns the index of the mark that has an empty filename or the next index where the next mark will be at.

## get_buf_name(id)

```lua
local function get_buf_name(id)
    log.trace("_get_buf_name():", id)
    -- Block (1)
    if id == nil then
        return utils.normalize_path(vim.api.nvim_buf_get_name(0))
    elseif type(id) == "string" then
        return utils.normalize_path(id)
    end

    -- Block (2)
    local idx = M.get_index_of(id)
    if M.valid_index(idx) then
        return M.get_marked_file_name(idx)
    end
    --
    -- not sure what to do here...
    --
    return ""
end
```

Let us focus on Block 1:

```lua
    if id == nil then
        return utils.normalize_path(vim.api.nvim_buf_get_name(0))
    elseif type(id) == "string" then
        return utils.normalize_path(id)
    end
```

we have one question:
1. What does *vim.api.nvim_buf_get_name(0)* do?

To the docs!
```
Gets the full file name for the buffer
```

If we run:
```
:lua print(vim.api.nvim_buf_get_name(0)) -> File-path
```
We get the full path of the file being edited in the buffer.
As we have seen in *utils*, it is extracting the relative section from the *current working directory*.

The *else* seems self explanatory. It is given an *id* of type *string* and does similar to before.

Now we go to Block 2:

```lua
    -- Block (2)
    local idx = M.get_index_of(id)
    if M.valid_index(idx) then
        return M.get_marked_file_name(idx)
    end
    --
    -- not sure what to do here...
    --
    return ""
```

So if it is *not* nil nor it is a *string* then we move on...
We reach another question:
* What does *get_index_of* do?
