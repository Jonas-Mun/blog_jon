---
title: Learning Assembly - Main Function
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-06T10:32:58Z
lastmod: 2022-11-06T10:32:58Z
featuredVideo:
featuredImage:
draft: false
---

This continues the previous article of learning about assembly language.

# Read/Write System calls

When a program is launched, Linux opens three files:
* *standard in* -> 		STDIN_FILENO
* *standard out* ->		STDOUT_FILENO
* *standard error* ->		STDRR_FILENO



These are assigned a number, called a **File descriptor**.
The **File descriptor** is used to interact with each file.


More on write/read:
```
man 2 write
man 2 read
```

We can call write in C:

```c
int main(void)
{
	write(STDOUT_FILENO, "Hello, World!\n", 14);

	return 0;
}
```

When passing values to a function, registers are used to store values.
Conventions are followed to avoid mistakes:

* https://github.com/hjl-tools/x86-psABI/wiki/x86-64-psABI-1.0.pdf
* https://gitlab.com/x86-psABIs/x86-64-ABI

## Position-Independent Code

When wanting to reference data in our assembly source file, it is ideal to do so by ensuring that the functionality of code is independent of its position.

There is a function: *lea* (Load effective address) that calculates the difference between the current instruction and the given position:

```
lea	rsi, .LC0[rip]
```

This function calculates the memory address difference from the next instruction *rip* to the position of *LC0*, and stores it in *rsi*.

When calling write, the assembly instruction would be:

```asm
call write@PLT
```

The *PLT* says where the function *write* can be located. In this case, it can be located in the **Procedure linkage table** with a *global offset table (GOT)*.

Calling this function involves the follwing steps:
1. OS loads the function into memory, using the *dynamic loader*
2. Puts the address of the function in the *global offset table*.
3. Updates the **Procedure Linkage Table** to point to the address.

If the function *write* is called again, then the **Procedure Linkage Table** uses the value given by the *global offset table*.

This procedure is mostly used when the location of functions are not known/explicitly given.

# The Call Stack

A storage unit to store *local variables* in memory for functions.

Two main operators:
* push *data*
* pop *location*

If we run out of Stack space => **Stack Overflow**
If we run out of items to pop => **Stack Underflow**

## Implementation of Stack

* A *continguous area of main memory* is assigned to the stack.
* They can grow in either direction:
	- **Ascending stack** => grows into higher addresses.
	- **Descending Stack** => grows into lower addresses.
* THe Stack pointer can either:
	- Point to the top item of the stack =>  FULL
	- Point to the empty location where the next item will be placed. => EMPTY

The *x84_64* instructions follow a **Full-Descending Stack**.

# Special Assembler Directive

```asm
.equ symbol, expression
```

The above instruction replaces the occurence of *symbol* with the value of *expression*.
*expression* must evaluate to an integer.

For example:

```asm
.equ	STDOUT, 1
```
The above instruction will replace STDOUT with the value *1*.

Here is another example, that calculates the length of a string:

```asm
message:
	.string "Hello, World\n"
	.equ	msgLength, .-message-1
```

The *.* character is an expression that gives the current memory location of the instruction.
In the above instruction, it is the end of the *C-Style* string.
It substracts the position with the beginning of the string, and subtracts by 1 to get rid of the NULL character.

# Going Over Hello World in Assembly

# Variables in Stack

We will create a section in the stack that would store all the local variables needed.
This avoids having to remember the position of variables in the stack frame.

We shall use the example of the book:

```asm

	.file	"echoChar.c"
	.intel_syntax noprefix
	.text
	.section	.rodata
.LC0:
	.string	"Enter one character: "
.LC1:
	.string	"You entered: "
	.text
	.globl	main
	.type	main, @function
main:
	push	rbp
	mov	rbp, rsp
	sub	rsp, 16			# Creates the space for local variables. Stack pointer must be 16-byte aligned.
	mov	rax, QWORD PTR fs:40	# Canary value to ensure no Stack corruption: From memory fs:40
	mov	QWORD PTR -8[rbp], rax
	xor	eax, eax
	mov	edx, 21
	lea	rax, .LC0[rip]
	mov	rsi, rax
	mov	edi, 1
	call	write@PLT
	lea	rax, -9[rbp]		# The letter &aLetter
	mov	edx, 1
	mov	rsi, rax
	mov	edi, 0
	call	read@PLT
	mov	edx, 13
	lea	rax, .LC1[rip]
	mov	rsi, rax
	mov	edi, 1
	call	write@PLT
	lea	rax, -9[rbp]
	mov	edx, 1
	mov	rsi, rax
	mov	edi, 1
	call	write@PLT
	mov	eax, 0
	mov	rdx, QWORD PTR -8[rbp]
	sub	rdx, QWORD PTR fs:40
	je	.L3
	call	__stack_chk_fail@PLT
.L3:
	leave				# Same for: mov rsp, rbp; pop rbp
	ret
	.size	main, .-main
	.ident	"GCC: (GNU) 11.2.0"
	.section	.note.GNU-stack,"",@progbits

```

# Lessons Learned:

* [x] Use of the Call Stack
* [x] Passing arguments to functions in stack and registers
* [x] Manipulation of call stack
* [x] Canary to ensure stack has not been corrupted.
