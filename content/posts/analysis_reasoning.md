---
title: Analysis as a Method of Reasoning.
description:
toc: true
authors: []
tags: [mathematics,computation,history]
categories: []
series: []
date: 2022-10-30T20:02:51Z
lastmod: 2022-10-30T20:02:51Z
featuredVideo:
featuredImage:
draft: false
---

Hello.[see ^1, pp.30]

[^1]: Mathematical thought from ancient to modern times. Oxford University Press, New York, 1972.
[^2]: Richard Dedekind. Essays on the theory of numbers. Dover books on advanced mathematics. Dover, New York, 1963.
[^3]: Gottlob Frege. Die Grundlagen der Arithmetik : eine logisch mathematische Un tersuchung uber den Begriff der Zahl =The foundations of arithmetic ; a logico mathematical enquiry into the concept of number. Blackwell, Oxford, 1950.
[^4]: Alonzo Church. An unsolvable problem of elementary number theory. American Journal of Mathematics, 58:345-363, 1936.
[^5]: Stephen Cole Kleene. Introduction to metamathematics. Bibliotheca mathematica ; Volume 1. North-Holland Publishing Company, Amsterdam, reprinted with corrections. edition, 1971.
