---
title: Creating a tmux script - Switching and Killing Sessions
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-19T14:53:08Z
lastmod: 2022-11-19T14:53:08Z
featuredVideo:
featuredImage:
draft: true
---

Say you are using a tmux session, and say you are finished with it.
You kill it, but you are sent back to your main terminal, despite already having other tmux sessions running.
How about we stay in the next available tmux session?

Scouring through the internet I found the following command:

