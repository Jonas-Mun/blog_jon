---
title: Multiple Github Accounts
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-02T19:03:25Z
lastmod: 2022-11-02T19:03:25Z
featuredVideo:
featuredImage:
draft: false
---

This article will detail the steps to setup multiple github accounts.
Taken from: https://www.freecodecamp.org/news/manage-multiple-github-accounts-the-ssh-way-2dadc30ccaca/

# Creating SSH Keys
Create a directory called *.ssh* and go into it.

In *.ssh* run:

```bash
$ ssh-keygen -t ed25519 -C "your_email@example.com" -f "<name>"
```

the *-f "name"* will give the file its unique name that you gave it.

# Registering SSH Keys in Github

Go to your account and add the Public SSH:

```
Settings -> SSH and GPG Keys
```

Select on the **New SSH Key**, and copy the key found in:

```
<name>.pub
```

Then create a *config* file in **.ssh** with the following:

```
# Personal account, - the default config
Host github.com
   HostName github.com
   User git
   IdentityFile ~/.ssh/<name>
```

This should be the basic set-up for a single account.

# Issues

If you get the following error:
```
Bad owner or permissions on ~/.ssh/config
```

You can run the following:
```bash
chmod go-w ~/.ssh/config
```
