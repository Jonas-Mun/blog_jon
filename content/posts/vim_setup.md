---
title: NeoVim Setup
description:
toc: true
authors: []
tags: []
categories: []
series: []
date: 2022-11-02T15:46:23Z
lastmod: 2022-11-02T15:46:23Z
featuredVideo:
featuredImage:
draft: false
---

This article will go over my basic neovim Setup.

* [ ] Neovim Installation
* [ ] Neovim Plugin Manager
* [ ] Neovim Folder Setup
* [ ] Neovim LSP-Config
* [ ] Neovim Auto-Complete

# Neovim Installation

To install neovim, simply run the following command:

```bash
sudo pacman -S neovim
```

Create a config file called **init.vim** for neovim under *~/.config*, by first creating the folder **nvim**:

```bash
mkdir ~/.config/nvim
touch ~/.config/nvim/init.vim
```

This completes the basic installation for neovim.

* [x] Neovim Installation

## From Source

If your package manager does not have the latest version, you will have to download it form the source.
https://github.com/neovim/neovim/wiki/Building-Neovim

1. Download the Prerequisistes: https://github.com/neovim/neovim/wiki/Building-Neovim#build-prerequisites
2. Once downloaded from git, checkout to stable.
3. The rest of the instructions should be self-explanatory

# Plugin Manager

I decided to use **Vim-Plug** as the plugin manager: https://github.com/junegunn/vim-plug
I used to use Pathogen, but it required me to remember all my plugins and made it tedious to update them. I wanted to reduced the things I needed to remember so I decided to use **Vim-Plug**.

Before instlaling *vim-plug*, you will have to create the folder **autoload** under **nvim**:

```bash
mkdir ~/.config/nvim/autoload
```

Then create/download the file **plug.vim** with the contents of: https://github.com/junegunn/vim-plug/blob/master/plug.vim

At the end you should have the following file:

```bash
~/.config/nvim/autoload/plug.vim
```

In your *init.vim* file, you will have to add the following in the beginning of the file:

```
call plug#begin()
... <Your plugins>
call plug#end()
```

All documentation is found in the github repo: https://github.com/junegunn/vim-plug.

This completes the basic setup for the Plugin manager.

* [x] Neovim Plugin Manager

# Folder Setup

This section is optional, but can be useful for organizing your vim config files.
I followed the setup given by **The Primeagen**.

```
nvim
    after
        plugin
    lua
    init.vim
    keymap.vim
```

I plan to use some of *vimscript* for the basic configurations such as mapping keys.
For other functionalities, such as plugin customization, I plan to use Lua.

## Lua Folder

Under the lua folder, create a folder called **<name>**.
This will be responsible for containing all your custom configurations in lua.

```
nvim
    after
        plugin
    lua
        <name>
            init.lua
    init.vim
    keymap.vim
```

```
nvim
    after
        plugin
    lua
        jonas
            init.lua
    init.vim
    keymap.vim
```

When neovim runs, it looks for files in *after*, so it won't call your custom **init.lua** file.
To ensure neovim checks your file, you will have to tell neovim to **require** a specific file.

This can be done in the following manner:

```vimscript
lua << EOF
require'jonas'
EOF
```

### Adding Custom Lua Files

You can have all your configurations under **<name>.lua**, but this can make it difficult to know where specific configurations are.
One way is to split the functionalities into different files in the *lua* folder.

For example, you create a file called **set.lua**.
Neovim will not run this file, because you did not tell it to **require** it.

```
nvim
    after
        plugin
    lua
        <name>
	        init.lua
            set.lua
    init.vim
    keymap.vim
```

In **init.lua** you write the following code:

```lua
require('<name>.set')
```


# LSP Config

Neovim has an in-built LSP server, but to avoid having to write scripts for each language server, there is a plugin that faciliates this: **nvim-lspconfig**

In your init.vim, inside the vimplug *begin* and *end*, write:

```vimscript
Plug 'neovim/nvim-lspconfig'
```

Run **PlugInstall** to install the plugins.

We will be testing the LSP with C++, so we have to install the *clang* compiler.

```bash
sudo pacman -S clangd
```

We now have to tell neovim to require the LSP server (in init.lua):

```
require('lspconfig').clangd.setup{}
```

This should now work when opening C++ and C file types.

**LspInfo** gives info of the buffer. Showing that it is working.

## On Attach

We can configure what is done when the LSP attaches to the buffer.
This is done inside the **setup{}** section:

```lua
require('lspconfig').clangd.setup{
    on_attach = function()
        print("Testing on Attach")
    end,
}
```

 Here is where you can add vim maps:

```lua

require('lspconfig').clangd.setup{
    on_attach = function()
        vim.keymap.set("n", "K", vim.lsp.buf.hover, {buffer=0})
        vim.keymap.set("n", "gd", vim.lsp.buf.definition, {buffer=0})
        vim.keymap.set("n", "gt", vim.lsp.buf.type_definition, {buffer=0})
        vim.keymap.set("n", "gi", vim.lsp.buf.implementation, {buffer=0})
        vim.keymap.set("n", "<leader>dj", vim.diagnostic.goto_next, {buffer=0})
        vim.keymap.set("n", "<leader>dk", vim.diagnostic.goto_prev, {buffer=0})
        vim.keymap.set("n", "<leader> r", vim.lsp.buf.rename, {buffer=0})
    end,
}

```

* [x] Neovim LSP-Config

# Neovim Auto-Complete

We will need to install the following plugins:

```vimscript
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
```

The autocomplete requires a snippet tool.
For this article, we will be using **LuaSnip**:

```vimscript
Plug 'L3MON4D3/LuaSnip'
Plug 'saadparwaiz1/cmp_luasnip'
```

To begin setting up autocomplete, add the following in the init.lua file:

```vimscript
  -- Set up lspconfig.
  local capabilities = require('cmp_nvim_lsp').default_capabilities()

-- Integrate autocomplete with language server.
require('lspconfig').clangd.setup{
    capabilities=capabilities,
    on_attach = function()
        vim.keymap.set("n", "K", vim.lsp.buf.hover, {buffer=0})
        vim.keymap.set("n", "gd", vim.lsp.buf.definition, {buffer=0})
        vim.keymap.set("n", "gt", vim.lsp.buf.type_definition, {buffer=0})
        vim.keymap.set("n", "gi", vim.lsp.buf.implementation, {buffer=0})
        vim.keymap.set("n", "<leader>dj", vim.diagnostic.goto_next, {buffer=0})
        vim.keymap.set("n", "<leader>dk", vim.diagnostic.goto_prev, {buffer=0})
        vim.keymap.set("n", "<leader> r", vim.lsp.buf.rename, {buffer=0})
    end,
}

-- Set up nvim-cmp.
  local cmp = require'cmp'

  cmp.setup({
    snippet = {
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
        --vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
        require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        -- require('snippy').expand_snippet(args.body) -- For `snippy` users.
        -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
      end,
    },
    window = {
      -- completion = cmp.config.window.bordered(),
      -- documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    }),
    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
      --{ name = 'vsnip' }, -- For vsnip users.
       { name = 'luasnip' }, -- For luasnip users.
      -- { name = 'ultisnips' }, -- For ultisnips users.
      -- { name = 'snippy' }, -- For snippy users.
    }, {
      { name = 'buffer' },
    })
  })
```

This is the basic script to have autocomplete running.
* [x] Neovim Auto-Complete

The LSP-config is based on the following video: https://www.youtube.com/watch?v=puWgHa7k3SY&t=600s
