---
author:
- Jonas Munoz Novelo
title: Grid Formation Behaviour v1.0
draft: true
---

::: titlepage
::: center
**Grid Formation Behaviour v1.0**

Mathematics and Algorithms

**Jonas**

{{< figure src="images/logo.png" >}}
![image](images/logo.png){width="50mm"}
:::
:::

# Licenses

## Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) 

This work is licensed under the Creative Commons
Attribution-NonCommercial 4.0 International License. To view a copy of
this license, visit http://creativecommons.org/licenses/by-nc/4.0/ or
send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042,
USA.

## The MIT License

Copyright 2021 Jonas

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
\"Software\"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Introduction

This document goes step by step into implementing a grid formation
behvaiour by clicking and dragging from point A to point B. It does so
by first implementing it along the X axis only, and then deriving ways
to implement the grid formation on any angle using the Unit circle.
Focusing mostly on the theory and mathematics of the grid formation
behaviour. The goal is to show how mathematics can be utilised to create
a system that is robust and easy to add/make changes without breaking
behaviour. The outcome of this paper lays the foundation for future
iterations and additions. Understanding the mathematics will allow one
to implement the behaviour in any platform that supports cartesian
coordinates.

The $\mathbb{R}^2$ symbol in this document indicates a vector of two
dimensions.

# Dragging

# Imaginary Line

# Applying Unit Circle

# Dynamic Positions

# Fixed Number of Units

# Depth in Formation

# Refactoring Procedures

# Main Procedure

# Conclusion

# General Procedure for Formation Positions

# Coordinate System Accomodation

# Pascal's Positions

# Final Equations and Procedures

# Code

[Godot Source
Code](https://gitlab.com/Jonas-Mun/godot-grid_formation_2d)
