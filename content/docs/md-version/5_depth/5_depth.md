Two scenarios will help derive the pattern to implement the depth
formation behaviour.

-   `Front Units - F`: Red

-   `Depth Units`: Blue

::: center
:::

::: center
:::

In the first scenario, the distance between the front unit and the depth
unit is equal to the *distanceBetweenEachUnit* along the positive
X-Axis.

::: center
:::

Here $\sin(90) = 1$ and $\cos(90) = 0$, and
$distanceBetweenEachUnit = 1$. As the depth unit must travel along the
positive X-Axis by a distance equal to the *distanceBetweenEachUnit*
from its front unit, the following equation is derived: where d =
*distanceBetweenEachUnit*

$$\begin{aligned}
depthUnitPosition &= Fn + ( (\sin(90),\cos(90) ) * d)\\
 &= Fn + ((1, 0) * d)\\
&= Fn + (d, 0)\end{aligned}$$

In the second scenario, the distance between the front unit and the
depth unit is equal to the *distanceBetweenEachUnit* along the negative
Y-Axis.

::: center
:::

Here $\sin(0) = 0$ and $\cos(0) = 1$, and $distanceBetweenEachUnit = 1$.
As the depth unit must travel along the negative Y-Axis ba a distance
equal to the *distanceBetweenEachUnit* from its front unit, the
following equation is derived:

$$\begin{aligned}
depthUnitPosition &= Fn + ( (\sin(0),-\cos(0) ) * d)\\
 &= Fn + ((0, -1) * d)\\
&= Fn + (0, -d)\end{aligned}$$

The general equation for the position of a *depth unit*:

$$depthUnitPosition = F + ( (\sin(\theta), -\cos(\theta) ) * d )$$
where: `F` is the unit's position that is in front of the *depth unit*.
`d` is the *distanceBetweenEachUnit*. **NOTICE** how the front units are
on a depth of layer 0, and the units behind depth0 are on a depth of
layer 1. This means the units on depth *n* will have units infront at
depth layer *n - 1*.

This behaviour is visualized in the following figures.

::: center
:::

::: center
:::

::: center
:::

::: center
:::

::: center
:::

::: center
:::

## Integrating Depth

*Check Appendix B to ensure that the coordinate system that is being
used is the correct one.*

The algorithm for calculating the depth positions requires the following
variables:

-   `Front Unit positions`

-   `distanceBetweenEachUnit`

-   `unitLimit`

-   `Number of front units`

-   `angle`

Integrate it with the `CalculatePositionsOnRadius`:
