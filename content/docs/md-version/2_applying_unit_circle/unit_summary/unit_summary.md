The unit circle consists of a circle with *radius* of `1`.

::: center
:::

The coordinates from the center to the end of the radius can ba
calculated using Sin and Cos:

::: center
:::

substituting the values, the coordinates are as follow:

::: center
:::
