Imagine the center of the circle is the Initial coordinate I.

::: center
:::

And E is located on the circumference at the end of the radius. This
creates the imaginary line as before, with angles being aligned on the
X-Axis.

::: center
:::

For the sake of the example $I = (0,0)$ and $E = (0.86, 0.5)$, as I and
E will be known upon finishising the dragging mechanism.

::: center
:::

What is left is to calculate the length of the radius

### Length of Radius/Line

Using Pythagora's theorem we can calculate the length of the given
radius, **r**:

::: center
:::

$$\begin{aligned}
r &= \sqrt{0.86^2 + 0.5 ^2}\\
r &= 1\end{aligned}$$

A radius of length 1 allows for the following number of units to lie on
the radius with a unit size of 1 with no gaps.

$$\begin{aligned}
totalUnits &= \frac{lengthOfRadius}{sizeOfUnit+gap} + 1\\
&= \frac{1}{1+0} +1\\
&= \frac{1}{1} + 1\\
&= 1 + 1\\
totalUnits&= 2\end{aligned}$$

### Positions on Radius/Line

The position of the first unit will be at I.

::: center
:::

To calculate the position of the *next* unit requires finding the angle
between the radius and the X-axis.

#### Acquiring Angle

Using an equation derived from SOHCATOAH. $$\begin{aligned}
\tan(\theta) &= \frac{opposite}{adjacent}\\
&= \frac{0.5}{0.86}\\
\tan(\theta) &= 0.58\\
\theta &= \tan^{-1}(0.58)\\
\theta &= 30\end{aligned}$$

Applying the Sin/Cos will give the position of the unit on the
radius/line at an angle of *30*, with unit size = 1 and no gap.

$$\begin{aligned}
0.86 &= \cos(30);\\
0.5 &= \sin(30);\end{aligned}$$

::: center
:::

**REMEMBER** The distance between each unit starts from the center of
one unit to the center of the next unit. **Notice** how the distance
between each unit is the same as the radius, *r*.

### Adding more units

Increasing the radius to 2 will allow to place more units on the line.

::: center
:::

Based on the equation, there should be three units.

Once can imagine as having two circles of same center I and same degrees
but of varying radii (In this case, 1 and 2).

::: center
:::

Allowing for three units of size 1 with no gaps to lie on the radius.

::: center
:::

Based on the imagine above, one can derive the following equation:

$$positionOnRadius = I + ( (sizeOfUnit + gap) * (cos(\theta), sin(\theta)) * i)$$
where:

sizeOfUnit + gap

:   is the the distance between units

(cos($\theta$), sin($\theta$))

:   aligns the radius by a degree of $\theta$

i

:   is the current unit *Or* the *nth* circle, starting from *0*.

Using the above example, the following coordinates on the line are:

`First Unit` $$\begin{aligned}
positionOnRadius &= I + ( (1 + 0) * (cos(30), sin(30)) * 0)\\
&= I + (1 * (0.86, 0.5) * 0)\\
&= I\end{aligned}$$

`Second Unit` $$\begin{aligned}
positionOnRadius &= I + ( (1 + 0) * (cos(30), sin(30)) * 1)\\
&= I + (1 * (0.86, 0.5) * 1)\\
&= I + ((0.86, 0.5) * 1 )\\
&= I + (0.86, 0.5)\end{aligned}$$

`Third Unit` $$\begin{aligned}
positionOnRadius &= I + ( (1 + 0) * (cos(30), sin(30)) * 2)\\
&= I + (1 * (0.86, 0.5) * 2)\\
&= I + ((0.86, 0.5) * 2 )\\
&= I + (1.72,1.0)\end{aligned}$$

### Deriving the function

Caulculating the position on the radius requires the following
variables: $$CalculatePositionOnRadius(I, E, sizeOfUnit, gap)$$ where:

::: description
I and E give the *Radius* of the circle.

sizeOfUnit and gap give the distance between each unit.
:::

**NOTE** subtracting $E - I$ give the *x* and *y* coordinates or
*adjacent* and *opposite* values respectively. Allowing one to calculate
the angle of the radius based on the X-axis/right vector.

### Algorithm
