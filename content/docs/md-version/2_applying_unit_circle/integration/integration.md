The following algorithm **$Dragging Mechanism$** is integrated with
**$PositionsOnRadius$**:

Everything needed to create positions along a radius has been created.
**NOTE** Make sure the *angle* is in degrees.
