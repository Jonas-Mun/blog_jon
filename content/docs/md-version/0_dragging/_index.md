---
title: Dragging
description: Nested chapter of example doc
toc: true
authors:
tags:
categories:
series:
date: '2020-10-16'
lastmod: '2020-10-16'
draft: false
---

Creating a function that detects when the cursor is being dragged across
the screen.
