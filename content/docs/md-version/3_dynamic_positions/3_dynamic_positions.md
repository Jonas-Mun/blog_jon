Currently the positions are calculated after dragging ends. What if the
positions can be calculated while dragging?

To achieve this behaviour, the calculation of positions must *during*
dragging.

## Re-arranging Calls

Certain changes must be done in **Dragging Mechanism**. A new Procedure
`Grid Formation` will be in charge of initializing the variables.

Have *Dragging Mechanism* only return whether dragging is occuring or
not.

### Dragging Mechanism

### Grid Formation

As *dragging* only returns whether we are dragging, we will have to
update the **I** and **E** when we begin dragging and we end dragging.
This can be achieved by introducing a temporary variable that will store
the new dragging state, and compare it to the old dragging state.

A new coordinate $C$ will store the current coordinate that the mouse is
located at
