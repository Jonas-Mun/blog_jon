---
title: Length of Line
description: Nested chapter of example doc
toc: true
authors:
tags:
categories:
series:
date: '2020-10-16'
lastmod: '2020-10-16'
draft: false
---
The length of the imaginary line will dictate the number of units that
are able to be placed on the line.

Calculating the length of the line can be done in two ways:

::: center
:::

::: center
:::

Using Pythagora's theorem also allows one to calculate the length.
$
d^2 &= x^2 + y^2\\
d &= \sqrt{x^2 + y^2}$

The approach in *Figure 1* only considers a line with no `y` value,
making it very limited for our purposes later on. Because of this, the
Pythagora's theorem approach will be used.
