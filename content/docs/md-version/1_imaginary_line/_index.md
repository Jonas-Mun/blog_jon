---
title: Imaginary Line
description: Nested chapter of example doc
toc: true
authors:
tags:
categories:
series:
date: '2020-10-16'
lastmod: '2020-10-16'
draft: false
---
As units will be placed along a line, we can create an imaginary line
from the moment the dragging begins until the moment the dragging stops.

Modifying the Dragging Mechanism from before

### Example

The mouse begins to drag at point I

::: center
:::

The mouse is being dragged and stops on point E, creating an imaginary
line from point I to point E.

::: center
:::

## Placing Units on the Line

## Integrating
