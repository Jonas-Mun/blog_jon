### Number of Units on the Line

Calculating the number of units that can fit on a line requires knowing
the *size* of the units that are going to be in formation. For the sake
of this document,: $unitSize = 1$.

Given the size of the unit, the total number of units that lie on a line
is as follows: $$totalUnits = \frac{lengthOfLine}{unitSize}$$

This assumes that for every radius of integer value \>= 1, a unit is
placed, but this behaviour places the first unit on the initial
coordinate. Resulting in *1* unit extra.

$$totalUnits = \frac{lengthOfLine}{unitSize} + 1$$

### Positions of units on the line

### Manipulating the Distance between each Unit

### Increasing/Decreasing Distance between Units

To calculate the positions on the line from point **I**, we applyi the
equation $$positionOnLine = I + (distBtxt*i)$$ for every unit.

The first position on the line will be located on index *0* in
`positions` list.
