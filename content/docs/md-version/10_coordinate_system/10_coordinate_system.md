The coordinate system being used is a **left-handed** coordinate system.

::: center
:::

::: center
:::

::: center
:::

::: center
:::

From these diagrams one can see that the depth positions is calculated
with the following equation:

$$depthPosition = (\sin(\theta), -\cos(\theta))$$

The current implementation will work if a **left-handed** coordinate
system is being used, with the *up vector* being $(0,1)$.

However, in certain game engines a different coordinate system is used.
This section will go over how to accomodate for the different coordinate
systems.

## Negative Y

In this situation, the *up vector* is now $(0,-1)$ so the following
changes must be made.

::: center
:::

::: center
:::

The depth position in this coordinate system is based on the following
equation:

$$depthPosition = (-\sin(\theta), \cos(\theta))$$

## Right-Hand Coordinate System

::: center
:::

::: center
:::

The depth position equation is as follows:

$$depthPosition = (-\sin(\theta), \cos(\theta))$$

### Negative Y

The Right-Handed system with Y pointing downwards.

::: center
:::

::: center
:::

The depth positin is calculated as follows:

$$depthPosition = (\sin(\theta), -\cos(\theta)  )$$

These diagrams will help understand why some of the depth units may not
be behaving as expected
