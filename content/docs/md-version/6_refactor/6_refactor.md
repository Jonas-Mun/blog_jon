The *PositionsOnRadius* is doing the following:

1.  Initializing unit data

2.  Gathering front row positions

3.  Gathering depth positions

A procedure called *GridFormationPositions* will in charge of
initializing the data, and deciding when to gather front positions and
depth positions. Allowing *PositionsOnRadius* to solely focus on
calculating the front unit positions.

## Grid Formation Positions

## Front Positions (Version 4)

## Depth Positions (Version 2) 
