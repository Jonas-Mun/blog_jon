---
title: Grid Formation, Mathematics and Algorithms
description: This is an example doc layout of Eureka theme
toc: true
authors:
tags:
categories:
series:
date: '2020-10-16'
lastmod: '2020-10-16'
draft: true
---

# Introduction

This document goes step by step into implementing a grid formation
behvaiour by clicking and dragging from point A to point B. It does so
by first implementing it along the X axis only, and then deriving ways
to implement the grid formation on any angle using the Unit circle.
Focusing mostly on the theory and mathematics of the grid formation
behaviour. The goal is to show how mathematics can be utilised to create
a system that is robust and easy to add/make changes without breaking
behaviour. The outcome of this paper lays the foundation for future
iterations and additions. Understanding the mathematics will allow one
to implement the behaviour in any platform that supports cartesian
coordinates.

The $\mathbb{R}^2$ symbol in this document indicates a vector of two
dimensions.
This is the root of example doc.

# Code
[Godot Source
Code](https://gitlab.com/Jonas-Mun/godot-grid_formation_2d)
