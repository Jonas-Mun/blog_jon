import regex as re
import sys

DEBUG = False

txt = "\\State $Hello $"
x = re.search("\\State", txt)

extracted = txt[x.span()[1]:]

filename = sys.argv[1]

header = "\\documentclass[crop=true]{standalone}\n\\usepackage[ruled, lined, linesnumbered, commentsnumbered, longend]{algorithm2e}\n"
start_algo = "\\begin{document}\n\n\\begin{algorithm}[H]"
algo_setup = "\\caption{}\n\\SetKwFunction{}{}\n\\SetKwInOut{KwIn}{Input}\n\\SetKwInOut{KwOut}{Output}\n\\KwIn{}\n"

def conv_algorithm(filename):
    # Using readlines()
    file1 = open(filename, 'r')
    Lines = file1.readlines()
     
    count = 0
    new_file = ""
    # Strips the newline character
    for line in Lines:
        count += 1
        new_line = ""
        if state_line(line):
            state_span = re.search("\\State", line)
            new_line = line[state_span.span()[1]:len(line)-1] + "\\;\n"
            #new_file += new_line + "\n"
        elif else_if_line(line):
            else_if = re.search("\\ElsIf", line)
            new_line = "}\\uElseIf"+line[else_if.span()[1]:len(line)-2] + "}{\n"
        elif end_if_line(line):
            end_if = re.search("\\EndIf", line)
            new_line = "}\n"
        elif if_line(line):
            if_span = re.search("\\If", line)
            new_line = "\\uIf"+line[if_span.span()[1]:len(line)-2] +"}{\n"
        elif end_while(line):
            new_line = "}\n"
        elif while_line(line):
            new_line = "\\While{true}{" + "\n"
        elif end_for_line(line):
            new_line = "}\n"
        elif for_line(line):
            for_span = re.search("\\For", line)
            new_line = "\\For"+line[for_span.span()[1]:len(line)-1] + "{\n"
        elif comment_line(line):
            pass
            #new_line = line
        if DEBUG:
            print("Line{}: {}".format(count, line.strip()))
            print(new_line)
        new_file += new_line 

    return new_file

def state_line(line):
    x = re.search("\\State",line)
    if x != None:
        return True
    else:
        return False
def if_line(line):
    x = re.search("\\If{", line)
    if x != None:
        return True
    else:
        return False

def else_if_line(line):
    x = re.search("\\ElsIf", line)
    if x != None:
        return True
    else:
        return False

def end_if_line(line):
    x = re.search("\\EndIf", line)
    if x != None:
        return True
    else:
        return False


def while_line(line):
    x = re.search("\\\Loop", line)
    if x != None:
        return True
    else:
        return False

def end_while(line):
    x = re.search("\\EndLoop", line)
    if x != None:
        return True
    else:
        return False

def for_line(line):
    x = re.search(r'\For', line)
    if x != None:
        return True
    else:
        return False

def end_for_line(line):
    x = re.search("\\EndFor", line)
    if x != None:
        return True
    else:
        return False
def comment_line(line):
    x = re.search("\\Comment", line)
    if x != None:
        return True
    else:
        return False

algo = conv_algorithm(filename)
new_file = header + algo_setup + algo
end_setup = "\\end{algorithm}\n\\end{document}\n"

new_file = header + start_algo + algo_setup + algo + end_setup

print(new_file)
