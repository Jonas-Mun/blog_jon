At the moment two methods are utilized to calculate the positions on the
radius, and the positions behind the units on the radius. This approach
is limiting when wanting to affect a certain depth, as doing so would
require one to juggle with various variables. To affect a specific depth
and not have to juggle with various variables, a general procedure will
be implemented that will allow more control on the grid positions.

## Scenarios

The following scenarios will help derive the procedure. Currently the
positions on the radius are calculated starting from I and moving up to
E

::: center
:::

::: center
:::

The units currently lie on positions located on the radius, while the
depth units will lie behind the front units.

::: center
:::

::: center
:::

Notice how the beginning of the depth position is exactly `d` distance
away from `I`.

::: center
:::

::: center
:::

One can imagine moving the center of the circle to the beginning of the
depth, `I_d`. Where `I_d` is the intial position, `I`, on the depth *d*.

::: center
:::

::: center
:::

Since this is the same radius but in different position, the same
equation `positionsOnRadius` can be utilized to calculate the depth
positions. The `I_d` position must be found in order to calculate the
positions on the current depth radius.

## I Depth

From the diagram above, $I_1$ is d distance away from I. This is the
exact calculation as acquiring a depth unit from the front position,
`F`. Simply replace `F` with `I_0` for the example above: $$\centering
I_1 = I_0 + (d * (\sin(\theta),-\cos(\theta)) )$$

The general equation for gathering the `I_{d+1}` given `I_{d}`:
$$\centering
I_{d+1} = I_d + (d * (\sin(\theta),-\cos(\theta)) )$$

## Number of Depths

The number of depths is determined by how many rows of units are in the
formation. One must first calculate the:

-   Number of rows in formation, given max units

-   Number of units on a row/radius

$$\centering
totalDepths = \frac{maxUnits}{unitsOnRadius}$$

### Example

::: center
:::

This considers the front row a depth row. Where $\frac{10}{10} = 1$.
Instead of treating the first row as a depth row of one, it will be
better to treat it as the 0th depth row. resulting in the following
equation $$\centering
totalDepth = (maxUnits / unitsOnRadius) - 1$$

Depicting the 0th depth row, otherwise known as the front row.

::: center
:::

### Leftover units

The equation currently calculates the number of full rows, but does not
take into consideration the rows that are not full. In other words, the
last row, if not full, will not be considered in the totalDepth
equation.

::: center
:::

Leftover units are calculated by taking the remainder of total depth:
$$\centering
    \texttt{maxUnits} \bmod \texttt{unitsOnRadius}$$ When left-over
units is greater than zero, then the last depth contains leftover units
and a `1` is added onto `total depths`. If not then the last row is
full, and `0` is added onto `total depths`.

This behaviour is abstracted into the function below.
$$LeftOverCheck(maxUnits, unitsOnRadius)$$

Having *Left-over* check be added onto the *totalDepth*:

$$totalDepth = (\frac{maxUnits}{unitsOnRadius} - 1) + LeftOverCheck(maxUnits, unitsOnRadius)$$

::: center
:::

## Deriving the procedure

The general algorithm goes as follows:

1.  Given $I_d = I_0$

2.  Calculate positions on $I_d$

3.  No more units? Go to 7

4.  Calculate $I_{d+1}$.

5.  $I_d = I_{d+1}$.

6.  Repeat 2

7.  Return positions.

## Visual Representation

### Calculating positions on $I_0$

::: center
::: center
:::

::: center
:::
:::

::: center
::: center
:::

::: center
:::
:::

### Calculating $I_1$

::: center
::: center
:::

[\[fig: calculating \$I_1\$\]]{#fig: calculating $I_1$
label="fig: calculating $I_1$"}

::: center
:::

[\[fig: \$I_1\$ coordinate\]]{#fig: $I_1$ coordinate
label="fig: $I_1$ coordinate"}
:::

### Shifting circle

::: center
::: center
:::

[\[fig: Shifting\]]{#fig: Shifting label="fig: Shifting"}

::: center
:::

[\[fig: New Radius\]]{#fig: New Radius label="fig: New Radius"}
:::

### Calculating positions on $I_1$

::: center
::: center
:::

::: center
:::
:::

::: center
::: center
:::

::: center
:::
:::

### Calculating $I_2$

::: center
::: center
:::

[\[fig: calculating \$I_1\$\]]{#fig: calculating $I_1$
label="fig: calculating $I_1$"}

::: center
:::

[\[fig: \$I_1\$ coordinate\]]{#fig: $I_1$ coordinate
label="fig: $I_1$ coordinate"}
:::

### Shifting circle

::: center
::: center
:::

[\[fig: Shifting\]]{#fig: Shifting label="fig: Shifting"}

::: center
:::

[\[fig: New Radius\]]{#fig: New Radius label="fig: New Radius"}
:::

::: center
::: center
:::

::: center
:::
:::

### Calculating Positions on single line

This is the same procedure as calculating the positions of the
`Front Units`.

Minor changes were added to accomodate for the general behaviour.

### Moving along Depths

For every new depth, the positions on the line must be calculated.

## Final Procedure

Combining both steps, gives the following procedure:
