Currently in the last row, the units go to the far left of the
formation.

::: center
:::

What if one wants the units on the last row be placed in the middle of
the formation as so:

::: center
:::

This behaviour can be implemented by observing Pascal's Triangle and
deriving a pattern for the last row. This paper will not delve into the
details as to how Pascal's Pattern is derived, rather it will focus on
the essential properties that can be used to implement the behaviour.

### Steps in Applying Pascal's Pattern Positions

1.  Find mid point

2.  Calculate Pascal's center from mid point

3.  Calculate Pascal's end from center

4.  Calculate positions starting from Pascal's end.

## Scenarios

Given the mid point of the line, `m`.

::: center
:::

### Pascal's Center

When there is one unit, it will lie directly behind the mid point, `d`
distance away.

::: center
:::

The center, `c`, is then calculated as follows: $$c = mid - d$$

Applying it to the Unit circle is the same as calculating the depth unit
position from the front unit. Thi difference is that the front unit is
the mid point, `m`, of the radius.

$$c = mid + (\sin(\theta), -\cos(\theta)) * d$$

This works for a formation of depth 1. To have it work with multiple
depths, `c` must be multiplied by the depth it lies on to correctly get
the `c` being at the last row.

Resulting in the equation:

$$c = mid + (\sin(\theta), \cos(\theta)) * d * curDepth$$

### Pascal's End

When there is one unit, Pascal's End, `e` lies exactly on `c`.

::: center
:::

Given two units, Pascal's End lies $\frac{d}{2}$ distance away from `c`.

::: center
:::

Given three units, Pascal's End lies $d$ distance away from `c`.

::: center
:::

Allowing one to derive the following equation for Pascal's End:

$$e = c - \frac{d}{2} * (numUnits - 1)$$

Now applying it onto the Unit Circle:

$$e = c - \frac{d}{2} * (numUnits - 1) * (\cos(\theta),(\sin(\theta)))$$

c is subtracted from the coordinates because e lies below C in terms of
radius given and angle $\theta$. Acquiring the negative value of
$(\cos(\theta), \sin(\theta))$ allows one to travel the radius in the
opposite direction (i.e downwards).

## Applying the Unit Circle

Given a circle of radius 6.

::: center
:::

The following properties apply:

-   Unit size = 1

-   gap = 1.

-   max units = 5.

-   total front units = 3

::: center
:::

### Mid point

The mid point is the center of the radius. $mid = (E - I) / 2$ This
gives half the vector of the radius, `(rx, ry)`, but not the specific
point at the mid point on radius. To align the mid point onto the
current radius, simply add `(rx, ry)` to `I`.
$midPoint = I + halfRadius$

::: center
:::

The equation for the mid point, `m`: $$m = I + \frac{E - I}{2}$$

### Center point

Apply the equation of the center point given the mid point

::: center
:::

**NOTICE** how this is the same behaviour as calculating the unit in
$depth_{n}$ given the front unit at $depth_{n-1}$

### End point

To find `e`, apply the equation:

::: center
:::

Imagine having another circle with `c` being the center of the circle.
(fitting name).

::: center
:::

**NOTICE** how the diameter of the circle on `c` starting from `e`
contains the line in which the units will be placed upon.

::: center
:::

One can then imagine another circle with center on `e`, and a radius
with the diameter of the circle of `c`.

### Circle on End point

::: center
:::

Given that the new radius on `e` has the same angle as the radius from
circle `I`, the equation to calculate a position on a line is applied.

$$position = e + d * (\cos(\theta), \sin(\theta)) * i$$

### Calculating Positions

::: center
:::

::: center
:::

## Pascal's Positions

To calculate the positions in the last row, the following values need to
be known.

-   mid point

-   center

-   end point

-   remainder units on last row

## Pascal's Procedure

Since applying Pascal's positions is on the last depth, a check can be
implemented that checks whether the current depth is the last and if so
apply the Pascal's position.

An issue occurs when the *totalFrontUnits* is equal to *maxUnits*. The
units dissapear. This is becuase $maxUnits mod totalFrontUnits = 0$ and
the first row is also the last depth. Meaning no positions will be
calculated.

One can fix this by only applying pascal's positions if there are any
remainder units.

## Sliding Row - Issue

When implementing the Pascal as is, one will encounter the last row to
be sliding while dragging, even though the formation will be stationary.
This is due to the End value that is sent to the grid formation.

::: center
:::

When the radius fits exactly the amount of units, the last row will
behave as desired.

But when the radius calculated upon dragging does not fit the exact
number of units and is hanging to the right, the last row will move
because the radius has increased, and thus the mid point, `m` has moved.

::: center
:::

The solution is to pass the position of the last unit onto
`Pascal’s Positions`

::: center
:::

Acquiring the last position of the unit on the radius: When the length
of the radius allows for exactly the number of totalFrontUnits to fit,
then the position of the last unit is at **totalFrontUnits-1**. But if
the length of the radius exceeds the *maxUnits* then the position of the
last unit is at **maxUnits-1**. This is because as you drag the radius
along, the length of the radius gets bigger and so do the
totalFrontUnits, but not the maxUnits value which can cause the row to
drag along.

$$\centering
    lastUnitIndex = min(maxUnits,totalFrontUnits)-1$$

$$\centering
    end = I + ((sizeOfUnit + gap) * (\cos(\theta), \sin(\theta)) * lastUnitIndex(maxUnits, totalFrontUnits)$$

## Applying fix

The last position on the line must be calculated and passed onto
Pascal's positions.

::: center
:::
