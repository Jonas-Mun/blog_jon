Currently the number of units on the line is dependent on the size of
the line, and not on a fixed value. If one wants to display a fixed
number of units, regardless of the size of the radius, then the
following changes must be made in *PositionsOnRadius*

## Adding limit to Number of Units

The *PositionsOnRadius* will be given a limit to the number of position
to calculate based on the size of the Radius.

## Unit Left Hanging

An issue arises when the length of the radius can not accomodate all the
units that are available to be placed on the radius. The unit that does
not fit in the radius will be left hanging.

::: center
:::

One can decided to ignore the hanging unit and discard it

::: center
:::

or create depth in the formation. Making hanging unit be placed directly
behind the first unit located at I.

::: center
:::
