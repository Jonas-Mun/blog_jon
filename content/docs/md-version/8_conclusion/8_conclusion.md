What has been implemented lays the foundation for the Grid Formation
Behaviour. From here one can add, modify, or experiment with the
behaviour as one desires. All new additions to the Base behaviour will
be included in the `Appendix A`.
