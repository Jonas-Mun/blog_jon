## Equations

### Base Equations

$$\begin{aligned}
    totalUnits &= \frac{lengthOfRadius}{sizeOfUnit + gap} + 1\\
    \theta &= \tan^{-1}(\frac{opposite}{adjacent})\\
    positionOnRadius &= I + ((sizeOfUnit + gap) * (\cos(\theta), \sin(\theta)) * i)\\
    positionOnLine &= I + (distBtxt * i)\\ depthUnitPosition &= F + ((\sin(\theta), -\cos(\theta)) * distBtxt) \\
    totalDepth &= (\frac{maxUnits}{unitsOnRadius}-1) + (maxUnits \% unitsOnRadius == 0 ? 0 : 1\end{aligned}$$

### General Position Equation

$$\begin{aligned}
    I_{d+1} &= I_d + distBtxt * (\sin(\theta), -\cos(\theta))\end{aligned}$$

### Pascal equations

$$\begin{aligned}
    m &= I +\frac{E-I}{2}\\
    c &= m + distBtxt * (\sin(\theta), -\cos(\theta)) * depth\\
    e &= c - \frac{d}{2} * (remainder - 1) * (\cos(\theta), \sin(\theta))\\
    endUnit &= I + distBtxt * (\cos(\theta), \sin(\theta)) * (min(maxUnits, totalFrontUnits)-1)\end{aligned}$$

## Equation Implementations

## Procedures

### Main

### Position Calculaters

### General Position calculator

### General + Pascal Positions

### Pascal Positions
