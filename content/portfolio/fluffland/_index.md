---
title: Fluffland - Dawn of Bunnies
description: RTS game made in 9 days
toc: true
authors: fonas
tags: ["fluffland"]
categories:
series:
date: '2021-08-16'
lastmod: '2021-08-16'
featuredImage: images/fluffland.png
draft: false
---



A Hexgonal-grid RTS-game made in 9 days
# Video Link
<a href="https://youtu.be/SOLM6UBFyCA">Fluffland: Dawn of the Bunnies</a>

# Team
* D4yz - Pixel Artis 
* Isabella - Music/SFX
* WillowBlade - Secondary Programmer
* Jonas - Primary Programmer

# What I implemented
* Hexagonal-grid
* Path-finding
* Unit AI
* Map Maker/builder
* Serialization/Deserialization of levels
