---
title: Doctor & Plague
description: Card Game
toc: true
authors:
tags:
categories:
series:
date: '2020-10-16'
lastmod: '2020-10-16'
featuredImage: images/doctor_plague.png
draft: false
---


This is a card game I helped create in 9 days in the Unity 3D game engine
# Video Link
<a href="https://youtu.be/yOKjmznOHHk">Doctor & Plague</a>

# Team
* Singularity Games

# My Role/What I implemented: Lead programer
* Robust component-based card system
* AI that plays cards according to the card rules of the game
* Camera controls
* Turn-based system
* Menu and gameplay interaction
