---
title: Jonas
draft: false
role: Programmer
avatar: images/compass_v2.png
bio: 
organization:
  name: 
  url: 
social:
  - icon: envelope
    iconPack: fas
    url: mailto:jonas_munz@outlook.com
#  - icon: twitter
#    iconPack: fab
#    url: https://example.com/
  - icon: github
    iconPack: fab
    url: https://github.com/Cartogy

weight: 1
widget:
  handler: about

  # Options: sm, md, lg and xl. Default is md.
  width:

  sidebar:
    # Options: left and right. Leave blank to hide.
    position:
    # Options: sm, md, lg and xl. Default is md.
    scale:
  
  background:
    # Options: primary, secondary, tertiary or any valid color value. Default is primary.
    color: secondary
    image:
    # Options: auto, cover and contain. Default is auto.
    size:
    # Options: center, top, right, bottom, left.
    position:
    # Options: fixed, local, scroll.
    attachment: 
---

## Self Introduction
My name is Jonas. I am a an individual who has obtained a Bachelor Degree in Computer Science. Even though my education is mostly in Computer Science, I enjoy various topics such as history, plants, and reading books. I plan to document my experiences and findings as a programmer in this blog. This blog will serve as a place to document my journey would help me in the future in case I forget something or help others who have faced similar struggles.


## Details  

Programming Languages Used
- C, C++, Rust, Java
- Ocaml, Haskell

Technologies/APIs used
- Vulkan, OpenGL, Wgpu-rs
- Unity game engine, Godot
