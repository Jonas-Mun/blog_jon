---
title: Experiences
draft: false
experiences:
  - title: Software Engineer 
    organization:
      name: Proud Cipher Media Group
      url: https://example.org
    dates: '2020 - 2021'
    location: Remote
    writeup: >
      Responsible for AI, Optimizations and Architecture.

      - Refactored all AIs to use State machines. Allowing the team to debug and make changes at a faster rate.

      - Created the software architecture, making it easier to reason about the game logic and implement similar
      behaviours without having to code everything from scratch.

      - Built robust systems such as: Dialogue Player, Missions and State Machines. This allowed our team to use 
      these systems multiple times throughout the project where needed.
      
  - title: Lead Programmer
    organization: 
      name: Godot Wild Jam 36 Team
      url: https://example.org
    dates: '13th August 2021 - 22nd August 2021'
    location: Remote
    writeup: >
     Responsible for core gameplay, AI, Tool creator

       - Created a map maker that allowed our team to debug mechanics and create levels at a faster rate

       - Implemented the AI in an abstract manner, that allowed us to create multiple AIs with similar behaviour.

       - Created the hexagonal grid, path-finding and AI combat which underpinned the foundation of our game.
  - title: Lead Programmer
    organization:
      name: Singularity Games
      url:
    dates:
    location: Remote
    writeup: >
     Implemented the core gameplay, AI, 
       
       - Created a robust card system that allowed the team to experiment with various values for each card.

       - Implemented the AI, adding a difficulty component onto the game

       - Created the turn-based system for the game. 

weight: 3
widget:
  handler: experiences

  # Options: sm, md, lg and xl. Default is md.
  width: lg

  sidebar:
    # Options: left and right. Leave blank to hide.
    position: left
    # Options: sm, md, lg and xl. Default is md.
    scale:
  
  background:
    # Options: primary, secondary, tertiary or any valid color value. Default is primary.
    color:
    image:
    # Options: auto, cover and contain. Default is auto.
    size:
    # Options: center, top, right, bottom, left.
    position:
    # Options: fixed, local, scroll.
    attachment: 
---
